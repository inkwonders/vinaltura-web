<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>VINALTURA  | Tienda</title>

	<?php

		$ruta = "https://clientes.ink/vinaltura/";
		$descripcion = "Vinaltura es un proyecto familiar, que de manera respetuosa con el entorno natural, transforma las bondades de la altura sobre la vid en un vino de calidad. Enfocados en la vitivinicultura razonada, mediante un adecuado manejo de viñedo, buscamos obtener la mejor calidad de uva que la naturaleza y el terruño nos proporcionan. Nuestra paciencia y pasión, quedan plasmadas en cada una de nuestras etiquetas. Vinaltura más que un viñedo, un terruño.";

	?>

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<!--<meta http-equiv="Expires" content="0">
	<meta http-equiv="Last-Modified" content="0">
	<meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
	<meta http-equiv="Pragma" content="no-cache">-->

	<meta name="Description" content="VINALTURA®" />
	<meta name="meta_url" property="og:url" content="<?php echo $rurta; ?>" />
	<meta name="meta_title" property="og:title" content="VINALTURA®" />
	<meta name="meta_image" property="og:image" content="<?php echo $rurta; ?>/vistas/assets/favicon/android-chrome-192x192.png" />
	<meta name="meta_desc" property="og:description" content="<?php echo $descripcion; ?>" />

	<!-- Open Graph / Facebook -->
	<meta property="og:type" content="website">
	<meta name="fb_url" property="og:url" content="<?php echo $rurta; ?>">
	<meta name="fb_title" property="og:title" content="VINALTURA®">
	<meta name="fb_desc" property="og:description" content="<?php echo $descripcion; ?>">
	<meta name="fb_img" property="og:image" content="<?php echo $rurta; ?>/vistas/assets/favicon/android-chrome-192x192.png">

	<!-- Twitter -->
	<meta name="twitter:card" content="summary_large_image">
	<meta name="tw_url" property="twitter:url" content="<?php echo $rurta; ?>">
	<meta name="tw_title" property="twitter:title" content="VINALTURA®">
	<meta name="tw_desc" property="twitter:description" content="<?php echo $descripcion; ?>">
	<meta name="tw_img" property="twitter:image" content="<?php echo $rurta; ?>/vistas/assets/favicon/android-chrome-192x192.png">


	<!-- fuentes -->
	<link rel="preload" href="../assets/fonts/Avenir.ttc" as="font" type="font/ttc" crossorigin>
	<link rel="preload" href="../assets/fonts/Gotham-Black.otf" as="font" type="font/otf" crossorigin>
	<link rel="preload" href="../assets/fonts/Gotham-Bold.otf" as="font" type="font/otf" crossorigin>
	<link rel="preload" href="../assets/fonts/Gotham-Book.otf" as="font" type="font/otf" crossorigin>
	<link rel="preload" href="../assets/fonts/Gotham-Light.otf" as="font" type="font/otf" crossorigin>
	<link rel="preload" href="../assets/fonts/Gotham-Medium.otf" as="font" type="font/otf" crossorigin>
	<link rel="preload" href="../assets/fonts/Prata-Regular.ttf" as="font" type="font/ttf" crossorigin>
	<link rel="preload" href="../assets/fonts/Gotham-ExtraLight.ttf" as="font" type="font/ttf" crossorigin>

	<!--favicon -->

	<link rel="apple-touch-icon" sizes="180x180" href="../assets/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="../assets/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="../assets/favicon/favicon-16x16.png">
	<link rel="mask-icon" href="../assets/favicon/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">

	<!-- end favicon -->

	<!-- webmanifest -->

	<!-- <link rel="manifest" href="assets/favicon/site.webmanifest"> -->

	<!--end web manifest -->

	<!-- ESTILOS -->

	<!--<link rel="stylesheet" href="assets/css/estilos.css">-->
	<link rel="stylesheet" href="../assets/css/estilos.min.css">

	<!-- END ESTILOS --->

	<!-- SLICK -->

	<link rel="stylesheet" type="text/css" href="../assets/slick/slick.css">
	<link rel="stylesheet" type="text/css" href="../assets/slick/slick-theme.css">

	<!-- END SLICK -->

	<!--<link rel="stylesheet" href="../assets/css/animated.css">-->
	<link rel="stylesheet" href="../assets/css/animated.min.css">

</head>
<body style="background-color: #222222;">
    <?php include "../header_tienda.php"; ?>

    <section class="home_tienda">

        <img class="uvas-tienda" src="../assets/img/svg/uvas-tienda.svg">

        <div class="cont-mensaje-tienda">
            <h2 class="prox">PRÓXIMAMENTE</h2>
            <h2 class="tien">TIENDA EN LÍNEA</h2>
            <h4 class="msj">Estamos en contrucción de nuestra nueva tienda
            en línea; por el momento con gusto atenderemos sus pedidos
            por Whatsapp.</h4>
            <a href="https://wa.link/qn4qro" target="_blank" class="wha"> <img class="wha-ico" src="../assets/img/svg/ico-what.svg" alt="whatsapp vinaltura">(442) 129 0903</a>
			<a href="../assets/pdf/CATALOGO_CARTA_VINALTURA.pdf" target="_blank" class="btn_calatologo" style="padding-top: 40px;">Descargar Catálogo</a>
        </div>
    
    </section>

	<!-- JQUERY -->

	<script type="text/javascript" src="../assets/js/jquery.min.js"></script>

	<!-- END JQUERY -->


	<!-- FONTAWESOME -->

	<script src="https://kit.fontawesome.com/2c36e9b7b1.js" crossorigin="anonymous"></script>

	<!-- END FONTAWESOME -->

</html>