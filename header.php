<header>
	<div class="reservacion">
		<a title="Reservar"  class="reservar_texto" op="8">
			<h2>RESERVACIONES</h2>
		</a>
	</div>
	<nav class="menu">

		<div class="men menu_opc_animation" op="1">NOSOTROS</div>

		<div class="men menu_opc_animation" op="2">VINOS</div>

		<div class="men menu_opc_animation" op="4">WINECLUB</div>

		<div class="men menu_opc_animation" op="0"><img class="logo_blanco" src="assets/img/svg/logo_cafe_completo.svg" alt="Vinaltura"></div>

		<div class="men menu_opc_animation" op="7">VISITAS</div>

		<!-- <div class="men menu_opc_animation" op="5">TERRAZA</div> -->

		<div class="men menu_opc_animation" op="3"><a href="https://tiendavinaltura.mx/">TIENDA</a></div>

		<div class="men menu_opc_animation" op="6">CONTACTO</div>

	</nav>


	<!-- <div class="login">LOGIN &#32; <svg class="img_login" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 23.77 23.77"><defs><style>.cls-1{fill:var(--c_cafe);}</style></defs><title>login</title><g id="Capa_2" data-name="Capa 2"><g id="Página"><path class="cls-1" d="M11.89,0A11.89,11.89,0,1,0,23.77,11.89,11.9,11.9,0,0,0,11.89,0ZM7.11,20.77V18.46a4.83,4.83,0,0,1,9.66,0v2.25a10.1,10.1,0,0,1-9.66.06Zm4.78-9a2.24,2.24,0,1,1,2.24-2.24A2.24,2.24,0,0,1,11.89,11.77Zm6.68,7.66v-1a6.62,6.62,0,0,0-3.84-6,4.08,4.08,0,1,0-5.65,0,6.64,6.64,0,0,0-3.77,6v1.06a10.08,10.08,0,1,1,13.26-.09Z"/></g></g></svg></div> -->

</header>

<!-- MENU MOVIL -->
<div class="menu-bar">

	<div class="nom_pos_menu_movil"></div>

	<div class="men logo_mov_header" op="0"><img class="logo_blanco" src="assets/img/svg/logo_cafe_completo.svg" alt="Vinaltura"></div>

	<div class="container" onclick="myFunction(this)" id="container">
		<div class="bar1"></div>
		<div class="bar2"></div>
		<div class="bar3"></div>
	</div>
</div>
<div class="reservacion reservacion_movil">
	<a title="Reservar"  class="reservar_texto" op="8">
		<h2>RESERVACIONES</h2>
	</a>
</div>

<div id="contenidoMenu" style="display:none">

	<img class="logo_menu_movil menu-el" id="btn_home" op='home' onclick="myFunction(this)" src="assets/img/svg/logo_movil_cafe.svg" alt="Vinaltura">

	<div class="menu-conten">

		<span class="menu-el" id="btn_nosotros" op='nosotros' onclick="myFunction(this)">NOSOTROS</span><br /><br />
		<span class="menu-el" id="btn_vinos" op='vinos' onclick="myFunction(this)">VINOS</span><br /><br />
		<span class="menu-el" id="btn_wineclub" op='wineclub' onclick="myFunction(this)">WINECLUB</span><br /><br />
		<span class="menu-el" id="btn_tienda" op='visitas' onclick="myFunction(this)">VISITAS</span><br /><br />
		<span class="menu-el" id="btn_terraza" op='tienda' ><a style="text-decoration: none; color:#503629" href="https://tiendavinaltura.mx/">TIENDA</a></span><br /><br />
		<span class="menu-el" id="btn_contacto" op='contacto' onclick="myFunction(this)">CONTACTO</span><br /><br /><br />

		<!-- <span class="menu-el" id="btn_login" op='login'><img class="icono_login_movil" src="assets/img/svg/login2.svg" alt=""><b>LOGIN</b></span><br /><br /><br /> -->


	</div>

</div>
<!-- END MENU MOVIL -->