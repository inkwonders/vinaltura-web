<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="UTF-8">
	<title>VINALTURA</title>

	<?php

	$ruta = "https://clientes.ink/vinaltura/";
	$descripcion = "Vino mexicano de altura.";

	?>

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<!--<meta http-equiv="Expires" content="0">
	<meta http-equiv="Last-Modified" content="0">
	<meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
	<meta http-equiv="Pragma" content="no-cache">-->

	<meta name="Description" content="VINALTURA®" />
	<meta name="meta_url" property="og:url" content="<?php echo $rurta; ?>" />
	<meta name="meta_title" property="og:title" content="VINALTURA®" />
	<meta name="meta_image" property="og:image" content="<?php echo $rurta; ?>/assets/img/slider/1.png" />
	<meta name="meta_desc" property="og:description" content="<?php echo $descripcion; ?>" />

	<!-- Open Graph / Facebook -->
	<meta property="og:type" content="website">
	<meta name="fb_url" property="og:url" content="<?php echo $rurta; ?>">
	<meta name="fb_title" property="og:title" content="VINALTURA®">
	<meta name="fb_desc" property="og:description" content="<?php echo $descripcion; ?>">
	<meta name="fb_img" property="og:image" content="<?php echo $rurta; ?>/assets/img/slider/1.png">

	<!-- pixel facebook -->
	<meta name="facebook-domain-verification" content="hyvn2m20gdycwfm4h9wrjti7v74xix" />

	<!-- Twitter -->
	<meta name="twitter:card" content="summary_large_image">
	<meta name="tw_url" property="twitter:url" content="<?php echo $rurta; ?>">
	<meta name="tw_title" property="twitter:title" content="VINALTURA®">
	<meta name="tw_desc" property="twitter:description" content="<?php echo $descripcion; ?>">
	<meta name="tw_img" property="twitter:image" content="<?php echo $rurta; ?>/assets/img/slider/1.png">


	<!-- fuentes -->
	<link rel="preload" href="assets/fonts/Avenir.ttc" as="font" type="font/ttc" crossorigin>
	<link rel="preload" href="assets/fonts/Gotham-Black.otf" as="font" type="font/otf" crossorigin>
	<link rel="preload" href="assets/fonts/Gotham-Bold.otf" as="font" type="font/otf" crossorigin>
	<link rel="preload" href="assets/fonts/Gotham-Book.otf" as="font" type="font/otf" crossorigin>
	<link rel="preload" href="assets/fonts/Gotham-Light.otf" as="font" type="font/otf" crossorigin>
	<link rel="preload" href="assets/fonts/Gotham-Medium.otf" as="font" type="font/otf" crossorigin>
	<link rel="preload" href="assets/fonts/Prata-Regular.ttf" as="font" type="font/ttf" crossorigin>
	<link rel="preload" href="assets/fonts/Gotham-ExtraLight.ttf" as="font" type="font/ttf" crossorigin>

	<!--favicon -->

	<link rel="apple-touch-icon" sizes="180x180" href="assets/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="assets/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="assets/favicon/favicon-16x16.png">
	<link rel="mask-icon" href="assets/favicon/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">

	<!-- Google tag (gtag.js) --> 
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-8NDVBYVJS4"></script> 
	<script> window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date()); gtag('config', 'G-8NDVBYVJS4'); 
	</script>

	<!-- end favicon -->
	<script async defer src="https://tools.luckyorange.com/core/lo.js?site-id=2c1b20fe"></script>
	<!-- webmanifest -->

	<!-- <link rel="manifest" href="assets/favicon/site.webmanifest"> -->

	<!--end web manifest -->

	<!-- ESTILOS -->
	<style>



		/* MODAL
–––––––––––––––––––––––––––––––––––––––––––––––––– */
		.modal {
			position: fixed;
			top: 0;
			left: 0;
			right: 0;
			bottom: 0;
			display: flex;
			align-items: center;
			justify-content: center;
			padding: 1rem;
			background: rgba(0, 0, 0, 0.7);
			cursor: pointer;
			visibility: hidden;
			opacity: 0;
			transition: all 0.35s ease-in;
			z-index: 9999;
		}

		.modal.is-visible {
			visibility: visible;
			opacity: 1;
		}

		.modal-dialog {
			position: relative;
			max-width: 95%;
			max-height: 60vh;
			width: 100%;
			height: 100%;
			border-radius: 5px;
			background: white;
			overflow: auto;
			overflow-x: hidden;
			padding: 10px 0;
			cursor: default;
			display: flex;
			justify-content: center;
			align-items: center;
		}

		.modal-dialog>* {
			padding: 1rem;
		}

		.close-modal {
			position: absolute;
			top: -10px;
			right: -5px;
			border: 0px transparent;
			background-color: transparent;
			font-size: 16px;
			transition: all;

		}

		.close-modal:hover {
			transform: scale(1.5);
			cursor: pointer;
		}

		.modal-header {
			display: flex;
			align-items: center;
			justify-content: space-between;
		}

		.modal-content {
			width: 90%;
			height: 90%;
		}

		.iframe_modal {
			width: 100%;
			height: 100%;
			border: solid 1px black;
			padding-bottom: 10px;
		}

		.modal-header .close-modal {
			font-size: 1.5rem;
		}

		.modal p+p {
			margin-top: 1rem;
		}


		/* ANIMATIONS
–––––––––––––––––––––––––––––––––––––––––––––––––– */
		[data-animation] .modal-dialog {
			opacity: 0;
			transition: all 0.5s var(--bounceEasing);
		}

		[data-animation].is-visible .modal-dialog {
			opacity: 1;
			transition-delay: 0.2s;
		}

		[data-animation="slideInOutDown"] .modal-dialog {
			transform: translateY(100%);
		}

		[data-animation="slideInOutTop"] .modal-dialog {
			transform: translateY(-100%);
		}

		[data-animation="slideInOutLeft"] .modal-dialog {
			transform: translateX(-100%);
		}

		[data-animation="slideInOutRight"] .modal-dialog {
			transform: translateX(100%);
		}

		[data-animation="zoomInOut"] .modal-dialog {
			transform: scale(0.2);
		}

		[data-animation="rotateInOutDown"] .modal-dialog {
			transform-origin: top left;
			transform: rotate(-1turn);
		}

		[data-animation="mixInAnimations"].is-visible .modal-dialog {
			animation: mixInAnimations 2s 0.2s linear forwards;
		}

		[data-animation="slideInOutDown"].is-visible .modal-dialog,
		[data-animation="slideInOutTop"].is-visible .modal-dialog,
		[data-animation="slideInOutLeft"].is-visible .modal-dialog,
		[data-animation="slideInOutRight"].is-visible .modal-dialog,
		[data-animation="zoomInOut"].is-visible .modal-dialog,
		[data-animation="rotateInOutDown"].is-visible .modal-dialog {
			transform: none;
		}

		@keyframes mixInAnimations {
			0% {
				transform: translateX(-100%);
			}

			10% {
				transform: translateX(0);
			}

			20% {
				transform: rotate(20deg);
			}

			30% {
				transform: rotate(-20deg);
			}

			40% {
				transform: rotate(15deg);
			}

			50% {
				transform: rotate(-15deg);
			}

			60% {
				transform: rotate(10deg);
			}

			70% {
				transform: rotate(-10deg);
			}

			80% {
				transform: rotate(5deg);
			}

			90% {
				transform: rotate(-5deg);
			}

			100% {
				transform: rotate(0deg);
			}
		}
	</style>

	<!-- <link rel="stylesheet" href="assets/css/estilos.css"> -->
	<link rel="stylesheet" href="assets/css/estilos.min.css">

	<!-- END ESTILOS --->

	<!-- SLICK -->

	<link rel="stylesheet" type="text/css" href="assets/slick/slick.css">
	<link rel="stylesheet" type="text/css" href="assets/slick/slick-theme.css">

	<!-- END SLICK -->

	<!--<link rel="stylesheet" href="assets/css/animated.css">-->
	<link rel="stylesheet" href="assets/css/animated.min.css">

</head>

<body>

	<?php include "preload.php"; ?>
	<!-- <div style="background-color: #ca9d4a; width:100%;align-items: center; color: white;">Reservaciones</div> -->
	<?php include "header.php"; ?>

	<section class="slider_principal slide_desktop_home">

		<div class="slide slide_desktop sl_1"></div>
		<div class="slide slide_desktop sl_2"></div>
		<!--<div class="slide slide_desktop sl_3"></div>-->
		<div class="slide slide_desktop sl_4"></div>

		<section class="circulo_scroll_mouse">
			<div class="mouse">
				<div class="scroll_mosue_circulo"></div>
			</div>
		</section>

	</section>

	<section class="slider_principal slide_movil_home">

		<div class="slide slide_movil sl_1_m"></div>
		<div class="slide slide_movil sl_2_m"></div>
		<div class="slide slide_movil sl_3_m"></div>

		<section class="circulo_scroll_mouse">
			<div class="mouse">
				<div class="scroll_mosue_circulo"></div>
			</div>
		</section>

	</section>

	<!-- MOVIL LINK'S SLIDER PRINCIPAL -->
	<div class="espacio_blanco_movil"></div>



	<!-- END MOVIL LINK'S SLIDER PRINCIPAL -->

	<div class="espacio_blanco"></div>
	<div class="espacio_blanco_movil"></div>

	<!-- MENSAJE - SECCION NUEVA -->

	<section class="nosotros_nuevo">

		<div class="seccion_mensaje">

			<h2 class="txt_mensaje">
				<!-- COMO EL BUEN <span class="txt_resaltado">VINO</span>, NOS ESFORZAMOS PARA <br>
				TRASCENDER A TRAVÉS DEL <span class="txt_resaltado">TIEMPO</span> <br>
				CON UN <span class="txt_resaltado">LEGADO</span> FAMILIAR. -->
				"Trascender a través del tiempo, con un legado familiar."
			</h2>

		</div>

		<div class="seccion_nosotros animated bounceIn">

			<img class="botellas img_botella_uno animated fadeInLeft" src="assets/img/imgizq_quienes_somos.png" alt="Vinaltura">

			<div class="top_img_bg top_img_bg_uno animated fadeIn">

				<div class="img_top_circulo img_top_circulo_uno"></div>

			</div>

			<!-- otros -->

			<img class="botellas img_botella_dos animated fadeInLeft oculto" src="assets/img/imgizq_manifiesto.png">

			<div class="top_img_bg top_img_bg_dos animated fadeIn oculto">

				<div class="img_top_circulo img_top_circulo_dos"></div>

			</div>

			<img class="botellas img_botella_tres animated fadeInLeft oculto" src="assets/img/imgizq_queretaro.png">

			<div class="top_img_bg top_img_bg_tres animated fadeIn oculto">

				<div class="img_top_circulo img_top_circulo_tres"></div>

			</div>

			<img class="botellas img_botella_cuatro animated fadeInLeft oculto" src="assets/img/imgizq_viticultura.png">

			<div class="top_img_bg top_img_bg_cuatro animated fadeIn oculto">

				<div class="img_top_circulo img_top_circulo_cuatro"></div>

			</div>

			<img class="botellas img_botella_cinco animated fadeInLeft oculto" src="assets/img/imgizq_vinedo.png">

			<div class="top_img_bg top_img_bg_cinco animated fadeIn oculto">

				<div class="img_top_circulo img_top_circulo_cinco"></div>

			</div>

			<img class="botellas img_botella_seis animated fadeInLeft oculto" src="assets/img/imgizq_vinicola-old.png">

			<div class="top_img_bg top_img_bg_seis animated fadeIn oculto">

				<div class="img_top_circulo img_top_circulo_seis"></div>

			</div>

			<div class="bottom_img_bg">

				<div class="bottom_izq">

					<div class="fondo_negro_opaco">

					</div>

					<div class="lista_nosotros">

						<div class="centrado padding_30">
							<div class="linea_seleccion_nosotros linea1"></div>
							<p class="txt_lista_nosotros" op="1">QUIÉNES SOMOS</p>
						</div>
						<div class="centrado">
							<div class="linea_seleccion_nosotros no-visible linea2"></div>
							<p class="txt_lista_nosotros" op="2">MANIFIESTO</p>
						</div>
						<!-- <div class="centrado-columna">
							<div class="centrado">
								<div class="linea_seleccion_nosotros no-visible linea3"></div>
								<p class="txt_lista_nosotros"  op="6">VIÑEDO - VITICULTURA RAZONADA</p>
							</div>
							<div class="cont_submenu_nosotros">
								<div class="linea_seleccion_nosotros no-visible linea6"></div><p class="txt_lista_nosotros"  op="7">VINÍCOLA - PEQUEÑAS PARCELAS PARA GRANDES VINOS.</p>
							</div>
						</div> -->

						<div class="centrado">
							<div class="linea_seleccion_nosotros no-visible linea6"></div>
							<p class="txt_lista_nosotros" op="6">VIÑEDO</p>
						</div>

						<div class="centrado">
							<div class="linea_seleccion_nosotros no-visible linea7"></div>
							<p class="txt_lista_nosotros" op="7">VINÍCOLA</p>
						</div>

						<div class="centrado">
							<div class="linea_seleccion_nosotros no-visible linea4"></div>
							<p class="txt_lista_nosotros" op="4">QUERÉTARO REGIÓN DE VINOS</p>
						</div>
						<div class="centrado">
							<div class="linea_seleccion_nosotros no-visible linea5"></div>
							<p class="txt_lista_nosotros" op="5">VITICULTURA EXTREMA</p>
						</div>

					</div>

					<div class="lista_nosotros_mov">

						<div class="centrado">
							<p class="txt_lista_nosotros txt_lista_nosotros_mov" style="text-decoration: underline;" op="1">QUIÉNES SOMOS</p>
							<div class="cont_datos_nosotros cont_quines_somos_mov animated fadeIn cont_datos_nosotros_mov">
								<h2 class="titulo_interior_nosotros">“Trascender a través del tiempo, con un legado familiar”</h2>
								<p class="txt_interior_nosotros">
									<!-- “Trascender a través del tiempo, con un legado familiar.” <br><br> -->
									Somos un proyecto familiar, enfocado a la producción de Vino Mexicano de calidad, que busca
									reflejar la esencia del terruño, el respeto al entorno y a la Naturaleza, así como una gran paciencia,
									pasión, arte y tradición en su elaboración.
								</p>
							</div>
						</div>

						<div class="centrado">
							<p class="txt_lista_nosotros txt_lista_nosotros_mov" op="2">MANIFIESTO</p>
							<div class="cont_datos_nosotros cont_manifiesto_mov animated fadeIn oculto cont_datos_nosotros_mov">
								<p class="txt_interior_nosotros">
									<b>SOÑAMOS</b> con trascender en el proyecto familiar de hacer vino.<br><br>
									<b>PENSAMOS</b> que Querétaro tiene el potencial de convertirse en referente del vino mexicano.<br><br>
									<b>CREEMOS</b> que el vino debe estar siempre en nuestra vida diaria.<br><br>
									<b>AMAMOS</b> el arte de hacer vino.
								</p>
							</div>
						</div>

						<div class="centrado">
							<p class="txt_lista_nosotros txt_lista_nosotros_mov" op="6">VIÑEDO</p>
							<div class="cont_datos_nosotros cont_viñedo_mov animated fadeIn oculto cont_datos_nosotros_mov">
								<h2 class="titulo_interior_nosotros">Viticultura Razonada</h2>
								<p class="txt_interior_nosotros">
									Vinaltura se localiza en el Valle de Colón, Querétaro y cuenta con 20 hectáreas de viñedo, más de
									100,000 vides y 15 variedades de uva entre blancas y tintas, organizado en pequeñas parcelas y con
									una capacidad de producir más de 200 toneladas de uva para vinificar cada año. <br><br>

									El suelo es franco-arcilloso en la superficie, seguido de una capa de tepetate calcáreo y, finalmente,
									una base pedregosa compuesta de piedra laja y basalto.<br><br>

									Para el manejo del viñedo usamos el enfoque de “viticultura razonada”, respetando el medio
									ambiente y la diversidad biológica de la región.
								</p>
							</div>
						</div>

						<div class="centrado">
							<p class="txt_lista_nosotros txt_lista_nosotros_mov" op="7">VINÍCOLA</p>
							<div class="cont_datos_nosotros cont_viñedo_vinicola_mov animated fadeIn oculto cont_datos_nosotros_mov">
								<h2 class="titulo_interior_nosotros">Pequeñas parcelas para grandes vinos</h2>
								<p class="txt_interior_nosotros">
									En Vinaltura elaboramos nuestros vinos con pasión por la cultura vinícola, apoyados en la
									innovación tecnológica, el factor humano calificado y las mejores prácticas de control y calidad. <br><br>
									Vendimiamos pequeñas cantidades de uva para hacer micro-vinificaciones, con una mínima
									intervención enológica, logrando elaborar grandes vinos que expresan la tipicidad de la región, la
									diversidad de la vid, la identidad del terruño y la personalidad de Vinaltura.
								</p>
							</div>
						</div>

						<div class="centrado">
							<p class="txt_lista_nosotros txt_lista_nosotros_mov" op="4">QUERÉTARO REGIÓN DE VINOS</p>
							<div class="cont_datos_nosotros cont_queretaro_region_de_vino_mov animated fadeIn oculto cont_datos_nosotros_mov">
								<h2 class="titulo_interior_nosotros">Pequeñas parcelas para grandes vinos</h2>
								<p class="txt_interior_nosotros">
									En Vinaltura elaboramos nuestros vinos con pasión por la cultura vinícola, apoyados en la innovación tecnológica, el factor humano calificado y las mejores prácticas de control y calidad. <br><br>
									Vendimiamos pequeñas cantidades de uva para hacer micro-vinificaciones, con unamínima intervención enológica, logrando elaborar grandes vinos que expresan la tipicidad de la región, la diversidad de la vid, la identidad del terruño y la personalidad de Vinaltura.
								</p>
							</div>
						</div>

						<div class="centrado">
							<p class="txt_lista_nosotros txt_lista_nosotros_mov" op="5">VITICULTURA EXTREMA</p>
							<div class="cont_datos_nosotros cont_viticultura_extrema_mov animated fadeIn oculto cont_datos_nosotros_mov">
								<h2 class="titulo_interior_nosotros">Pequeñas parcelas para grandes vinos</h2>
								<p class="txt_interior_nosotros">
									En Vinaltura elaboramos nuestros vinos con pasión por la cultura vinícola, apoyados en la innovación tecnológica, el factor humano calificado y las mejores prácticas de control y calidad. <br><br>
									Vendimiamos pequeñas cantidades de uva para hacer micro-vinificaciones, con unamínima intervención enológica, logrando elaborar grandes vinos que expresan la tipicidad de la región, la diversidad de la vid, la identidad del terruño y la personalidad de Vinaltura.
								</p>
							</div>
						</div>

					</div>

				</div>

				<div class="bottom_der">

					<div class="fondo_blanco_opaco">
						<div class="espacio_quienes_somos"></div>

						<div class="cont_datos_nosotros cont_quines_somos animated fadeInRight">

							<h2 class="titulo_interior_nosotros">Quiénes somos</h2>

							<p class="txt_interior_nosotros">
								<b>“Trascender a través del tiempo, con un legado familiar.”</b> <br><br>
								Somos un proyecto familiar, enfocado a la producción de Vino Mexicano de calidad, que busca
								reflejar la esencia del terruño, el respeto al entorno y a la Naturaleza, así como una gran paciencia,
								pasión, arte y tradición en su elaboración.

							<div class="linea_interior_texto"></div>

							</p>

						</div>

						<div class="cont_datos_nosotros cont_manifiesto oculto animated fadeInRight">

							<h2 class="titulo_interior_nosotros">Manifiesto</h2>

							<p class="txt_interior_nosotros">
								<b>SOÑAMOS</b> con trascender en el proyecto familiar de hacer vino.<br><br>
								<b>PENSAMOS</b> que Querétaro tiene el potencial de convertirse en referente del vino mexicano.<br><br>
								<b>CREEMOS</b> que el vino debe estar siempre en nuestra vida diaria.<br><br>
								<b>AMAMOS</b> el arte de hacer vino.
							<div class="linea_interior_texto"></div>

							</p>

						</div>

						<div class="cont_datos_nosotros cont_viñedo oculto animated fadeInRight">

							<h2 class="titulo_interior_nosotros">Viticultura Razonada</h2>

							<p class="txt_interior_nosotros">
								Vinaltura se localiza en el Valle de Colón, Querétaro y cuenta con 20 hectáreas de viñedo, más de
								100,000 vides y 15 variedades de uva entre blancas y tintas, organizado en pequeñas parcelas y con
								una capacidad de producir más de 200 toneladas de uva para vinificar cada año. <br><br>

								El suelo es franco-arcilloso en la superficie, seguido de una capa de tepetate calcáreo y, finalmente,
								una base pedregosa compuesta de piedra laja y basalto.<br><br>

								Para el manejo del viñedo usamos el enfoque de “viticultura razonada”, respetando el medio
								ambiente y la diversidad biológica de la región.

							<div class="linea_interior_texto"></div>

							</p>

						</div>

						<div class="cont_datos_nosotros cont_viñedo_vinicola oculto animated fadeInRight">

							<h2 class="titulo_interior_nosotros">Pequeñas parcelas para grandes vinos</h2>

							<p class="txt_interior_nosotros">
								En Vinaltura elaboramos nuestros vinos con pasión por la cultura vinícola, apoyados en la
								innovación tecnológica, el factor humano calificado y las mejores prácticas de control y calidad. <br><br>
								Vendimiamos pequeñas cantidades de uva para hacer micro-vinificaciones, con una mínima
								intervención enológica, logrando elaborar grandes vinos que expresan la tipicidad de la región, la
								diversidad de la vid, la identidad del terruño y la personalidad de Vinaltura.

							<div class="linea_interior_texto"></div>

							</p>

						</div>

						<div class="cont_datos_nosotros cont_queretaro_region_de_vino oculto animated fadeInRight">

							<h2 class="titulo_interior_nosotros">Querétaro región de vinos</h2>

							<p class="txt_interior_nosotros">
								Querétaro se ubica alrededor del paralelo 20 de latitud norte, al sur del Trópico de Cáncer, por lo
								que está, técnicamente, fuera de la tradicional “franja del vino”, comprendida entre los 30 y 50
								grados de latitud, para el hemisferio norte. Sin embargo, lo que la región “pierde” por latitud lo
								compensa con “altitud”, pues está a 2,000 metros sobre el nivel del mar.<br><br>

								Además, el clima semidesértico provoca un diferencial térmico (amplitud térmica) entre los días
								calurosos y las noches frescas de hasta 20 grados Centígrados, con temperaturas máximas de 35 °C
								y mínimas de 2 °C, según la estación del año.<br><br>

								Con 400-600 mm de precipitación pluvial y 2,800 horas sol al año, Querétaro tiene las condiciones
								para cultivar uvas blancas y tintas, elaborando vinos de gran calidad.

							<div class="linea_interior_texto"></div>

							</p>

						</div>

						<div class="cont_datos_nosotros cont_viticultura_extrema oculto animated fadeInRight">

							<h2 class="titulo_interior_nosotros">Viticultura extrema</h2>

							<p class="txt_interior_nosotros">
								Por su orografía y diversidad de suelos, Querétaro tiene una gran variedad de microclimas. Además,
								en la región se presentan fenómenos climáticos diversos, tales como: heladas tardías, que coinciden
								con la poda; granizo durante la floración; lluvias durante el período de maduración y vendimia;
								golpes de calor en invierno, todo lo anterior se ha definido en Querétaro como “Viticultura
								Extrema”.

							<div class="linea_interior_texto"></div>

							</p>

						</div>

					</div>

				</div>

			</div>

		</div>

	</section>

	<!-- END MENSAJE - SECCION NUEVA -->

	<!--<div class="espacio_blanco"></div>-->
	<!-- <div class="espacio_blanco_movil"></div>
	<div class="espacio_blanco_movil"></div> -->

	<!-- SECCION VIDEO -->

	<section class="video">

		<div style="width: 90%; display:none" id="videodiv">
			<img src="img/logos/video.jpg" style="height:90%; visibility:hidden" alt="Vinos Vinaltura" loading="lazy">
			<iframe title="reproductor de video vinaltura" pHandler="ytiframe" pHideControls="0" src="" allowfullscreen="" wmode="Opaque" frameborder="0" height="100%" width="100%" style="position: absolute; left:0; top:0" id="videoInicio1" name="videoInicio1"></iframe>
		</div>

		<a style="width:100%; cursor:pointer; z-index:2; display:inline-block" id="clicVideo1" href="https://www.youtube.com/embed/jeYsFcbagXs?rel=0&amp;showinfo=0&amp;autoplay=1&amp;controls=0&amp;modestbranding=1" target="videoInicio1" onclick="document.getElementById('clicVideo1').style.display = 'none'; document.getElementById('videodiv').style.display = '';" rel="noopener noreferrer">
			<div style="width:100%; height:100%; position:relative; display: flex; justify-content: center; align-items: center;">
				<img src="assets/img/slider/1.png" alt="Vinaltura" loading="lazy">
				<img src="assets/img/svg/play.svg" alt="Video Vinaltura" class="img_play_video">
			</div>
		</a>

	</section>

	<!-- END SECCION VIDEO -->

	<!-- NOSOTROS -->

	<div class="espacio_blanco_movil"></div>

	<div class="espacio_blanco"></div>

	<div class="espacio_blanco_movil"></div>
	<div class="espacio_blanco_movil"></div>

	<!-- VINOS DESKTOP -->

	<section class="vinos">

		<div class="lista_vinos lista_vinos_bajio">
			<span class="lista_vinos_opc lista_vinos_active" op="1">BAJÍO</span> |
			<span class="lista_vinos_opc" op="2">VARIETALES</span> |
			<span class="lista_vinos_opc" op="3">TERRUÑOS</span> |
			<span class="lista_vinos_opc" op="4">EDICIONES ESPECIALES</span>
		</div>

		<div class="lista_vinos_sub lista_tipos_vino_bajio lista_tipos_vino">
			<!--<span class="sub1 lista_tipos_vino_opc lista_tipos_vino_active" op="1">Tinto Bajío</span>
			<span class="sub2 lista_tipos_vino_opc" op="2">Espuma Rosé</span>
			<span class="sub3 lista_tipos_vino_opc" op="3">Blanco</span>
			<span class="sub4 lista_tipos_vino_opc" op="4">Rosé Bajío</span>-->
			<span class="sub3 lista_tipos_vino_opc lista_tipos_vino_active" op="3">Blanco Bajío</span>
			<span class="sub4 lista_tipos_vino_opc" op="4">Rosé Bajío</span>
			<span class="sub1 lista_tipos_vino_opc" op="1">Tinto Bajío</span>
			<span class="sub2 lista_tipos_vino_opc" op="2">Espuma Rosé</span>
			<span class="sub2 lista_tipos_vino_opc" op="20"> Blanc de Blancs</span>
		</div>

		<div class="lista_vinos_sub lista_tipos_vino_varietales lista_tipos_vino oculto">
			<span class="sub8 lista_tipos_vino_opc lista_tipos_vino_active" op="7">CB</span>
			<span class="sub12 lista_tipos_vino_opc" op="12">GW</span>
			<span class="sub6 lista_tipos_vino_opc" op="6">RS</span>
			<span class="sub7 lista_tipos_vino_opc" op="5">SB</span>
			<span class="sub5 lista_tipos_vino_opc" op="8">ML</span>
			<span class="sub17 lista_tipos_vino_opc" op="19">MR</span>
			
			<span class="sub17 lista_tipos_vino_opc" op="21">CH</span>
			
		</div>

		<div class="lista_vinos_sub lista_tipos_vino_terruños lista_tipos_vino oculto">
			<span class="sub9 lista_tipos_vino_opc" op="15">Blanco</span>
			<span class="sub10 lista_tipos_vino_opc" op="16">Bajio</span>
			<span class="sub11 lista_tipos_vino_opc" op="9">Arroyo</span>
			<span class="sub12 lista_tipos_vino_opc" op="10">Ladera</span>
			<span class="sub13 lista_tipos_vino_opc" op="11">Dos terruños</span>
		</div>

		<div class="lista_vinos_sub lista_tipos_vino_ensayos lista_tipos_vino oculto">
			<span class="sub14 lista_tipos_vino_opc" op="17">GW Ancestral</span>
			<span class="sub15 lista_tipos_vino_opc" op="18">GW Dulce</span>
			<span class="sub16 lista_tipos_vino_opc" op="13">Un par de ... (Blanco)</span>
			<span class="sub22 lista_tipos_vino_opc" op="22">Un par de ... (Rosé)</span>
			<span class="sub17 lista_tipos_vino_opc" op="14">GW Tradicional</span>
		</div>

		<div class="cont_vino_caracteristicas zona_bajio">

			<!-- vino 1 -->

			<div class="datos vino1 oculto">
				<div class="nombre_vino">
					<p class="txt_nombre_vino"><span class="bold">Tinto Bajio </span></p>
				</div>
				<div class="caracteristicas">
					<div class="car_cont_izq">
						<div>
							<p class="opc">Familia:</p>
							<p class="info_opc">Bajío.</p>
						</div>
						<div>
							<p class="opc">Origen:</p>
							<p class="info_opc">Valle de Colón, QRO.</p>
						</div>
						<div>
							<p class="opc">Varietal:</p>
							<p class="info_opc">Tempranillo, Merlot, Petit Syrah.</p>
						</div>
						<div>
							<p class="opc">Fermentación:</p>
							<p class="info_opc">Tanques de acero inoxidable.</p>
						</div>
						<div>
							<p class="opc">Añejamiento:</p>
							<p class="info_opc">12 meses en barrica de roble francés y americano, mínimo 6 meses de botella.</p>
						</div>
						<div>
							<p class="opc">Maridaje:</p>
							<p class="info_opc">Gorditas de migajas y quesadillas con guisados.</p>
						</div>
					</div>
					<div class="car_cont_der">
						<div>
							<p class="opc">Vista:</p>
							<p class="info_opc">Capa de color media, con tonalidades rojo carmesí con ribete color marrón, alta limpidez y brillantez.</p>
						</div>
						<div>
							<p class="opc">Olfato:</p>
							<p class="info_opc">Buena intensidad aromática, destacando notas especiadas y herbales tales como pimienta, canela, eucalipto.</p>
						</div>
						<div>
							<p class="opc">Gusto:</p>
							<p class="info_opc">Buena acidez, tanino sedoso y carnoso, con agradable permanencia en boca.</p>
						</div>

						<div>
							<br><br>
							<a href="assets/pdf/Bajío Tinto Bajío.pdf" target="_blank" class="btn_ficha_tecnica" rel="noopener noreferrer">
								<!--<img src="assets/img/svg/flecha_abajo.svg" class="flecha_abajo">-->
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.79 23.64" class="flecha_abajo">
									<defs>
										<style>
											.fa-1 {
												fill: none;
												stroke: #fff;
												stroke-miterlimit: 10;
												stroke-width: 2.5px;
											}
										</style>
									</defs>
									<title>flecha_abajo</title>
									<g id="Capa_2" data-name="Capa 2">
										<g id="Capa_1-2" data-name="Capa 1">
											<polyline class="fa-1" points="0.81 14.29 9.89 22 18.98 14.29" />
											<line class="fa-1" x1="9.89" y1="22.66" x2="9.89" />
										</g>
									</g>
								</svg>
								FICHA TÉCNICA
							</a>
						</div>

					</div>
				</div>
			</div>

			<!-- end vino 1 -->

			<!-- vino 2 -->

			<div class="datos vino2 oculto">

				<div class="nombre_vino">
					<p class="txt_nombre_vino"><span class="bold">Espuma Rosé</span></p>
				</div>
				<div class="caracteristicas">
					<div class="car_cont_izq">
						<div>
							<p class="opc">Familia:</p>
							<p class="info_opc">Bajío.</p>
						</div>
						<div>
							<p class="opc">Origen:</p>
							<p class="info_opc">Valle de Colón, QRO.</p>
						</div>
						<div>
							<p class="opc">Varietal:</p>
							<p class="info_opc">Syrah y Chardonnay.</p>
						</div>
						<div>
							<p class="opc">Fermentación:</p>
							<p class="info_opc">Método tradicional.</p>
						</div>
						<div>
							<p class="opc">Añejamiento:</p>
							<p class="info_opc">18 meses en contacto con lías.</p>
						</div>
						<div>
							<p class="opc">Maridaje:</p>
							<p class="info_opc">Barbacoa de borrego, tacos de carnitas y moles.</p>
						</div>
					</div>
					<div class="car_cont_der">
						<div>
							<p class="opc">Vista:</p>
							<p class="info_opc">Color rosa salmón, formando en el centro rosarios de burbujas muy finas para terminar en la superficie con una elegante corona.</p>
						</div>
						<div>
							<p class="opc">Olfato:</p>
							<p class="info_opc">Intensidad aromática media, carácter frutal destacando notas como arándano, frutos secos como nuez y almendras.</p>
						</div>
						<div>
							<p class="opc">Gusto:</p>
							<p class="info_opc">Vino con un ataque amplio al paladar, buena acidez fresco con una burbuja muy delicada.</p>
						</div>

						<div>
							<br><br>
							<a href="assets/pdf/Espuma-Bajio-Rose.pdf" target="_blank" class="btn_ficha_tecnica" rel="noopener noreferrer">
								<!--<img src="assets/img/svg/flecha_abajo.svg" class="flecha_abajo">-->
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.79 23.64" class="flecha_abajo">
									<defs>
										<style>
											.fa-1 {
												fill: none;
												stroke: #fff;
												stroke-miterlimit: 10;
												stroke-width: 2.5px;
											}
										</style>
									</defs>
									<title>flecha_abajo</title>
									<g id="Capa_2" data-name="Capa 2">
										<g id="Capa_1-2" data-name="Capa 1">
											<polyline class="fa-1" points="0.81 14.29 9.89 22 18.98 14.29" />
											<line class="fa-1" x1="9.89" y1="22.66" x2="9.89" />
										</g>
									</g>
								</svg>
								FICHA TÉCNICA 2018
							</a>
						</div>

						<div>
							<br><br>
							<a href="assets/pdf/Espuma-Bajio-Rose-2019.pdf" target="_blank" class="btn_ficha_tecnica" rel="noopener noreferrer">
								<!--<img src="assets/img/svg/flecha_abajo.svg" class="flecha_abajo">-->
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.79 23.64" class="flecha_abajo">
									<defs>
										<style>
											.fa-1 {
												fill: none;
												stroke: #fff;
												stroke-miterlimit: 10;
												stroke-width: 2.5px;
											}
										</style>
									</defs>
									<title>flecha_abajo</title>
									<g id="Capa_2" data-name="Capa 2">
										<g id="Capa_1-2" data-name="Capa 1">
											<polyline class="fa-1" points="0.81 14.29 9.89 22 18.98 14.29" />
											<line class="fa-1" x1="9.89" y1="22.66" x2="9.89" />
										</g>
									</g>
								</svg>
								FICHA TÉCNICA 2019
							</a>
						</div>

					</div>
				</div>

			</div>

			<!-- end vino 2 -->

			<!-- vino 3 -->

			<div class="datos vino3">

				<div class="nombre_vino">
					<p class="txt_nombre_vino"><span class="bold">Blanco Bajío</span></p>
				</div>
				<div class="caracteristicas">
					<div class="car_cont_izq">
						<div>
							<p class="opc">Familia:</p>
							<p class="info_opc">Bajío.</p>
						</div>
						<div>
							<p class="opc">Origen:</p>
							<p class="info_opc">Valle de Colón, QRO.</p>
						</div>
						<div>
							<p class="opc">Varietal:</p>
							<p class="info_opc">Chenin Blanc, Sauvignon Blanc, Chardonnay, Riesling.</p>
						</div>
						<div>
							<p class="opc">Fermentación:</p>
							<p class="info_opc">Tanques de acero inoxidable.</p>
						</div>
						<div>
							<p class="opc">Añejamiento:</p>
							<p class="info_opc">Sin Barrica, mínimo 6 meses de botella.</p>
						</div>
						<div>
							<p class="opc">Maridaje:</p>
							<p class="info_opc">Queso con miel, pescado almendrado, cacahuates tostados.</p>
						</div>
					</div>
					<div class="car_cont_der">
						<div>
							<p class="opc">Vista:</p>
							<p class="info_opc">Vino limpio y brillante color verdoso y un ribete plateado.</p>
						</div>
						<div>
							<p class="opc">Olfato:</p>
							<p class="info_opc">Intensidad aromática alta de carácter frutal destacando notas de melón verde, manzana, membrillo y algunas notas florales como la manzanilla.</p>
						</div>
						<div>
							<p class="opc">Gusto:</p>
							<p class="info_opc">Vino con una muy buena acidez, fresco y reafirmando las notas frutales en el retrogusto.</p>
						</div>

						<div>
							<br><br>
							<a href="assets/pdf/Bajío Blanco Bajío.pdf" target="_blank" class="btn_ficha_tecnica" rel="noopener noreferrer">
								<!--<img src="assets/img/svg/flecha_abajo.svg" class="flecha_abajo">-->
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.79 23.64" class="flecha_abajo">
									<defs>
										<style>
											.fa-1 {
												fill: none;
												stroke: #fff;
												stroke-miterlimit: 10;
												stroke-width: 2.5px;
											}
										</style>
									</defs>
									<title>flecha_abajo</title>
									<g id="Capa_2" data-name="Capa 2">
										<g id="Capa_1-2" data-name="Capa 1">
											<polyline class="fa-1" points="0.81 14.29 9.89 22 18.98 14.29" />
											<line class="fa-1" x1="9.89" y1="22.66" x2="9.89" />
										</g>
									</g>
								</svg>
								FICHA TÉCNICA
							</a>
						</div>

					</div>
				</div>

			</div>

			<!-- end vino 3 -->

			<!-- vino 4 -->

			<div class="datos vino4 oculto">

				<div class="nombre_vino">
					<p class="txt_nombre_vino"><span class="bold">Rosé Bajío</span></p>
				</div>
				<div class="caracteristicas">
					<div class="car_cont_izq">
						<div>
							<p class="opc">Familia:</p>
							<p class="info_opc">Bajío</p>
						</div>
						<div>
							<p class="opc">Origen:</p>
							<p class="info_opc">Valle de Colón, QRO.</p>
						</div>
						<div>
							<p class="opc">Varietal:</p>
							<p class="info_opc">Tempranillo, Nebbiolo.</p>
						</div>
						<div>
							<p class="opc">Fermentación:</p>
							<p class="info_opc">Tanques de acero inoxidable.</p>
						</div>
						<div>
							<p class="opc">Añejamiento:</p>
							<p class="info_opc">Sin Barrica, mínimo 6 meses de botella.</p>
						</div>
						<div>
							<p class="opc">Maridaje:</p>
							<p class="info_opc">Comida agridulce, cochinita pibil y tacos al pastor.</p>
						</div>
					</div>
					<div class="car_cont_der">
						<div>
							<p class="opc">Vista:</p>
							<p class="info_opc">Vino brillante, color rosa cereza con ribete color marrón.</p>
						</div>
						<div>
							<p class="opc">Olfato:</p>
							<p class="info_opc">Vino con buena intensidad aromática con carácter frutal, notas de ciruela, mamey y papa dulce.</p>
						</div>
						<div>
							<p class="opc">Gusto:</p>
							<p class="info_opc">Vino fresco con buena acidez y final con ligera presencia de tanino.</p>
						</div>

						<div>
							<br><br>
							<a href="assets/pdf/Bajío Rosé Bajío.pdf" target="_blank" class="btn_ficha_tecnica" rel="noopener noreferrer">
								<!--<img src="assets/img/svg/flecha_abajo.svg" class="flecha_abajo">-->
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.79 23.64" class="flecha_abajo">
									<defs>
										<style>
											.fa-1 {
												fill: none;
												stroke: #fff;
												stroke-miterlimit: 10;
												stroke-width: 2.5px;
											}
										</style>
									</defs>
									<title>flecha_abajo</title>
									<g id="Capa_2" data-name="Capa 2">
										<g id="Capa_1-2" data-name="Capa 1">
											<polyline class="fa-1" points="0.81 14.29 9.89 22 18.98 14.29" />
											<line class="fa-1" x1="9.89" y1="22.66" x2="9.89" />
										</g>
									</g>
								</svg>
								FICHA TÉCNICA
							</a>
						</div>

					</div>
				</div>

			</div>

			<!-- end vino 4 -->


			<!-- vino 5 -->

			<div class="datos vino5 oculto">

				<div class="nombre_vino">
					<p class="txt_nombre_vino"><span class="bold">Espuma Bajío Blanc de Blancs</span></p>
				</div>

				<div class="caracteristicas">
					<div class="car_cont_izq">

						<div>
							<p class="opc">Familia:</p>
							<p class="info_opc">Bajío</p>
						</div>
						<div>
							<p class="opc">Varietal:</p>
							<p class="info_opc">50% Chardonnay – 50% Chenin Blanc</p>
						</div>
						<div>
							<p class="opc">Region:</p>
							<p class="info_opc">Valle de Colón, Querétaro</p>
						</div>
						<div>
							<p class="opc">Bodega:</p>
							<p class="info_opc">Vinaltura.</p>
						</div>
						<div>
							<p class="opc">Maridaje:</p>
							<p class="info_opc">Ensaladas de frutas, tacos de pescado, pay de limón, barbacoa, quesadillas con flor de calabaza, huitlacoche y rajas. </p>
						</div>
						<!-- <div>
							<p class="opc">Elaboración:</p>
							<p class="info_opc">Selección de uva manual, con un prensado suave, fermentación controlada en tanque de acero inoxidable, con una crianza de 12 meses en barrica de roble francés.</p>
						</div> -->

					</div>
					<div class="car_cont_der">
						<div>
							<p class="opc">Vista:</p>
							<p class="info_opc">Amarillo paja con destellos dorados con una burbuja fina, constante y abundante; rosarios de burbujas muy marcados que terminan en una corona definida, resaltando la limpidez y la brillantez. </p>
						</div>
						<div>
							<p class="opc">Olfato:</p>
							<p class="info_opc">Intensidad aroma alta donde resaltan notas frutales y dulces como la manzana, miel, semillas y frutos secos.</p>
						</div>
						<div>
							<p class="opc">Gusto:</p>
							<p class="info_opc">Ataque amplio al paladar con una explosión de burbuja muy marcada y agradable, fresco, un vino seco y con una acidez jugosa.</p>
						</div>
						<br><br>

						<div>

							<a href="assets/pdf/blanc_de_blancs.pdf" target="_blank" class="btn_ficha_tecnica" rel="noopener noreferrer">
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.79 23.64" class="flecha_abajo">
									<defs>
										<style>
											.fa-1 {
												fill: none;
												stroke: #fff;
												stroke-miterlimit: 10;
												stroke-width: 2.5px;
											}
										</style>
									</defs>
									<title>flecha_abajo</title>
									<g id="Capa_2" data-name="Capa 2">
										<g id="Capa_1-2" data-name="Capa 1">
											<polyline class="fa-1" points="0.81 14.29 9.89 22 18.98 14.29" />
											<line class="fa-1" x1="9.89" y1="22.66" x2="9.89" />
										</g>
									</g>
								</svg>
								FICHA TÉCNICA
							</a>

						</div>
						<br>
						<div>

							<a href="assets/pdf/Ficha_Tecnica_Espuma_Bajio_Blanc_de_Blancs_2019.pdf" target="_blank" class="btn_ficha_tecnica" rel="noopener noreferrer">
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.79 23.64" class="flecha_abajo">
									<defs>
										<style>
											.fa-1 {
												fill: none;
												stroke: #fff;
												stroke-miterlimit: 10;
												stroke-width: 2.5px;
											}
										</style>
									</defs>
									<title>flecha_abajo</title>
									<g id="Capa_2" data-name="Capa 2">
										<g id="Capa_1-2" data-name="Capa 1">
											<polyline class="fa-1" points="0.81 14.29 9.89 22 18.98 14.29" />
											<line class="fa-1" x1="9.89" y1="22.66" x2="9.89" />
										</g>
									</g>
								</svg>
								FICHA TÉCNICA 2019
							</a>

						</div>

					</div>
				</div>

			</div>

			<!-- end vino 5 -->

			<!-- imgen -->

			<div class="imagen">
				<!-- <img class="uvas" src="assets/img/svg/uvas.svg" alt=""> -->
				<div class="uvas layer2 uvas_verdes uvas_bajio"></div>
				<!-- <img class="circulo layer2" src="assets/img/circulo.png" alt=""> -->
				<img class="vino_img layer" src="assets/img/vinos/bajio/blanco-sin-año.png" alt="Vinaltura-espuma-rose-bajio">
				<!-- <img class="puntos" src="assets/img/puntos.png" alt=""> -->
				<div class="btn_tienda"> <a style="text-decoration: none; color:white;" href="https://tiendavinaltura.mx/" target="_blank">COMPRAR</a></div>

				<div class="cont_nombre_tipo bajios_ubic">
					<p>Bajío<br>
				</div>
			</div>

			<!-- end imagen -->

		</div>

		<div class="cont_vino_caracteristicas zona_varietales oculto">

			<!-- vino 1 -->

			<div class="datos vino1 oculto">
				<div class="nombre_vino">
					<p class="txt_nombre_vino"><span class="bold">Vinaltura Sauvignon Blanc</span></p>
				</div>
				<div class="caracteristicas">
					<div class="car_cont_izq">
						<div>
							<p class="opc">Familia:</p>
							<p class="info_opc">Varietales.</p>
						</div>
						<div>
							<p class="opc">Origen:</p>
							<p class="info_opc">Valle de Colón, QRO.</p>
						</div>
						<div>
							<p class="opc">Varietal:</p>
							<p class="info_opc">Sauvignon Blanc.</p>
						</div>
						<div>
							<p class="opc">Fermentación:</p>
							<p class="info_opc">Tanques de acero inoxidable.</p>
						</div>
						<div>
							<p class="opc">Añejamiento:</p>
							<p class="info_opc">Sin Barrica, mínimo 6 meses de botella.</p>
						</div>
						<div>
							<p class="opc">Maridaje:</p>
							<p class="info_opc">Mariscos y aves con salsas blancas.</p>
						</div>
					</div>
					<div class="car_cont_der">
						<div>
							<p class="opc">Vista:</p>
							<p class="info_opc">Limpio y brillante, color amarillo pajizo con un ribete color dorado, con una densidad aparente media.</p>
						</div>
						<div>
							<p class="opc">Olfato:</p>
							<p class="info_opc">Intensidad aromática alta de carácter floral y frutal sobresaliendo notas de lima, manzanilla y miel.</p>
						</div>
						<div>
							<p class="opc">Gusto:</p>
							<p class="info_opc">Buena estructura y acidez, bastante fresco y equilibrado.</p>
						</div>

						<div>
							<br><br>
							<a href="assets/pdf/Varietales SB.pdf" target="_blank" class="btn_ficha_tecnica" rel="noopener noreferrer">
								<!--<img src="assets/img/svg/flecha_abajo.svg" class="flecha_abajo">-->
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.79 23.64" class="flecha_abajo">
									<defs>
										<style>
											.fa-1 {
												fill: none;
												stroke: #fff;
												stroke-miterlimit: 10;
												stroke-width: 2.5px;
											}
										</style>
									</defs>
									<title>flecha_abajo</title>
									<g id="Capa_2" data-name="Capa 2">
										<g id="Capa_1-2" data-name="Capa 1">
											<polyline class="fa-1" points="0.81 14.29 9.89 22 18.98 14.29" />
											<line class="fa-1" x1="9.89" y1="22.66" x2="9.89" />
										</g>
									</g>
								</svg>
								FICHA TÉCNICA
							</a>
						</div>

					</div>
				</div>
			</div>

			<!-- end vino 1 -->

			<!-- vino 2 -->

			<div class="datos vino2 oculto">

				<div class="nombre_vino">
					<p class="txt_nombre_vino"><span class="bold">Vinaltura Riesling</span></p>
				</div>
				<div class="caracteristicas">
					<div class="car_cont_izq">
						<div>
							<p class="opc">Familia:</p>
							<p class="info_opc">Varietales.</p>
						</div>
						<div>
							<p class="opc">Origen:</p>
							<p class="info_opc">Valle de Colón, QRO.</p>
						</div>
						<div>
							<p class="opc">Varietal:</p>
							<p class="info_opc">Riesling.</p>
						</div>
						<div>
							<p class="opc">Fermentación:</p>
							<p class="info_opc">Tanques de acero inoxidable.</p>
						</div>
						<div>
							<p class="opc">Añejamiento:</p>
							<p class="info_opc">Sin Barrica, mínimo 6 meses de botella.</p>
						</div>
						<div>
							<p class="opc">Maridaje:</p>
							<p class="info_opc">Ceviche verde, pepitas garapiñadas.</p>
						</div>
					</div>
					<div class="car_cont_der">
						<div>
							<p class="opc">Vista:</p>
							<p class="info_opc">Vino limpio y brillante, color amarillo paja con destellos plateados.</p>
						</div>
						<div>
							<p class="opc">Olfato:</p>
							<p class="info_opc">Intensidad aromática media, notas cítricas resaltando la cascara de limón y la flor de naranjo.</p>
						</div>
						<div>
							<p class="opc">Gusto:</p>
							<p class="info_opc">Vino fresco con un ataque amplio al paladar, con una acidez balanceada y larga permanencia en boca.</p>
						</div>

						<div>
							<br><br>
							<a href="assets/pdf/Varietales RS.pdf" target="_blank" class="btn_ficha_tecnica" rel="noopener noreferrer">
								<!--<img src="assets/img/svg/flecha_abajo.svg" class="flecha_abajo">-->
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.79 23.64" class="flecha_abajo">
									<defs>
										<style>
											.fa-1 {
												fill: none;
												stroke: #fff;
												stroke-miterlimit: 10;
												stroke-width: 2.5px;
											}
										</style>
									</defs>
									<title>flecha_abajo</title>
									<g id="Capa_2" data-name="Capa 2">
										<g id="Capa_1-2" data-name="Capa 1">
											<polyline class="fa-1" points="0.81 14.29 9.89 22 18.98 14.29" />
											<line class="fa-1" x1="9.89" y1="22.66" x2="9.89" />
										</g>
									</g>
								</svg>
								FICHA TÉCNICA
							</a>
						</div>

					</div>
				</div>

			</div>

			<!-- end vino 2 -->

			<!-- vino 3 -->

			<div class="datos vino3 oculto">

				<div class="nombre_vino">
					<p class="txt_nombre_vino"><span class="bold">Vinaltura Chenin Blanc</span></p>
				</div>
				<div class="caracteristicas">
					<div class="car_cont_izq">
						<div>
							<p class="opc">Familia:</p>
							<p class="info_opc">Varietales.</p>
						</div>
						<div>
							<p class="opc">Origen:</p>
							<p class="info_opc">Valle de Colón, QRO.</p>
						</div>
						<div>
							<p class="opc">Varietal:</p>
							<p class="info_opc">Chenin Blanc.</p>
						</div>
						<div>
							<p class="opc">Fermentación:</p>
							<p class="info_opc">Tanques de acero inoxidable.</p>
						</div>
						<div>
							<p class="opc">Añejamiento:</p>
							<p class="info_opc">Sin Barrica, mínimo 6 meses de botella.</p>
						</div>
						<div>
							<p class="opc">Maridaje:</p>
							<p class="info_opc">Tacos estilo Ensenada y Pay de piña.</p>
						</div>
					</div>
					<div class="car_cont_der">
						<div>
							<p class="opc">Vista:</p>
							<p class="info_opc">Amarillo paja con destellos plateados, limpio y brillante.</p>
						</div>
						<div>
							<p class="opc">Olfato:</p>
							<p class="info_opc">Buena intensidad aromática, de carácter frutal, destacando notas como piña, naranja, durazno y miel.</p>
						</div>
						<div>
							<p class="opc">Gusto:</p>
							<p class="info_opc">Fresco con una acidez balanceada y buena permanencia en boca.</p>
						</div>

						<div>
							<br><br>
							<a href="assets/pdf/Varietales CB.pdf" target="_blank" class="btn_ficha_tecnica" rel="noopener noreferrer">
								<!--<img src="assets/img/svg/flecha_abajo.svg" class="flecha_abajo">-->
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.79 23.64" class="flecha_abajo">
									<defs>
										<style>
											.fa-1 {
												fill: none;
												stroke: #fff;
												stroke-miterlimit: 10;
												stroke-width: 2.5px;
											}
										</style>
									</defs>
									<title>flecha_abajo</title>
									<g id="Capa_2" data-name="Capa 2">
										<g id="Capa_1-2" data-name="Capa 1">
											<polyline class="fa-1" points="0.81 14.29 9.89 22 18.98 14.29" />
											<line class="fa-1" x1="9.89" y1="22.66" x2="9.89" />
										</g>
									</g>
								</svg>
								FICHA TÉCNICA
							</a>
						</div>

					</div>
				</div>

			</div>

			<!-- end vino 3 -->

			<!-- vino 4 -->

			<div class="datos vino4 oculto">

				<div class="nombre_vino">
					<p class="txt_nombre_vino"><span class="bold">Vinaltura Malbec</span></p>
				</div>
				<div class="caracteristicas">
					<div class="car_cont_izq">
						<div>
							<p class="opc">Familia:</p>
							<p class="info_opc">Varietales.</p>
						</div>
						<div>
							<p class="opc">Origen:</p>
							<p class="info_opc">Valle de Colón, QRO.</p>
						</div>
						<div>
							<p class="opc">Varietal:</p>
							<p class="info_opc">Malbec.</p>
						</div>
						<div>
							<p class="opc">Fermentación:</p>
							<p class="info_opc">Tanques de acero inoxidable.</p>
						</div>
						<div>
							<p class="opc">Añejamiento:</p>
							<p class="info_opc">12 meses de barrica de roble francés y americano, mínimo 6 meses de botella.</p>
						</div>
						<div>
							<p class="opc">Maridaje:</p>
							<p class="info_opc">Quesos suaves, ensalada de betabel con queso de cabra, pay de queso con frutos rojos.</p>
						</div>
					</div>
					<div class="car_cont_der">
						<div>
							<p class="opc">Vista:</p>
							<p class="info_opc">Capa de color alta, color violáceo intenso, con un ribete color granate, vino limpio y brillante con una densidad aparente media alta.</p>
						</div>
						<div>
							<p class="opc">Olfato:</p>
							<p class="info_opc">Intensidad aromática elevada, notas frutales, dando como resultado notas compotadas de zarzamora y fresa.</p>
						</div>
						<div>
							<p class="opc">Gusto:</p>
							<p class="info_opc">Ataque amplio al paladar con un tanino elegante, buena acidez y muy fresco.</p>
						</div>

						<div>
							<br><br>
							<a href="assets/pdf/Malbec-2019.pdf" target="_blank" class="btn_ficha_tecnica" rel="noopener noreferrer">
								<!--<img src="assets/img/svg/flecha_abajo.svg" class="flecha_abajo">-->
								<svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 19.79 23.64" class="flecha_abajo">
									<defs>
										<style>
											.fa-1 {
												fill: none;
												stroke: #fff;
												stroke-miterlimit: 10;
												stroke-width: 2.5px;
											}
										</style>
									</defs>
									<title>flecha_abajo</title>
									<g id="Capa_2" data-name="Capa 2">
										<g id="Capa_1-2" data-name="Capa 1">
											<polyline class="fa-1" points="0.81 14.29 9.89 22 18.98 14.29" />
											<line class="fa-1" x1="9.89" y1="22.66" x2="9.89" />
										</g>
									</g>
								</svg>
								FICHA TÉCNICA
							</a>
						</div>

					</div>
				</div>

			</div>

			<!-- end vino 4 -->



			<!-- vino 5 -->

			<div class="datos vino5 oculto">

				<div class="nombre_vino">
					<p class="txt_nombre_vino"><span class="bold">Vinaltura Gewürztraminer</span></p>
				</div>
				<div class="caracteristicas">
					<div class="car_cont_izq">
						<div>
							<p class="opc">Familia:</p>
							<p class="info_opc">Varietales.</p>
						</div>
						<div>
							<p class="opc">Origen:</p>
							<p class="info_opc">Valle de Colón, QRO.</p>
						</div>
						<div>
							<p class="opc">Varietal:</p>
							<p class="info_opc">Gewürztraminer</p>
						</div>
						<div>
							<p class="opc">Fermentación:</p>
							<p class="info_opc">Tanques de acero inoxidable</p>
						</div>
						<div>
							<p class="opc">Añejamiento:</p>
							<p class="info_opc">Sin Barrica, mínimo 12 meses de botella</p>
						</div>
						<div>
							<p class="opc">Maridaje:</p>
							<p class="info_opc">Quesos fuertes con miel, chocolate amargo.</p>
						</div>
					</div>
					<div class="car_cont_der">
						<div>
							<p class="opc">Vista:</p>
							<p class="info_opc">Limpio y brillante, color amarillo paja con destellos dorados, con una densidad aparente media alta.</p>
						</div>
						<div>
							<p class="opc">Olfato:</p>
							<p class="info_opc">Intensidad aromática alta de carácter floral destacando notas como pétalos de rosa, lavanda y azucena. Ligeras notas a frutos secos.</p>
						</div>
						<div>
							<p class="opc">Gusto:</p>
							<p class="info_opc">Ataque amplio al paladar, untuoso y sedoso, reafirmando la floralidad de nariz.</p>
						</div>

						<div>
							<br><br>
							<a href="assets/pdf/GW-2020-Dulce.pdf" target="_blank" class="btn_ficha_tecnica" rel="noopener noreferrer">
								<!--<img src="assets/img/svg/flecha_abajo.svg" class="flecha_abajo">-->
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.79 23.64" class="flecha_abajo">
									<defs>
										<style>
											.fa-1 {
												fill: none;
												stroke: #fff;
												stroke-miterlimit: 10;
												stroke-width: 2.5px;
											}
										</style>
									</defs>
									<title>flecha_abajo</title>
									<g id="Capa_2" data-name="Capa 2">
										<g id="Capa_1-2" data-name="Capa 1">
											<polyline class="fa-1" points="0.81 14.29 9.89 22 18.98 14.29" />
											<line class="fa-1" x1="9.89" y1="22.66" x2="9.89" />
										</g>
									</g>
								</svg>
								FICHA TÉCNICA
							</a>
						</div>

					</div>
				</div>

			</div>

			<!-- end vino 5 -->

			<!-- vino 6 -->

			<div class="datos vino6 oculto">

				<div class="nombre_vino">
					<p class="txt_nombre_vino"><span class="bold">Vinaltura Merlot</span></p>
				</div>
				<div class="caracteristicas">
					<div class="car_cont_izq">

						<div>
							<p class="opc">Familia:</p>
							<p class="info_opc">Varietales.</p>
						</div>
						<div>
							<p class="opc">Origen:</p>
							<p class="info_opc">Valle de Colón, Querétaro</p>
						</div>
						<div>
							<p class="opc">Varietal:</p>
							<p class="info_opc">Merlot.</p>
						</div>
						<div>
							<p class="opc">Fermentación:</p>
							<p class="info_opc">Tanques de acero inoxidable.</p>
						</div>
						<div>
							<p class="opc">Añejamiento:</p>
							<p class="info_opc">12 meses de barrica de roble francés.</p>
						</div>
						<div>
							<p class="opc">Maridaje:</p>
							<p class="info_opc">Costillas asadas, pulpo a las brasas, hamburguesa de res, tacos de legua. </p>
						</div>

					</div>
					<div class="car_cont_der">
						<div>
							<p class="opc">Vista:</p>
							<p class="info_opc">Capa media alta, color rojo carmín un ribete color granate. </p>
						</div>
						<div>
							<p class="opc">Olfato:</p>
							<p class="info_opc">Intensidad aromática media, donde destacan notas frutales, frutales, y herbales, tales como cereza, zarzamora, ciruela pasa, jamaica, hierbabuena y rosas.</p>
						</div>
						<div>
							<p class="opc">Gusto:</p>
							<p class="info_opc">Vino seco con un ataque medio, acidez media con un tanino sutil y elegante.</p>
						</div>
						<br><br>
						<div>

							<a href="assets/pdf/Varietales MR.pdf" target="_blank" class="btn_ficha_tecnica" rel="noopener noreferrer">
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.79 23.64" class="flecha_abajo">
									<defs>
										<style>
											.fa-1 {
												fill: none;
												stroke: #fff;
												stroke-miterlimit: 10;
												stroke-width: 2.5px;
											}
										</style>
									</defs>
									<title>flecha_abajo</title>
									<g id="Capa_2" data-name="Capa 2">
										<g id="Capa_1-2" data-name="Capa 1">
											<polyline class="fa-1" points="0.81 14.29 9.89 22 18.98 14.29" />
											<line class="fa-1" x1="9.89" y1="22.66" x2="9.89" />
										</g>
									</g>
								</svg>
								FICHA TÉCNICA
							</a>
						</div>

					</div>
				</div>

			</div>

			<!-- end vino 6 -->

			<!-- vino 6 -->

			<div class="datos vino7 oculto">

				<div class="nombre_vino">
					<p class="txt_nombre_vino"><span class="bold">Vinaltura Chardonnay</span></p>
				</div>
				<div class="caracteristicas">
					<div class="car_cont_izq">

						<div>
							<p class="opc">Familia:</p>
							<p class="info_opc">Varietales.</p>
						</div>
						<div>
							<p class="opc">Origen:</p>
							<p class="info_opc">Valle de Colón, Querétaro</p>
						</div>
						<div>
							<p class="opc">Varietal:</p>
							<p class="info_opc">100% Chardonnay.</p>
						</div>
						<div>
							<p class="opc">Fermentación:</p>
							<p class="info_opc">Tanques de acero inoxidable.</p>
						</div>
						<div>
							<p class="opc">Añejamiento:</p>
							<p class="info_opc">Sin Barrica, 6 meses en botella.</p>
						</div>
						<div>
							<p class="opc">Maridaje:</p>
							<p class="info_opc">Ensalada con queso de cabra o una vinagreta característica, proteínas suaves, quesos o un postre con buena acidez </p>
						</div>

					</div>
					<div class="car_cont_der">
						<div>
							<p class="opc">Vista:</p>
							<p class="info_opc">Vino limpio, color dorado con destellos cobrizos y densidad media. </p>
						</div>
						<div>
							<p class="opc">Olfato:</p>
							<p class="info_opc">Intensidad aromática media, notas frutales resaltando piña, durazno y mango verde. Dejando una ligera nota a membrillo y caramelo.</p>
						</div>
						<div>
							<p class="opc">Gusto:</p>
							<p class="info_opc">Vino fresco con un ataque amplio al paladar, acidez balanceada y untuoso.</p>
						</div>
						<br><br>
						<div>

							<a href="assets/pdf/ficha_tecnica_ch.pdf" target="_blank" class="btn_ficha_tecnica" rel="noopener noreferrer">
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.79 23.64" class="flecha_abajo">
									<defs>
										<style>
											.fa-1 {
												fill: none;
												stroke: #fff;
												stroke-miterlimit: 10;
												stroke-width: 2.5px;
											}
										</style>
									</defs>
									<title>flecha_abajo</title>
									<g id="Capa_2" data-name="Capa 2">
										<g id="Capa_1-2" data-name="Capa 1">
											<polyline class="fa-1" points="0.81 14.29 9.89 22 18.98 14.29" />
											<line class="fa-1" x1="9.89" y1="22.66" x2="9.89" />
										</g>
									</g>
								</svg>
								FICHA TÉCNICA
							</a>
						</div>

					</div>
				</div>

			</div>

				<!-- end vino 6 -->

			<!-- imgen -->

			<div class="imagen">
				<div class="uvas layer2 uvas_verdes uvas_varietales"></div>
				<img class="vino_img layer" src="assets/img/vinos/varietales/suavignon-blanc-2019.png" alt="Vinaltura-suavignon-blanc-2018-varietales">
				<div class="btn_tienda"> <a style="text-decoration: none; color:white;" href="https://tiendavinaltura.mx/" target="_blank">COMPRAR</a></div>
				<div class="cont_nombre_tipo_va">
					<p>Varie-</p><br>
					<span> tales</span>
				</div>
			</div>

			<!-- end imagen -->

		</div>

		<div class="cont_vino_caracteristicas zona_terruños oculto">

			<!-- vino 1 -->

			<div class="datos vino1">
				<div class="nombre_vino">
					<p class="txt_nombre_vino"><span class="bold">Terruño Arroyo</span></p>
				</div>
				<div class="caracteristicas">
					<div class="car_cont_izq">
						<div>
							<p class="opc">Familia:</p>
							<p class="info_opc">Terruños.</p>
						</div>
						<div>
							<p class="opc">Origen:</p>
							<p class="info_opc">Valle de Guadalupe, B.C.</p>
						</div>
						<div>
							<p class="opc">Varietal:</p>
							<p class="info_opc">Merlot, Cabernet Sauvignon.</p>
						</div>
						<div>
							<p class="opc">Fermentación:</p>
							<p class="info_opc">Tanques de acero inoxidable.</p>
						</div>
						<div>
							<p class="opc">Añejamiento:</p>
							<p class="info_opc">12 meses de barrica de roble francés y americano, mínimo 12 meses de botella.</p>
						</div>
						<div>
							<p class="opc">Maridaje:</p>
							<p class="info_opc">Carnitas y enfrijoladas.</p>
						</div>
					</div>
					<div class="car_cont_der">
						<div>
							<p class="opc">Vista:</p>
							<p class="info_opc">Color granate con un ribete color cereza, vino limpio y brillante con una densidad aparente alta.</p>
						</div>
						<div>
							<p class="opc">Olfato:</p>
							<p class="info_opc">Intensidad aromática elevada destacando notas de frutos como zarzamora y tostados como la canela y nuez.</p>
						</div>
						<div>
							<p class="opc">Gusto:</p>
							<p class="info_opc">Vino redondo, con un ataque amplio al paladar con un tanino sedoso, buena acidez y excelente permanencia en boca.</p>
						</div>

						<div>
							<br>
							<a href="assets/pdf/Terruño-Arroyo-2016.pdf" target="_blank" class="btn_ficha_tecnica" rel="noopener noreferrer">
								<!--<img src="assets/img/svg/flecha_abajo.svg" class="flecha_abajo">-->
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.79 23.64" class="flecha_abajo">
									<defs>
										<style>
											.fa-1 {
												fill: none;
												stroke: #fff;
												stroke-miterlimit: 10;
												stroke-width: 2.5px;
											}
										</style>
									</defs>
									<title>flecha_abajo</title>
									<g id="Capa_2" data-name="Capa 2">
										<g id="Capa_1-2" data-name="Capa 1">
											<polyline class="fa-1" points="0.81 14.29 9.89 22 18.98 14.29" />
											<line class="fa-1" x1="9.89" y1="22.66" x2="9.89" />
										</g>
									</g>
								</svg>
								FICHA TÉCNICA 2016
							</a>
							<br>
							<a href="assets/pdf/Terruños Arroyo.pdf" target="_blank" class="btn_ficha_tecnica" rel="noopener noreferrer">
								<!--<img src="assets/img/svg/flecha_abajo.svg" class="flecha_abajo">-->
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.79 23.64" class="flecha_abajo">
									<defs>
										<style>
											.fa-1 {
												fill: none;
												stroke: #fff;
												stroke-miterlimit: 10;
												stroke-width: 2.5px;
											}
										</style>
									</defs>
									<title>flecha_abajo</title>
									<g id="Capa_2" data-name="Capa 2">
										<g id="Capa_1-2" data-name="Capa 1">
											<polyline class="fa-1" points="0.81 14.29 9.89 22 18.98 14.29" />
											<line class="fa-1" x1="9.89" y1="22.66" x2="9.89" />
										</g>
									</g>
								</svg>
								FICHA TÉCNICA 2017
							</a>
						</div>

					</div>
				</div>
			</div>

			<!-- end vino 1 -->

			<!-- vino 2 -->

			<div class="datos vino2 oculto">

				<div class="nombre_vino">
					<p class="txt_nombre_vino"><span class="bold">Terruño Ladera</span></p>
				</div>
				<div class="caracteristicas">
					<div class="car_cont_izq">
						<div>
							<p class="opc">Familia:</p>
							<p class="info_opc">Terruños.</p>
						</div>
						<div>
							<p class="opc">Origen:</p>
							<p class="info_opc">Valle de Guadalupe, B.C.</p>
						</div>
						<div>
							<p class="opc">Varietal:</p>
							<p class="info_opc">Merlot, Cabernet Sauvignon.</p>
						</div>
						<div>
							<p class="opc">Fermentación:</p>
							<p class="info_opc">Tanques de acero inoxidable.</p>
						</div>
						<div>
							<p class="opc">Añejamiento:</p>
							<p class="info_opc">12 meses de barrica de roble francés y americano, mínimo 12 meses de botella.</p>
						</div>
						<div>
							<p class="opc">Maridaje:</p>
							<p class="info_opc">Asado, ahumados, BBQ.</p>
						</div>
					</div>
					<div class="car_cont_der">
						<div>
							<p class="opc">Vista:</p>
							<p class="info_opc">Color granate con destellos rojizos, limpio, brillante y una capa de color elevada.</p>
						</div>
						<div>
							<p class="opc">Olfato:</p>
							<p class="info_opc">Intensidad aromática elevada destacando notas de moras, mermeladas, especiadas como canela, clavo y torrefactos.</p>
						</div>
						<div>
							<p class="opc">Gusto:</p>
							<p class="info_opc">Vino carnoso y redondo con un ataque amplio al paladar con tanino elevado, buena acidez y excelente permanencia en boca.</p>
						</div>


						<div>
							<br>
							<a href="assets/pdf/Terruño-Ladera-2016.pdf" target="_blank" class="btn_ficha_tecnica" rel="noopener noreferrer">
								<!--<img src="assets/img/svg/flecha_abajo.svg" class="flecha_abajo">-->
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.79 23.64" class="flecha_abajo">
									<defs>
										<style>
											.fa-1 {
												fill: none;
												stroke: #fff;
												stroke-miterlimit: 10;
												stroke-width: 2.5px;
											}
										</style>
									</defs>
									<title>flecha_abajo</title>
									<g id="Capa_2" data-name="Capa 2">
										<g id="Capa_1-2" data-name="Capa 1">
											<polyline class="fa-1" points="0.81 14.29 9.89 22 18.98 14.29" />
											<line class="fa-1" x1="9.89" y1="22.66" x2="9.89" />
										</g>
									</g>
								</svg>
								FICHA TÉCNICA 2016
							</a>
							<br>
							<a href="assets/pdf/Terruños Ladera.pdf" target="_blank" class="btn_ficha_tecnica" rel="noopener noreferrer">
								<!--<img src="assets/img/svg/flecha_abajo.svg" class="flecha_abajo">-->
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.79 23.64" class="flecha_abajo">
									<defs>
										<style>
											.fa-1 {
												fill: none;
												stroke: #fff;
												stroke-miterlimit: 10;
												stroke-width: 2.5px;
											}
										</style>
									</defs>
									<title>flecha_abajo</title>
									<g id="Capa_2" data-name="Capa 2">
										<g id="Capa_1-2" data-name="Capa 1">
											<polyline class="fa-1" points="0.81 14.29 9.89 22 18.98 14.29" />
											<line class="fa-1" x1="9.89" y1="22.66" x2="9.89" />
										</g>
									</g>
								</svg>
								FICHA TÉCNICA 2017
							</a>
						</div>
						<!-- <div>
							<br><br>
							<a href="assets/pdf/Terruños Ladera.pdf" target="_blank" class="btn_ficha_tecnica" rel="noopener noreferrer">
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.79 23.64" class="flecha_abajo">
									<defs>
										<style>
											.fa-1 {
												fill: none;
												stroke: #fff;
												stroke-miterlimit: 10;
												stroke-width: 2.5px;
											}
										</style>
									</defs>
									<title>flecha_abajo</title>
									<g id="Capa_2" data-name="Capa 2">
										<g id="Capa_1-2" data-name="Capa 1">
											<polyline class="fa-1" points="0.81 14.29 9.89 22 18.98 14.29" />
											<line class="fa-1" x1="9.89" y1="22.66" x2="9.89" />
										</g>
									</g>
								</svg>
								FICHA TÉCNICA
							</a>
						</div> -->

					</div>
				</div>

			</div>

			<!-- end vino 2 -->

			<!-- vino 3 -->

			<div class="datos vino3 oculto">

				<div class="nombre_vino">
					<p class="txt_nombre_vino"><span class="bold">Dos Terruños Baja Bajío</span></p>
				</div>
				<div class="caracteristicas">
					<div class="car_cont_izq">
						<div>
							<p class="opc">Familia:</p>
							<p class="info_opc">Terruños.</p>
						</div>
						<div>
							<p class="opc">Origen:</p>
							<p class="info_opc">Valle de Guadalupe, B.C. – Valle de Colón, QRO.</p>
						</div>
						<div>
							<p class="opc">Varietal:</p>
							<p class="info_opc">Cabernet Sauvignon, Merlot, Cabernet Franc, Malbec.</p>
						</div>
						<div>
							<p class="opc">Fermentación:</p>
							<p class="info_opc">Tanques de acero inoxidable.</p>
						</div>
						<div>
							<p class="opc">Añejamiento:</p>
							<p class="info_opc">12 meses de barrica de roble francés y americano, mínimo 12 meses de botella.</p>
						</div>
						<div>
							<p class="opc">Maridaje:</p>
							<p class="info_opc">Chiles en nogada, caramelos, confit.</p>
						</div>
					</div>
					<div class="car_cont_der">
						<div>
							<p class="opc">Vista:</p>
							<p class="info_opc">Elegante color granate con tonos púrpura. </p>
						</div>
						<div>
							<p class="opc">Olfato:</p>
							<p class="info_opc">Nariz compleja y buena intensidad, con notas a frutos negros, pimienta negra y tostados.</p>
						</div>
						<div>
							<p class="opc">Gusto:</p>
							<p class="info_opc">Ataque agradable y refinado, con buena persistencia. Se confirman notas complejas de frutos negros.</p>
						</div>

						<div>
							<br><br>
							<a href="assets/pdf/Terruños Dos Terruños.pdf" target="_blank" class="btn_ficha_tecnica" rel="noopener noreferrer">
								<!--<img src="assets/img/svg/flecha_abajo.svg" class="flecha_abajo">-->
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.79 23.64" class="flecha_abajo">
									<defs>
										<style>
											.fa-1 {
												fill: none;
												stroke: #fff;
												stroke-miterlimit: 10;
												stroke-width: 2.5px;
											}
										</style>
									</defs>
									<title>flecha_abajo</title>
									<g id="Capa_2" data-name="Capa 2">
										<g id="Capa_1-2" data-name="Capa 1">
											<polyline class="fa-1" points="0.81 14.29 9.89 22 18.98 14.29" />
											<line class="fa-1" x1="9.89" y1="22.66" x2="9.89" />
										</g>
									</g>
								</svg>
								FICHA TÉCNICA 2017
							</a>
						</div>

						<div>
							<br><br>
							<a href="assets/pdf/ficha_tecnica_dos_terrunos_2018.pdf" target="_blank" class="btn_ficha_tecnica" rel="noopener noreferrer">
								<!--<img src="assets/img/svg/flecha_abajo.svg" class="flecha_abajo">-->
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.79 23.64" class="flecha_abajo">
									<defs>
										<style>
											.fa-1 {
												fill: none;
												stroke: #fff;
												stroke-miterlimit: 10;
												stroke-width: 2.5px;
											}
										</style>
									</defs>
									<title>flecha_abajo</title>
									<g id="Capa_2" data-name="Capa 2">
										<g id="Capa_1-2" data-name="Capa 1">
											<polyline class="fa-1" points="0.81 14.29 9.89 22 18.98 14.29" />
											<line class="fa-1" x1="9.89" y1="22.66" x2="9.89" />
										</g>
									</g>
								</svg>
								FICHA TÉCNICA 2018
							</a>
						</div>

					</div>
				</div>

			</div>

			<!-- end vino 3 -->


			<!-- Vino 4 -->
			<div class="datos vino4 oculto">

				<div class="nombre_vino">
					<p class="txt_nombre_vino"><span class="bold">Terruño Blanco</span></p>
				</div>
				<div class="caracteristicas">
					<div class="car_cont_izq">
						<div>
							<p class="opc">Familia:</p>
							<p class="info_opc">Terruños.</p>
						</div>
						<div>
							<p class="opc">Origen:</p>
							<p class="info_opc">Valle de Colón, QRO.</p>
						</div>
						<div>
							<p class="opc">Varietal:</p>
							<p class="info_opc">Riesling, Chardonnay, Chenin Blanc, Sauvignon Blanc y Gewürztraminer.</p>
						</div>
						<div>
							<p class="opc">Elaboración:</p>
							<p class="info_opc">Selección de uva manual por racimo, con un prensado suave, fermentación controlada en tanque de acero inoxidable, estabilización y filtración parcial.</p>
						</div>
						<!-- <div>
							<p class="opc">Añada:</p>
							<p class="info_opc">2020</p>
						</div> -->
						<div>
							<p class="opc">Maridaje:</p>
							<p class="info_opc">Crema de almejas, tacos de camarón y toast de frutas con miel.</p>
						</div>
					</div>
					<div class="car_cont_der">
						<div>
							<p class="opc">Vista:</p>
							<p class="info_opc">Vino cristalino, amarillo paja con destellos plateados..</p>
						</div>
						<div>
							<p class="opc">Olfato:</p>
							<p class="info_opc">Buena intensidad aromática donde resaltan frutos tropicales como piña, durazno, mango y notas de flores blancas.</p>
						</div>
						<div>
							<p class="opc">Gusto:</p>
							<p class="info_opc">Ataque fresco al paladar, reafirmando la frutalidad, equilibrado y buen de acompañante de las tardes calurosas.</p>
						</div>

						<div>
							<br><br>
							<a href="assets/pdf/Terruños Blanco.pdf" target="_blank" class="btn_ficha_tecnica" rel="noopener noreferrer">
								<!--<img src="assets/img/svg/flecha_abajo.svg" class="flecha_abajo">-->
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.79 23.64" class="flecha_abajo">
									<defs>
										<style>
											.fa-1 {
												fill: none;
												stroke: #fff;
												stroke-miterlimit: 10;
												stroke-width: 2.5px;
											}
										</style>
									</defs>
									<title>flecha_abajo</title>
									<g id="Capa_2" data-name="Capa 2">
										<g id="Capa_1-2" data-name="Capa 1">
											<polyline class="fa-1" points="0.81 14.29 9.89 22 18.98 14.29" />
											<line class="fa-1" x1="9.89" y1="22.66" x2="9.89" />
										</g>
									</g>
								</svg>
								FICHA TÉCNICA
							</a>
						</div>

					</div>
				</div>

			</div>

			<!-- end vino 4 -->

			<!-- Vino 5 -->
			<div class="datos vino5 oculto">

				<div class="nombre_vino">
					<p class="txt_nombre_vino"><span class="bold">Terruño Bajío</span></p>
				</div>
				<div class="caracteristicas">
					<div class="car_cont_izq">
						<div>
							<p class="opc">Familia:</p>
							<p class="info_opc">Terruños.</p>
						</div>
						<div>
							<p class="opc">Origen:</p>
							<p class="info_opc">Valle de Colón, QRO.</p>
						</div>
						<div>
							<p class="opc">Varietal:</p>
							<p class="info_opc">Merlot, Malbec y Tempranillo.</p>
						</div>
						<div>
							<p class="opc">Elaboración:</p>
							<p class="info_opc">Selección de uva manual, con un prensado suave, fermentación controlada en tanque de acero inoxidable, con una crianza de 12 meses en barrica de roble francés y americano.</p>
						</div>
						<!-- <div>
							<p class="opc">Añada:</p>
							<p class="info_opc">2019</p>
						</div> -->
						<div>
							<p class="opc">Maridaje:</p>
							<p class="info_opc">Pasta con salsas rojas, enchiladas queretanas, pierna mechada, gorditas queretanas.</p>
						</div>
					</div>
					<div class="car_cont_der">
						<div>
							<p class="opc">Vista:</p>
							<p class="info_opc">Capa media alta y alta densidad aparente, color granate con un ribete púrpura.</p>
						</div>
						<div>
							<p class="opc">Olfato:</p>
							<p class="info_opc">Intensidad aromática media donde resaltan notas frutales y de barrica, tales como frutos rojos deshidratados, compota de frutos, notas tostadas y caramelo.</p>
						</div>
						<div>
							<p class="opc">Gusto:</p>
							<p class="info_opc">Ataque medio con tanino consistente y aterciopelado, una acidez y retrogusto medio.</p>
						</div>

						<div>
							<br><br>
							<a href="assets/pdf/Terruños Bajío.pdf" target="_blank" class="btn_ficha_tecnica" rel="noopener noreferrer">
								<!--<img src="assets/img/svg/flecha_abajo.svg" class="flecha_abajo">-->
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.79 23.64" class="flecha_abajo">
									<defs>
										<style>
											.fa-1 {
												fill: none;
												stroke: #fff;
												stroke-miterlimit: 10;
												stroke-width: 2.5px;
											}
										</style>
									</defs>
									<title>flecha_abajo</title>
									<g id="Capa_2" data-name="Capa 2">
										<g id="Capa_1-2" data-name="Capa 1">
											<polyline class="fa-1" points="0.81 14.29 9.89 22 18.98 14.29" />
											<line class="fa-1" x1="9.89" y1="22.66" x2="9.89" />
										</g>
									</g>
								</svg>
								FICHA TÉCNICA
							</a>
						</div>

					</div>
				</div>

			</div>

			<!-- end vino 5 -->

			<!-- imgen -->

			<div class="imagen">
				<div class="uvas layer2 uvas_verdes uvas_terrunos"></div>
				<img class="vino_img layer" src="assets/img/vinos/terruños/terruno-blanco.png" alt="Vinaltura - arroyo 2016 terruno">
				<div class="btn_tienda"> <a style="text-decoration: none; color:white;" href="https://tiendavinaltura.mx/" target="_blank">COMPRAR</a></div>
				<div class="cont_nombre_tipo_te terrunos_ubic">
					<p>Terruños<br>

				</div>
			</div>

			<!-- end imagen -->

		</div>

		<div class="cont_vino_caracteristicas zona_ensayos oculto">

			<!-- vino 1 -->

			<div class="datos vino1 oculto">

				<div class="nombre_vino">
					<p class="txt_nombre_vino"><span class="bold">Un par de ... <br> (Blanco)</span></p>
				</div>
				<div class="caracteristicas">
					<div class="car_cont_izq">
						<div>
							<p class="opc">Familia:</p>
							<p class="info_opc">Ediciones especiales</p>
						</div>
						<div>
							<p class="opc">Variedad:</p>
							<p class="info_opc">(I) Albariño <br> (D) Verdejo</p>
						</div>
						<!-- <div>
							<p class="opc">Varietal:</p>
							<p class="info_opc">Verdejo. Brix de cosecha: 22.5 <br> Viognier. Brix de cosecha: 21.5</p>
						</div>
						-->
						<div>
							<p class="opc">Winemaker:</p>
							<p class="info_opc">(I) Hans Duer <br> (D) Lluis Raventós.</p>
						</div>
						<div>
							<p class="opc">Elaboración:</p>
							<p class="info_opc">Fermentación en tanques de acero inoxidable, en forma de huevo, sin añejamiento en barrica, 12 meses reposo en botella.</p>
						</div> 
						<div>
							<p class="opc">Vista:</p>
							<p class="info_opc">(I) Amarillo paja con destellos dorados, limpio, brillante con una densidad alta. (D) Amarillo paja con destellos verdosos y plateados, limpio, brillante con una densidad media alta. </p>
						</div>
					</div>
					<div class="car_cont_der">

						<div>
							<p class="opc">Olfato:</p>
							<p class="info_opc">(I) Intensidad aromática media donde resaltan notas como flores y frutos tales como azucenas, melón, piña, pera y maracuyá. (D) Intensidad aromática media donde resaltan notas frutales tropicales como mango, piña y manzana verde.</p>
						</div>
						<div>
							<p class="opc">Gusto:</p>
							<p class="info_opc">(I) Vino suave con una acidez equilibrada, fresco, seco y un retrogusto donde recordamos las frutas como piña y melón. (D) Vino suave y fresco, con un equilibrio entre la acidez y el alcohol.</p>
						</div>
						<div>
							<p class="opc">Maridaje:</p>
							<p class="info_opc">Ceviche de pescado, tacos estilo ensenada, quesadillas de flor de calabaza y pay de limón. </p>
						</div>
						<br><br>
						<div>

							<a href="assets/pdf/Especiales_Un_par_de.pdf" target="_blank" class="btn_ficha_tecnica" rel="noopener noreferrer">
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.79 23.64" class="flecha_abajo">
									<defs>
										<style>
											.fa-1 {
												fill: none;
												stroke: #fff;
												stroke-miterlimit: 10;
												stroke-width: 2.5px;
											}
										</style>
									</defs>
									<title>flecha_abajo</title>
									<g id="Capa_2" data-name="Capa 2">
										<g id="Capa_1-2" data-name="Capa 1">
											<polyline class="fa-1" points="0.81 14.29 9.89 22 18.98 14.29" />
											<line class="fa-1" x1="9.89" y1="22.66" x2="9.89" />
										</g>
									</g>
								</svg>
								FICHA TÉCNICA
							</a>
						</div>

					</div>
				</div>

			</div>

			<!-- end vino 1 -->

			<div class="datos vino22 oculto">

				<div class="nombre_vino">
					<p class="txt_nombre_vino"><span class="bold">Un par de ... <br> (Rosé)</span></p>
				</div>
				<div class="caracteristicas">
					<div class="car_cont_izq">

						<div>
							<p class="opc">Familia:</p>
							<p class="info_opc">Ediciones especiales.</p>
						</div>
						<div>
							<p class="opc">Varietal:</p>
							<p class="info_opc">(I) 100% Grenache <br>  (D) 100% Zinfandel </p>
						</div>
						<div>
							<p class="opc">Fermentación:</p>
							<p class="info_opc">Tanques de acero inoxidable en forma ovoidal (huevo)</p>
						</div>
						<div>
							<p class="opc">Añejamiento:</p>
							<p class="info_opc">Sin barrica, 6 meses en botella.</p>
						</div>
						<div>
							<p class="opc">Vista:</p>
							<p class="info_opc">(I) Color rosa medio con destellos cereza, con una densidad media, limpio y brillante.  (D) Elegante color a palo de rosa, aspecto  limpio y brillante con una densidad media.</p>
						</div>
					</div>
					<div class="car_cont_der">

						<div>
							<p class="opc">Olfato:</p>
							<p class="info_opc">(I) Intensidad aromática media, donde resaltan frutos rojos y amarillos como frambuesa, cereza y manzana, con notas herbales similares a menta y eucalipto.  (D) Intensidad aromática media baja, donde resaltan notas frutales y florales tales como rosas, yogurt de fresa y toronja.</p>
						</div>
						<div>
							<p class="opc">Gusto:</p>
							<p class="info_opc">(I) Vino fresco con una acidez media y buen ataque en boca.  (D) Frescura y elegancia que reafirma notas percibidas en nariz.</p>
						</div>
						<div>
							<p class="opc">Persistencia:</p>
							<p class="info_opc">Media</p>
						</div>
						<div>
							<p class="opc">Maridaje:</p>
							<p class="info_opc">(I) Pizzas, quesos suaves, pay de queso con frutos rojos. (D) Quesadillas con diferentes guisos, pay de limón.</p>
						</div>
						<br><br>
						<div>

							<a href="assets/pdf/Un-par-de_Rose_2023_1.pdf" target="_blank" class="btn_ficha_tecnica" rel="noopener noreferrer">
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.79 23.64" class="flecha_abajo">
									<defs>
										<style>
											.fa-1 {
												fill: none;
												stroke: #fff;
												stroke-miterlimit: 10;
												stroke-width: 2.5px;
											}
										</style>
									</defs>
									<title>flecha_abajo</title>
									<g id="Capa_2" data-name="Capa 2">
										<g id="Capa_1-2" data-name="Capa 1">
											<polyline class="fa-1" points="0.81 14.29 9.89 22 18.98 14.29" />
											<line class="fa-1" x1="9.89" y1="22.66" x2="9.89" />
										</g>
									</g>
								</svg>
								FICHA TÉCNICA
							</a>
						</div>

					</div>
				</div>

			</div>

			<!-- end vino 1 -->

			<!-- vino 2 -->

			<div class="datos vino2 oculto">

				<div class="nombre_vino">
					<p class="txt_nombre_vino"><span class="bold">GW Ancestral</span></p>
				</div>
				<div class="caracteristicas">
					<div class="car_cont_izq">

						<div>
							<p class="opc">Familia:</p>
							<p class="info_opc">Ediciones especiales.</p>
						</div>
						<div>
							<p class="opc">Varietal:</p>
							<p class="info_opc">Gewürztraminer</p>
						</div>
						<div>
							<p class="opc">Bodega:</p>
							<p class="info_opc">Vinaltura.</p>
						</div>
						<div>
							<p class="opc">Estilo:</p>
							<p class="info_opc">Espumoso método ancestral.</p>
						</div>
						<div>
							<p class="opc">Maridaje:</p>
							<p class="info_opc">Mole de xoconostle, costillas de cerdo, ensalada de frutos, pastel tres leches.</p>
						</div>

					</div>
					<div class="car_cont_der">
						<div>
							<p class="opc">Vista:</p>
							<p class="info_opc">Amarillo paja con destellos verdosos, con una turbidez media debido a su proceso de elaboración, burbuja pequeña que va por el centro de la copa reafirmando su naturaleza con una corona de media persistencia. </p>
						</div>
						<div>
							<p class="opc">Olfato:</p>
							<p class="info_opc">Intensidad aromática elevada donde resaltan la frutalidad y la floralidad del varietal, notas como litchi, guanábana, rosas, flores blancas. </p>
						</div>
						<div>
							<p class="opc">Gusto:</p>
							<p class="info_opc">Vino freso con una burbuja amplia al paladar, seco con una buena acidez y una burbuja noble. </p>
						</div>
						<br><br>

						<div>

							<a href="assets/pdf/Especiales GW Ancestral.pdf" target="_blank" class="btn_ficha_tecnica" rel="noopener noreferrer">
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.79 23.64" class="flecha_abajo">
									<defs>
										<style>
											.fa-1 {
												fill: none;
												stroke: #fff;
												stroke-miterlimit: 10;
												stroke-width: 2.5px;
											}
										</style>
									</defs>
									<title>flecha_abajo</title>
									<g id="Capa_2" data-name="Capa 2">
										<g id="Capa_1-2" data-name="Capa 1">
											<polyline class="fa-1" points="0.81 14.29 9.89 22 18.98 14.29" />
											<line class="fa-1" x1="9.89" y1="22.66" x2="9.89" />
										</g>
									</g>
								</svg>
								FICHA TÉCNICA 2020
							</a>
						</div>

						<div>
							<br><br>
							<a href="assets/pdf/Ficha_Tecnica_GW_Ancestral_2021.pdf" target="_blank" class="btn_ficha_tecnica" rel="noopener noreferrer">
								<!--<img src="assets/img/svg/flecha_abajo.svg" class="flecha_abajo">-->
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.79 23.64" class="flecha_abajo">
									<defs>
										<style>
											.fa-1 {
												fill: none;
												stroke: #fff;
												stroke-miterlimit: 10;
												stroke-width: 2.5px;
											}
										</style>
									</defs>
									<title>flecha_abajo</title>
									<g id="Capa_2" data-name="Capa 2">
										<g id="Capa_1-2" data-name="Capa 1">
											<polyline class="fa-1" points="0.81 14.29 9.89 22 18.98 14.29" />
											<line class="fa-1" x1="9.89" y1="22.66" x2="9.89" />
										</g>
									</g>
								</svg>
								FICHA TÉCNICA 2021
							</a>
						</div>

					</div>
				</div>

			</div>

			<!-- end vino 2 -->

			<!-- vino 3 -->

			<div class="datos vino3 oculto">

				<div class="nombre_vino">
					<p class="txt_nombre_vino"><span class="bold">GW Tradicional</span></p>
				</div>
				<div class="caracteristicas">
					<div class="car_cont_izq">

						<div>
							<p class="opc">Familia:</p>
							<p class="info_opc">Ediciones especiales.</p>
						</div>
						<div>
							<p class="opc">Varietal:</p>
							<p class="info_opc">100% Gewürztraminer</p>
						</div>
						<div>
							<p class="opc">Region:</p>
							<p class="info_opc">Valle de Colón, Querétaro</p>
						</div>
						<div>
							<p class="opc">Bodega:</p>
							<p class="info_opc">Vinaltura.</p>
						</div>
						<div>
							<p class="opc">Maridaje:</p>
							<p class="info_opc">Huazontles, pozole verde, mole verde.</p>
						</div>
						<!-- <div>
							<p class="opc">Elaboración:</p>
							<p class="info_opc">Selección de uva manual, prensado suave, fermentación en tanque de acero inoxidable, estabilización y filtración parcial.</p>
						</div> -->

					</div>
					<div class="car_cont_der">
						<div>
							<p class="opc">Vista:</p>
							<p class="info_opc">Amarillo verdoso, con destellos plateados, burbuja fina y elegante que forma un rosario constante y una corona muy fina. </p>
						</div>
						<div>
							<p class="opc">Olfato:</p>
							<p class="info_opc">Intensidad aroma alta media, donde destacan notas cítricas y Dulces principalmente como el limón, lima, y miel.</p>
						</div>
						<div>
							<p class="opc">Gusto:</p>
							<p class="info_opc">Vino muy fresco, acidez media, con un final ligeramente dulce y una burbuja muy sutil.</p>
						</div>
						<br><br>
						<div>

							<a href="assets/pdf/gw_tradicional.pdf" target="_blank" class="btn_ficha_tecnica" rel="noopener noreferrer">
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.79 23.64" class="flecha_abajo">
									<defs>
										<style>
											.fa-1 {
												fill: none;
												stroke: #fff;
												stroke-miterlimit: 10;
												stroke-width: 2.5px;
											}
										</style>
									</defs>
									<title>flecha_abajo</title>
									<g id="Capa_2" data-name="Capa 2">
										<g id="Capa_1-2" data-name="Capa 1">
											<polyline class="fa-1" points="0.81 14.29 9.89 22 18.98 14.29" />
											<line class="fa-1" x1="9.89" y1="22.66" x2="9.89" />
										</g>
									</g>
								</svg>
								FICHA TÉCNICA
							</a>
						</div>

					</div>
				</div>

			</div>

			<!-- end vino 3 -->

			<!-- vino 4 -->

			<div class="datos vino4 oculto">

				<div class="nombre_vino">
					<p class="txt_nombre_vino"><span class="bold">Merlot</span></p>
				</div>
				<div class="caracteristicas">
					<div class="car_cont_izq">

						<div>
							<p class="opc">Familia:</p>
							<p class="info_opc">Ediciones especiales.</p>
						</div>
						<div>
							<p class="opc">Varietal:</p>
							<p class="info_opc">Merlot.</p>
						</div>
						<div>
							<p class="opc">Region:</p>
							<p class="info_opc">Valle de Colón, Querétaro</p>
						</div>
						<div>
							<p class="opc">Bodega:</p>
							<p class="info_opc">Vinaltura.</p>
						</div>
						<div>
							<p class="opc">Maridaje:</p>
							<p class="info_opc">Costillas asadas, pulpo a las brasas, hamburguesa de res, tacos de legua. </p>
						</div>
						<div>
							<p class="opc">Elaboración:</p>
							<p class="info_opc">Selección de uva manual, con un prensado suave, fermentación controlada en tanque de acero inoxidable, con una crianza de 12 meses en barrica de roble francés.</p>
						</div>

					</div>
					<div class="car_cont_der">
						<div>
							<p class="opc">Vista:</p>
							<p class="info_opc">Capa media alta, color rojo carmín un ribete color granate. </p>
						</div>
						<div>
							<p class="opc">Olfato:</p>
							<p class="info_opc">Intensidad aromática media, donde destacan notas frutales, frutales, y herbales, tales como cereza, zarzamora, ciruela pasa, jamaica, hierbabuena y rosas.</p>
						</div>
						<div>
							<p class="opc">Gusto:</p>
							<p class="info_opc">Vino seco con un ataque medio, acidez media con un tanino sutil y elegante.</p>
						</div>
						<br><br>
						<div>

							<a href="assets/pdf/un_par_de_huevos.pdf" target="_blank" class="btn_ficha_tecnica" rel="noopener noreferrer">
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.79 23.64" class="flecha_abajo">
									<defs>
										<style>
											.fa-1 {
												fill: none;
												stroke: #fff;
												stroke-miterlimit: 10;
												stroke-width: 2.5px;
											}
										</style>
									</defs>
									<title>flecha_abajo</title>
									<g id="Capa_2" data-name="Capa 2">
										<g id="Capa_1-2" data-name="Capa 1">
											<polyline class="fa-1" points="0.81 14.29 9.89 22 18.98 14.29" />
											<line class="fa-1" x1="9.89" y1="22.66" x2="9.89" />
										</g>
									</g>
								</svg>
								FICHA TÉCNICA
							</a>
						</div>

					</div>
				</div>

			</div>

			<!-- end vino 4 -->

			<!-- vino 5 -->
			<div class="datos vino5 oculto">

				<div class="nombre_vino">
					<p class="txt_nombre_vino"><span class="bold">GW Dulce</span></p>
				</div>
				<div class="caracteristicas">
					<div class="car_cont_izq">

						<div>
							<p class="opc">Familia:</p>
							<p class="info_opc">Ediciones especiales.</p>
						</div>
						<div>
							<p class="opc">Varietal:</p>
							<p class="info_opc">Gewürztraminer.</p>
						</div>
						<div>
							<p class="opc">Region:</p>
							<p class="info_opc">Valle de Colón, Querétaro</p>
						</div>
						<div>
							<p class="opc">Bodega:</p>
							<p class="info_opc">Vinaltura.</p>
						</div>
						<div>
							<p class="opc">Maridaje:</p>
							<p class="info_opc">Postres frescos como helados, ate con queso, strudel de manzana y quesos fuertes. </p>
						</div>
						<div>
							<p class="opc">Elaboración:</p>
							<p class="info_opc">Selección de uva manual, cosechado a 25°Brix y reposado en camas de pasificación. Prensado suave, fermentación en tanque de acero inoxidable, estabilización y filtración parcial.</p>
						</div>

					</div>
					<div class="car_cont_der">
						<div>
							<p class="opc">Vista:</p>
							<p class="info_opc">Vino amarillo paja con destellos dorados, limpio, brillante, densidad aparente elevada. </p>
						</div>
						<div>
							<p class="opc">Olfato:</p>
							<p class="info_opc">Intensidad aromática elevada, notas florales, herbales y frutales, predominando las rosas, jazmín, gardenia y un con ligeras notas a litchi, miel y guanábana.</p>
						</div>
						<div>
							<p class="opc">Gusto:</p>
							<p class="info_opc">Amable al paladar con buena untuosidad y un retrogusto que reafirma las notas florales.</p>
						</div>
						<br><br>
						<div>

							<a href="assets/pdf/Especiales-GW-dulce.pdf" target="_blank" class="btn_ficha_tecnica" rel="noopener noreferrer">
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.79 23.64" class="flecha_abajo">
									<defs>
										<style>
											.fa-1 {
												fill: none;
												stroke: #fff;
												stroke-miterlimit: 10;
												stroke-width: 2.5px;
											}
										</style>
									</defs>
									<title>flecha_abajo</title>
									<g id="Capa_2" data-name="Capa 2">
										<g id="Capa_1-2" data-name="Capa 1">
											<polyline class="fa-1" points="0.81 14.29 9.89 22 18.98 14.29" />
											<line class="fa-1" x1="9.89" y1="22.66" x2="9.89" />
										</g>
									</g>
								</svg>
								FICHA TÉCNICA
							</a>
						</div>

					</div>
				</div>

			</div>
			<!-- endvino 5 -->

			<!-- imgen -->

			<div class="imagen">
				<div class="uvas uvas_especiales uvas_verdes layer2"></div>
				<img class="vino_img layer" src="assets/img/vinos/especiales/un-par-de.png" alt="Vinaltura - Especiales">
				<div class="btn_tienda"> <a style="text-decoration: none; color:white;" href="https://tiendavinaltura.mx/" target="_blank">COMPRAR</a></div>
				<div class="cont_nombre_tipo_es terrunos_ubic">
					<p class="especiales">Especiales</p>

				</div>
			</div>

			<!-- end imagen -->

		</div>

	</section>

	<div class="espacio_blanco centrado">
		<img src="assets/img/svg/flecha_abajo_larga.svg" style="height: 90%;" alt="Vinaltura">
	</div>

	<!-- END VINOS DESKTOP -->

	<!-- VINOS MOPVIL -->

	<section class="vinos_movil">

		<div class="cont_opciones_vinos_movil">

			<div class="cont_zonas_vinos_movil">
				<p class="active_movil_zona opc_vino_movil" op="1">BAJÍO</p>
				<p class="opc_vino_movil" op="2">VARIETALES</p>
				<p class="opc_vino_movil" op="3">TERRUÑOS</p>
				<p class="opc_vino_movil" op="4">ESPECIALES</p>
			</div>

			<div class="tipos_vino_movil tip_bajio oculto">
				<p class="tipo_movil tipo_movil_active" op="blanco">Blanco Bajío</p>
				<p class="tipo_movil" op="rose_bajio">Rosé Bajío</p>
				<p class="tipo_movil" op="tinto_bajio">Tinto Bajío</p>
				<p class="tipo_movil" op="espuma_rose">Espuma Rosé</p>
				<p class="tipo_movil" op="blanc_blancs">Blanc de Blancs</p>

			</div>

			<div class="tipos_vino_movil tip_varietales oculto">
				<p class="tipo_movil tipo_movil_active" op="chenin">CB</p>
				<p class="tipo_movil" op="gw">GW</p>
				<p class="tipo_movil" op="riesling">RS</p>
				<p class="tipo_movil" op="suavignon">SB</p>
				<p class="tipo_movil" op="malbec">ML</p>
				<p class="tipo_movil" op="merlot">MR</p>
				<p class="tipo_movil" op="ch">CH</p>

			</div>

			<div class="tipos_vino_movil tip_terrunos oculto">
				<span class="tipo_movil tipo_movil_active" op="terruno_blanco">Blanco</span>
				<span class="tipo_movil" op="terruno_bajio">Bajio</span>
				<p class="tipo_movil" op="arroyo">Arroyo</p>
				<p class="tipo_movil" op="ladera">Ladera</p>
				<p class="tipo_movil" op="dos_terrunos">Dos terruños</p>
			</div>

			<div class="tipos_vino_movil tip_ensayos oculto">
				<span class="tipo_movil active" op="gw_ancestral">GW Ancestral</span>
				<span class="tipo_movil" op="gw_2019">GW Dulce</span>
				<p class="tipo_movil" op="par_de_huevos">Un par de ... (Blanco)</p>
				<p class="tipo_movil" op="par_de_huevos_rose">Un par de ... (Rosé)</p>
				<p class="tipo_movil" op="gw_tradicional">GW Tradicional</p>
			</div>

		</div>

		<div class="contenido_vinos_movil bajio_movil">

			<section class="interno_tipo_vino_movil tinto_bajio_movil oculto">

				<?php include "includes/tinto_bajio.php"; ?>

			</section>

			<section class="interno_tipo_vino_movil espuma_rose_movil oculto">

				<?php include "includes/espuma_rose.php"; ?>

			</section>

			<section class="interno_tipo_vino_movil blanco_movil">

				<?php include "includes/blanco.php"; ?>

			</section>

			<section class="interno_tipo_vino_movil rose_bajio_movil oculto">

				<?php include "includes/rose_bajio.php"; ?>

			</section>

			<section class="interno_tipo_vino_movil blanc_blancs_movil oculto">

				<?php include "includes/blanc_blancs.php"; ?>

			</section>

		</div>

		<div class="contenido_vinos_movil varietales_movil oculto">

			<section class="interno_tipo_vino_movil chenin_movil oculto">

				<?php include "includes/chenin.php"; ?>

			</section>

			<section class="interno_tipo_vino_movil suavignon_movil">

				<?php include "includes/suavignon.php"; ?>

			</section>

			<section class="interno_tipo_vino_movil riesling_movil oculto">

				<?php include "includes/riesling.php"; ?>

			</section>

			<section class="interno_tipo_vino_movil gw_movil oculto">

				<?php include "includes/gw2018.php"; ?>

			</section>

			<section class="interno_tipo_vino_movil malbec_movil oculto">

				<?php include "includes/malbec.php"; ?>

			</section>

			<section class="interno_tipo_vino_movil merlot_movil">

				<?php include "includes/merlot.php"; ?>

			</section>

			<section class="interno_tipo_vino_movil ch_movil ocultar">

				<?php include "includes/ch.php"; ?>

			</section>

		</div>


		<div class="contenido_vinos_movil terrunos_movil oculto">

			<section class="interno_tipo_vino_movil terruno_blanco_movil">

				<?php include "includes/terruno_blanco.php"; ?>

			</section>

			<section class="interno_tipo_vino_movil terruno_bajio_movil">

				<?php include "includes/terruno_bajio.php"; ?>

			</section>

			<section class="interno_tipo_vino_movil arroyo_movil">

				<?php include "includes/arroyo.php"; ?>

			</section>

			<section class="interno_tipo_vino_movil ladera_movil oculto">

				<?php include "includes/ladera.php"; ?>

			</section>

			<section class="interno_tipo_vino_movil dos_terrunos_movil oculto">

				<?php include "includes/dos_terrunos.php"; ?>

			</section>

		</div>

		<div class="contenido_vinos_movil ensayos_movil oculto">


			<section class="interno_tipo_vino_movil par_huevos_movil">

				<?php include "includes/un_par_huevos.php"; ?>

			</section>

			<section class="interno_tipo_vino_movil par_huevos_rose_movil">

				<?php include "includes/un_par_huevos_rose.php"; ?>

			</section>

			<section class="interno_tipo_vino_movil gw_ancestral_movil">

				<?php include "includes/gw_ancestral.php"; ?>

			</section>

			<section class="interno_tipo_vino_movil gw_2019_movil">

				<?php include "includes/gw_2019.php"; ?>

			</section>

			<section class="interno_tipo_vino_movil gw_tradicional_movil">

				<?php include "includes/gw_tradicional.php"; ?>

			</section>


		</div>

		<img src="assets/img/svg/flecha_abajo_larga.svg" style="height: 150px; padding-top: 50px;" alt="Vinaltura">

	</section>

	<!-- END VINOS MOVIL -->

	<div class="espacio_blanco"></div>

	<!-- <div class="espacio_blanco_movil"></div>
	<div class="espacio_blanco_movil"></div> -->

	<!-- WINE CLUB DESKTOP -->

	<section class="wine_club">

		<div class="cont_logo_wineclub">

			<img src="assets/img/svg/logo_wineclub.svg" alt="Wine club Vinaltura">

		</div>

		<div class="form_wine_club">

			| <p class="titulo_formulario_wine_club">ÚNETE A NUESTRO <br> WINE CLUB</p>
			<form id="formulario_wine_club" class="form" method="POST">
				<div class="grupo_input grupo__nombre formulario__grupo-incorrecto" id="grupo__nombre">
					<label class="input_titulo_club_wine" for="nombre_wine_club">Nombre</label>
					<input id="nombre_wine_club" type="text" class="input_100_wine_club nombre_form" name="nombre_wine_club" required>
					<i class="formulario__validacion-estado-wineclub fas fa-times-circle" aria-hidden="true"></i>
				</div>
				<div class="grupo_input grupo__correo" id="grupo__correo">
					<label class="input_titulo_club_wine" for="correo_wine_club">Mail</label>
					<input id="correo_wine_club" type="mail" class="input_100_wine_club correo_form" name="correo_wine_club" required>
					<i class="formulario__validacion-estado-wineclub fas fa-times-circle" aria-hidden="true"></i>
				</div>

				<button type="submit" class="boton_unir-wineclub">
					QUIERO UNIRME

					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.94 15.19">
						<defs>
							<style>
								.fa-2 {
									fill: none;
									stroke: #fff;
									stroke-miterlimit: 10;
								}
							</style>
						</defs>
						<title>flecha-derecha</title>
						<g id="Capa_2" data-name="Capa 2">
							<g id="Capa_1-2" data-name="Capa 1">
								<polyline class="fa-2" points="122.99 0.35 130.23 7.59 122.99 14.83" />
								<line class="fa-2" x1="130.1" y1="7.59" y2="7.59" />
							</g>
						</g>
					</svg>
				</button>

			</form>
		</div>

	</section>

	<!-- END WINE DESKTOP -->

	<div class="espacio_blanco"></div>

	<div class="espacio_blanco_movil"></div>
	<div class="espacio_blanco_movil"></div>

	<!-- TERRAZA DESKTOP -->

	<div id="vistas"></div>

	<section id="terraza" class="terraza">

		<div class="tasting">

			<div class="cont_interno_tasting_envero">

				<div class="fondo_negro_transparente"></div>

				<!--<h2 class="titulo_tasting_envero">tasting_logo</h2>-->
				<!-- <img src="assets/img/svg/experiencias-logo.svg" class="logo_envero" alt="Vinaltura"> -->
				<span class="bold_tasting">EXPERIENCIAS</span>

				<div class="linea_titulo_tasting_envero" style="top: 20px;"></div>

				<p class="txt_tasting_envero">
					Descubre y reserva la oferta de experiencias que tenemos en nuestro viñedo
				</p>

				<div class="terms" style="display: flex;align-items: center;justify-content: center;">
					<!-- <a href="assets/pdf/Experiencias-Vinaltura.pdf" target="_blank" class="boton_tasting_envero" style="font-size: 13px;">VER TOUR</a> -->
					<!-- <div class="verticalLine" style="width: 2px;background-color: white;height: 72px;float: left;z-index: 10;margin-left: 20px;margin-right: 20px;"></div> -->
					<a  href="javascript:void(0);" onclick="return popupInitQHKBybeZDr('https://es.vintrail.com/popup_widget/widget_feature?cid=1204&type=landing', this);" class="btn_reservaciones">RESERVACIONES</a>
				</div>

			</div>

		</div>

		<div class="envero" id="visitas">

			<div class="cont_interno_tasting_envero">

				<div class="fondo_negro_transparente"></div>

				<img src="assets/img/svg/envero_logo.svg" class="logo_envero" alt="Vinaltura">

				<div class="linea_titulo_tasting_envero"></div>

				<p class="txt_tasting_envero">
					Reserva en el restaurante de cocina de viñedo en nuestra terraza
				</p>
				<div class="terms" style="display: flex;align-items: center;justify-content: center;">
					<a href="assets/pdf/MENU_Invierno.pdf" target="_blank" class="boton_tasting_envero" style="font-size: 13px;">VER MENÚ</a>
					<div class="verticalLine" style="width: 2px;background-color: white;height: 72px;float: left;z-index: 10;margin-left: 20px;margin-right: 20px;"></div>
					<!-- <a type="button" class="btn_reservaciones open-modal" data-open="modal1" target="_blank" >RESERVACIONES</a> -->
					<a href="https://bit.ly/Vinaltura_Visitas2023" class="btn_reservaciones" target="_blank" >RESERVACIONES</a>

				</div>
			</div>

		</div>

	</section>
	<!-- <div class="modal" id="modal1" data-animation="slideInOutLeft">
		<div class="modal-dialog">
			<button class="close-modal" aria-label="closemodal" data-close onclick="cerrar_modal()">
				✕
			</button>
			<section class="modal-content">
				<iframe id="envero-queretaro" class="iframe_modal" title="Reservas" src="https://www.covermanager.com/reservation/module_restaurant/envero-queretaro/spanish" frameborder="0" onload="iFrameResize ();"> </iframe>
			</section>

		</div>
	</div> -->


	<script type="text / javascript" src="https://www.covermanager.com/js/iframeResizer.min.js"> </script>
	<script type="text / javascript" src="https: / /www.covermanager.com/reservation/gtmcrossdomain/envero-queretaro">
	</script>


	<!-- END TERRAZA DESKTOP -->

	<div class="espacio_blanco"></div>

	<div class="espacio_blanco_movil"></div>

	<section class="contacto">

		<div class="titulo_seccion">¡Brindemos!</div>
		<div class="espacio_blanco_movil"></div>

		<div class="cont_mapa_formulario">

			<div class="mapa">

				<!-- <iframe title="Mapa" class="imapa" src="mapa.html" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d38731.22203519185!2d-99.99024224027251!3d20.66015303987835!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1cd8f04da7495b6d!2sVINALTURA!5e0!3m2!1ses-419!2smx!4v1632321328335!5m2!1ses-419!2smx" style="border:0; width: 100%; height: 100%;" allowfullscreen="" loading="lazy"></iframe>

			</div>

			<div class="formulario">

				<form id="formulario" class="form">
					<div class="grupo_input grupo__nombre" id="grupo__nombre">
						<label class="input_titulo" for="nombre">Nombre</label>
						<input id="nombre" type="text" class="input_100 nombre_form" name="nombre">
						<i class="formulario__validacion-estado fas fa-times-circle"></i>
					</div>
					<div class="grupo_input grupo__correo" id="grupo__correo">
						<label class="input_titulo" for="correo">Mail</label>
						<input id="correo" type="mail" class="input_100 correo_form" name="correo">
						<i class="formulario__validacion-estado fas fa-times-circle"></i>
					</div>
					<div class="grupo_input grupo__telefono" id="grupo__telefono">
						<label class="input_titulo" for="telefono">Teléfono</label>
						<input id="telefono" type="tel" class="input_100 telefono_form" name="telefono">
						<i class="formulario__validacion-estado fas fa-times-circle"></i>
					</div>
					<div class="grupo_input grupo__mensaje" id="grupo__mensaje">
						<label class="input_titulo" for="mensaje">Mensaje</label>
						<input id="mensaje" type="text" class="input_100 mensaje_form" name="mensaje">
						<i class="formulario__validacion-estado fas fa-times-circle"></i>
					</div>
					<div class="grupo_input">
						<button type="submit" class="btn_enviar_form" href="#">ENVIAR</a>
					</div>
				</form>

				<div class="direccion">

					<div class="cont_redes">

						<a target="_blank" href="https://www.facebook.com/vinaltura/"><img style="width: 50px;margin-right: 10px;" class="img_red" src="assets/img/svg/icono_facebook.svg" alt="Vinaltura Facebook"></a>

						<a target="_blank" href="https://www.instagram.com/vinaltura/"><img style="width: 50px;margin-right: 10px;" class="img_red" src="assets/img/svg/icono_instagram.svg" alt="Vinaltura Instagram"></a>

						<a type="button" href="https://wa.me/+524424866337" target="_blank" >
                    	<img src="http://s2.accesoperu.com/logos/btn_whatsapp.png" alt="" style="width: 55px;margin-right: 10px;">
                    </a>

					</div>

					<p>Ignacio Zaragoza S/N, Santa Rosa de Lima Colón,</p>
					<p>Querétaro de Arteaga</p>
					<p>México.</p>

				</div>

			</div>

		</div>

	</section>

	<style>

		#form-newsletters-contact{

			position: relative;

			background-color: #1F1F1F;

			width: 100%;

			margin-top: 120px;

			color: #fff;

			display:flex;

			padding : 80px 0
		}

		.img_newsletters{
			
			width: 45%;
			
			display:flex;

			flex-direction:column;

			justify-content: center;

		}

		.contenedor_newsletter{
			
			width: 55%;
			
			display:flex;

			flex-direction:column;

			justify-content: center;

		}

		.contenedor_newsletter{
			
			height:40vh;

		}

		.contenedor_newsletter h2{
			font-size: 3rem;

			font-family:Prata-Regular;
			
			margin : 0px;

			padding: 0px;

			margin-bottom: 20px;
		}

		.contenedor_newsletter p{
			
			font-family:Gotham-Book;

			margin-bottom: 20px;

		}

		.contenedor_newsletter input{

			border-radius:5px;

			width:40%;

			padding:8px 10px;

		}

		.contenedor_newsletter button{

			background-color: #00A400;

			color: #fff;

			padding: 9px 22px;

			border-radius: 5px;

			margin-left: -15px;

		}

		.img_newsletters{

			position: relative;

		}

		.img_newsletters img {

			width: 90%;
			
			position: absolute;
			
			top: -50%;
			
		}

	</style>

	<section id="form-newsletters-contact">

		<div class="img_newsletters">

			<img src="assets/img/back_newsletter.png" >

		</div>

		<div class="contenedor_newsletter">

			<h2>

				Únete a nuestro <br> newsletter
				
			</h2>
			
			<p>
				
				Entérate cada semana de nuestras últimas noticias.
			
			</p>

			<form id="form-newsletters">

				<input placeholder="Correo" id="txt_unirse" name="txt_unirse" type="text">

				<button > UNIRSE </button>

			</form>
		
		</div>

	</section>


	<?php include "footer.php"; ?>

	<script>
		/** load pagina **/

		window.onload = function() {

			var scroll_pos = $(window).scrollTop();

			if (scroll_pos > 10) {
				//$(".preload").addClass("oculto");
				$("body").css("overflow", "initial");
			} else {

				//$(".preload").addClass("move");
				$("body").css("overflow", "initial");

			}

		}

		function cli_si() {
			$("#mayoria").addClass("oculto");
			$("#img-pre").removeClass("oculto");
			$("#preloader").removeClass("oculto");
			$("body").css("overflow", "initial");

			setTimeout(function() {
				$(".preload").addClass("move");
			}, 1500);
		}

		function cli_no() {
			location.reload();
		}

		/** end load pagina **/

		var mouseX, mosueY;
		var vw = $(window).width();
		var vh = $(window).height();
		var traX, traY;

		$(document).mousemove(function(e) {
			mouseX = e.pageX;
			mouseY = e.pageY;
			traX = ((14 * mouseX) / 600) + 50;
			traY = ((14 * mouseY) / 600) + 50;
			$(".cont_nombre_tipo p, .cont_nombre_tipo span").css({
				"background-position": traX + "%" + traY + "%"
			});
		});
	</script>

	<script>
		function cerrar_modal() {
			document.querySelector(".modal.is-visible").classList.remove(isVisible);
		}

		const openEls = document.querySelectorAll("[data-open]");
		// const closeEls = document.querySelectorAll("[data-close]");
		const closeEls = document.getElementsByClassName("close-modal");
		const isVisible = "is-visible";

		for (const el of openEls) {
			el.addEventListener("click", function() {
				const modalId = this.dataset.open;
				document.getElementById(modalId).classList.add(isVisible);
			});
		}

		for (const el of closeEls) {
			el.addEventListener("click", function() {
				this.parentElement.parentElement.parentElement.classList.remove(isVisible);
			});
		}

		document.addEventListener("click", e => {
			if (e.target == document.querySelector(".modal.is-visible")) {
				document.querySelector(".modal.is-visible").classList.remove(isVisible);
			}
		});

		document.addEventListener("keyup", e => {
			// if we press the ESC
			if (e.key == "Escape" && document.querySelector(".modal.is-visible")) {
				document.querySelector(".modal.is-visible").classList.remove(isVisible);
			}
		});
	</script>

	<!-- JQUERY -->

	<script type="text/javascript" src="assets/js/jquery.min.js"></script>

	<!-- END JQUERY -->

	<!-- SLICK JS -->

	<script type="text/javascript" src="assets/slick/slick.min.js"></script>

	<!-- END SLICK JS -->

	<!-- sweet alert 2 -->

	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

	<!-- end sweetalert 2 -->

	<!-- FONTAWESOME -->

	<script src="https://kit.fontawesome.com/2c36e9b7b1.js" crossorigin="anonymous"></script>

	<!-- END FONTAWESOME -->

	<script type="text/javascript" src="assets/js/script.js"></script>
	<!-- <script type="text/javascript" src="assets/js/script.min.js"></script> -->

	<!-- YOUTUBE API -->

	<script type="text/javascript" src="https://www.youtube.com/player_api"></script>

	<!-- END YOUTUBE API -->

	<script> 
  		!function(f,b,e,v,n,t,s)
		{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};
		if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
		n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];
		s.parentNode.insertBefore(t,s)}(window, document,'script',
		'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '1275282106620195');
		fbq('track', 'PageView');
	</script>
	<script>function widget_modal_close_QHKBybeZDr(_this){Array.prototype.forEach.call(document.getElementsByClassName('iframe-page-block'), function(el) {var srcVal = el.getAttribute('data-src');el.setAttribute('src', srcVal);}); document.getElementsByTagName('body')[0].style.overflow=''; _this.parentNode.parentNode.parentNode.nextElementSibling.remove(); _this.parentNode.parentNode.parentNode.remove();} function stay_close_modal_QHKBybeZDr(_this){_this.parentElement.parentElement.parentElement.parentElement.remove();} function leave_close_modal_QHKBybeZDr(_this){Array.prototype.forEach.call(document.getElementsByClassName('iframe-page-block'), function(el) {var srcVal = el.getAttribute('data-src');el.setAttribute('src', srcVal);});document.getElementsByTagName('body')[0].style.overflow=''; _this.parentElement.parentElement.parentElement.parentElement.remove(); document.body.getElementsByClassName('staypopup')[0].remove(); document.body.getElementsByClassName('pageContentModal')[0].remove();} function resizeIframeModalQHKBybeZDr(obj){document.getElementsByTagName('body')[0].style.overflow='hidden'; var eventMethod = window.addEventListener ? 'addEventListener' : 'attachEvent'; var eventer = window[eventMethod]; var messageEvent = eventMethod === 'attachEvent' ? 'onmessage' : 'message'; eventer(messageEvent, e => {var dimensions = e.data.dimensions; if (dimensions){obj.style.height = dimensions.height+'px';}});document.addEventListener('click', function (e) {if(e.target.parentNode.className == 'pageContentModal'){if(document.getElementsByClassName('pageContentModal').length > 0){var close_popup_html='<div style="display: block; overflow-x: hidden; overflow-y: scroll; position: fixed; top: 0; right: 0; left: 0; bottom: 0; z-index: 99999; opacity: 1; background-color: rgba(0,0,0,.8);">'+'<div style="margin: 10% auto; width: 350px; background: #fff; padding: 30px; border-radius: 16px;">'+'<div style="text-align: center;"><div style="font-size: 16px; line-height: 16px;">Are you sure you want to leave?</div><div style="padding: 10px;"><button style="margin: 0 10px; cursor: pointer; border: 1px solid transparent; padding: 6px 12px; font-size: 14px; line-height: 1.42857143; color: #fff; background-color: #5cb85c; border-radius: 4px;" onclick="return stay_close_modal_QHKBybeZDr(this);">Stay</button><button style="margin: 0 10px; cursor: pointer; border: 1px solid transparent; padding: 6px 12px; font-size: 14px; line-height: 1.42857143; color: #fff; background-color: #f0ad4e; border-radius: 4px;" onclick="return leave_close_modal_QHKBybeZDr(this);">Cancel Booking</button></div></div></div>'+'</div>'; var staypopupElement = document.body.getElementsByClassName('staypopup'); staypopupElement[0].innerHTML=close_popup_html; } } }, false);} function popupInitQHKBybeZDr(url, _this){var modal_html='<style type="text/css">.main-iframe-modal,.iframe-modal-block{width: 100%; border-radius: 10px} @media (min-width: 992px) {.main-iframe-modal,.iframe-modal-block{width: 1024px;}}</style><div style="display: block; overflow-x: hidden; overflow-y: scroll; position: fixed; top: 0; right: 0; left: 0; bottom: 0; z-index: 99999; opacity: 1; background-color: rgba(0,0,0,.8);">'+'<div class="main-iframe-modal" style="-webkit-transform: translate(0,0); -ms-transform: translate(0,0); transform: translate(0,0); margin: 30px auto;">'+'<button onclick="return widget_modal_close_QHKBybeZDr(this);" style="position: absolute; right: 0; width: 24px; background: transparent; border: 0; font-size: 22px; height: 30px; color: #000; padding: 0; cursor: pointer;">X</button>'+'<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/><iframe class="iframe-modal-block" src="'+url+'" height="600" frameborder="0" scrolling="no"  onload="resizeIframeModalQHKBybeZDr(this)"></iframe>'+'</div>'+'</div>'; var divElement = document.createElement('div'); divElement.className='pageContentModal'; document.body.appendChild(divElement); divElement.innerHTML=modal_html; var closeDivElement = document.createElement('div'); closeDivElement.className='staypopup'; document.body.appendChild(closeDivElement); Array.prototype.forEach.call(document.getElementsByClassName('iframe-page-block'), function(el) {el.removeAttribute('src');});}</script>

	<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1275282106620195&ev=PageView&noscript=1"/></noscript>

</body>

</html>