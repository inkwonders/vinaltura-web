<footer class="footer_desktop">

	<div class="cont_footer">

		<!-- <p class="txt_footer over_aviso">AVISO DE PRIVACIDAD</p> -->
		<a class="txt_footer" href="assets/pdf/aviso_vinaltura-3.pdf" target="_blank" rel="noopener noreferrer"><p>AVISO DE PRIVACIDAD</p></a>
		<p class="txt_footer">&#169; VINALTURA <?php echo date("Y"); ?></p>
		<a class="txt_footer" href="http://inkwonders.com/" target="_blank" rel="noopener noreferrer"><p class="over_ink">DESARROLLADO POR INKWONDERS</p></a>

	</div>

</footer>

<footer class="footer_movil">

	<div class="cont_footer_movil">

		<p class="txt_footer">VINALTURA <?php echo date("Y"); ?>&#174;</p>
		<!-- <p class="txt_footer">AVISO DE PRIVACIDAD</p> -->
		<a class="txt_footer" href="assets/pdf/aviso_vinaltura-3.pdf" target="_blank" rel="noopener noreferrer"><p>AVISO DE PRIVACIDAD</p></a>
		<a class="txt_footer" href="http://inkwonders.com/" target="_blank" rel="noopener noreferrer"><p>DESARROLLADO POR INKWONDERS</p></a>

	</div>

</footer>
