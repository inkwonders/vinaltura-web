<div class="cont_int_datos_vino section1">

	<div class="title_vino_movil">
		<p class="titulo_movil">Espuma Bajío Blanc de Blancs</p>
	</div>

	<img class="cirulo_movil" src="assets/img/uvas-moradas.jpg" alt="Vinaltura Espuma Bajío Blanc de Blancs">
	<img class="botella_vino_movil" src="assets/img/vinos/bajio/blanc_blancs.png" alt="Vinaltura rEspuma Bajío Blanc de Blancs">

	<a style="text-decoration: none; color:white;" href="https://tiendavinaltura.mx/" target="_blank">
		<div class="btn_tienda">COMPRAR</div>
	</a>

	<!-- <a target="_blank" href="assets/pdf/blanc_de_blancs.pdf"><div class="btn_tienda_movil">FICHA TECNICA</div></a> -->

	<div style="display: flex; flex-direction: column; justify-content: space-around; align-items: center; position: relative; width: 100%; height: 100px; top: 62%;">

	<a target="_blank" class="btn_tienda_movil_doble" href="assets/pdf/Ficha_Tecnica_Espuma_Bajio_Blanc_de_Blancs_2019.pdf">FICHA TECNICA 2019</a>

	<a target="_blank" class="btn_tienda_movil_doble" href="assets/pdf/blanc_de_blancs.pdf">FICHA TECNICA  </a>

	</div>

	<img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

	<div class="cont_nombre_tipo bajios_ubic">
		<p style="background-position: 83.4133% 152.643%;">Bajío</p>
	</div>

</div>

<div class="cont_int_datos_vino section2">

	<div class="cont_datos_movil_vino">

		<div class="grupo_movil">
			<p class="bold_movil">Familia:</p>
			<p class="normal_movil">Bajío</p>
		</div>

		<div class="grupo_movil">
			<p class="bold_movil">Origen:</p>
			<p class="normal_movil">Valle de Colón, QRO.</p>
		</div>

		<div class="grupo_movil">
			<p class="bold_movil">Varietal:</p>
			<p class="normal_movil">50% Chardonnay – 50% Chenin Blanc</p>
		</div>

		<div class="grupo_movil">
			<p class="bold_movil">Fermentación:</p>
			<p class="normal_movil">Espumoso método TRADICIONAL, doble fermentación, primera en tanque de acero inoxidable y segunda en botella.</p>
		</div>

		<div class="grupo_movil">
			<p class="bold_movil">Añejamiento:</p>
			<p class="normal_movil">18 meses en contacto con lías durante segunda fermentación.</p>
		</div>

		<div class="grupo_movil">
			<p class="bold_movil">Maridaje:</p>
			<p class="normal_movil">Ensaladas de frutas, tacos de pescado, pay de limón, barbacoa, quesadillas con flor de calabaza, huitlacoche y rajas.</p>
		</div>

	</div>

	<img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

</div>

<div class="cont_int_datos_vino section3">

	<div class="cont_datos_movil_vino_2">

		<p class="titulo_datos_movil">Nota de cata</p>

		<div class="grupo_movil2">
			<p class="bold_movil">Vista:</p>
			<p class="normal_movil">Amarillo paja con destellos dorados con una burbuja fina, constante y abundante; rosarios de burbujas muy marcados que terminan en una corona definida, resaltando la limpidez y la brillantez.</p>
		</div>

		<div class="grupo_movil2">
			<p class="bold_movil">Olfato:</p>
			<p class="normal_movil">Intensidad aroma alta donde resaltan notas frutales y dulces como la manzana, miel, semillas y frutos secos.</p>
		</div>

		<div class="grupo_movil2">
			<p class="bold_movil">Gusto:</p>
			<p class="normal_movil">Ataque amplio al paladar con una explosión de burbuja muy marcada y agradable, fresco, un vino seco y con una acidez jugosa.</p>
		</div>

	</div>

	<img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

</div>