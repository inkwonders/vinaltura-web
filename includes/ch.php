<div class="cont_int_datos_vino section1">

    <div class="title_vino_movil">
        <p class="titulo_movil">CH <br> <span class="ano_movil"></span></p>
    </div>


    <img class="cirulo_movil" src="assets/img/uvas-verdes.png" alt="Vinaltura">
    <img class="botella_vino_movil" src="assets/img/vinos/varietales/CH_Inclinada-8.png" alt="Vinaltura tinto bajio">

    <a style="text-decoration: none; color:white;" href="https://tiendavinaltura.mx/" target="_blank">
        <div class="btn_tienda">COMPRAR</div>
    </a>

    <!-- <a target="_blank" href="assets/pdf/Especiales GW Ancestral.pdf"><div class="btn_tienda_movil">FICHA TECNICA</div></a> -->

    <div style="display: flex; flex-direction: column; justify-content: space-around; align-items: center; position: relative; width: 100%; height: 100px; top: 62%;">

        <a target="_blank" class="btn_tienda_movil_doble" href="assets/pdf/ficha_tecnica_ch.pdf">FICHA TECNICA</a>
        
    </div>

    <img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

    <div class="cont_nombre_tipo bajios_ubic">
        <p style="background-position: 83.4133% 152.643%;">Bajío</p>
    </div>

</div>

<div class="cont_int_datos_vino section2">

    <div class="cont_datos_movil_vino">

        <div class="grupo_movil">
            <p class="bold_movil">Familia:</p>
            <p class="normal_movil">Varietales.</p>
        </div>

        <div class="grupo_movil">
            <p class="bold_movil">Origen:</p>
            <p class="normal_movil">Valle de Colón, QRO.</p>
        </div>

        <div class="grupo_movil">
            <p class="bold_movil">Varietal:</p>
            <p class="normal_movil">100% Chardonnay</p>
        </div>

        <div class="grupo_movil">
            <p class="bold_movil">Fermentación:</p>
            <p class="normal_movil">Tanques de acero inoxidable</p>
        </div>

        <div class="grupo_movil">
            <p class="bold_movil">Maridaje:</p>
            <p class="normal_movil">Ensalada con queso de cabra o una vinagreta característica, proteínas suaves, quesos o un postre con buena acidez.</p>
        </div>

    </div>

    <img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

</div>

<div class="cont_int_datos_vino section3">

    <div class="cont_datos_movil_vino_2">

        <p class="titulo_datos_movil">Nota de cata</p>

        <div class="grupo_movil2">
            <p class="bold_movil">Vista:</p>
            <p class="normal_movil">Vino limpio, color dorado con destellos cobrizos y densidad media.</p>
        </div>

        <div class="grupo_movil2">
            <p class="bold_movil">Olfato:</p>
            <p class="normal_movil">Intensidad aromática media, notas frutales resaltando piña, durazno y mango verde. Dejando una ligera nota a membrillo y caramelo.</p>
        </div>

        <div class="grupo_movil2">
            <p class="bold_movil">Gusto:</p>
            <p class="normal_movil">Vino fresco con un ataque amplio al paladar, acidez balanceada y untuoso.</p>
        </div>

    </div>

    <img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

</div>