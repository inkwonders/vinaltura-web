<div class="cont_int_datos_vino section1">

    <div class="title_vino_movil">
        <p class="titulo_movil">Vinaltura Merlot <br> <span class="ano_movil"></span></p>
    </div>


    <img class="cirulo_movil" src="assets/img/uvas-moradas.jpg" alt="Vinaltura">
    <img class="botella_vino_movil" src="assets/img/vinos/especiales/Merlot.png" alt="Vinaltura tinto bajio">

    <a style="text-decoration: none; color:white;" href="https://tiendavinaltura.mx/" target="_blank">
        <div class="btn_tienda">COMPRAR</div>
    </a>

    <a target="_blank" href="assets/pdf/Varietales MR.pdf"><div class="btn_tienda_movil">FICHA TECNICA</div></a>

    <img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

    <div class="cont_nombre_tipo bajios_ubic">
        <p style="background-position: 83.4133% 152.643%;">Bajío</p>
    </div>

</div>

<div class="cont_int_datos_vino section2">

    <div class="cont_datos_movil_vino">

        <div class="grupo_movil">
            <p class="bold_movil">Familia:</p>
            <p class="normal_movil">Ediciones especiales.</p>
        </div>

        <div class="grupo_movil">
            <p class="bold_movil">Origen:</p>
            <p class="normal_movil">Valle de Colón, QRO.</p>
        </div>

        <div class="grupo_movil">
            <p class="bold_movil">Varietal:</p>
            <p class="normal_movil">Merlot</p>
        </div>

        <div class="grupo_movil">
            <p class="bold_movil">Elaboración:</p>
            <p class="normal_movil">Selección de uva manual, fermentación controlada en tanque de acero inoxidable.</p>
        </div>

        <div class="grupo_movil">
            <p class="bold_movil">Maridaje:</p>
            <p class="normal_movil">Costillas asadas, pulpo a las brasas, hamburguesa de res, tacos de lengua.</p>
        </div>

    </div>

    <img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

</div>

<div class="cont_int_datos_vino section3">

    <div class="cont_datos_movil_vino_2">

        <p class="titulo_datos_movil">Nota de cata</p>

        <div class="grupo_movil2">
            <p class="bold_movil">Vista:</p>
            <p class="normal_movil">Capa media alta, color rojo carmín un ribete color granate.</p>
        </div>

        <div class="grupo_movil2">
            <p class="bold_movil">Olfato:</p>
            <p class="normal_movil">Intensidad aromática media, donde destacan notas frutales, frutales, y herbales, tales como cereza, zarzamora, ciruela pasa, jamaica, hierbabuena y rosas.</p>
        </div>

        <div class="grupo_movil2">
            <p class="bold_movil">Gusto:</p>
            <p class="normal_movil">Vino seco con un ataque medio, acidez media con un tanino sutil y elegante.</p>
        </div>

    </div>

    <img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

</div>