
	<div class="cont_int_datos_vino section1">

		<div class="title_vino_movil">
			<p class="titulo_movil">Espuma Rosé <br> <span class="ano_movil"></span></p>
		</div>

		<img class="cirulo_movil" src="assets/img/uvas-verdes-moradas.png" alt="Vinaltura - espuma rose">
		<img class="botella_vino_movil" src="assets/img/vinos/bajio/espuma-rose-bajio.png" alt="espuma rose bajio">

		<a style="text-decoration: none; color:white;" href="https://tiendavinaltura.mx/" target="_blank"><div class="btn_tienda">COMPRAR</div></a>

		<a target="_blank" href="assets/pdf/Espuma-Bajio-Rose.pdf"><div class="btn_tienda_movil" style="bottom: 20%; text-align:center">FICHA TECNICA 2018</div></a>
		<a target="_blank" href="assets/pdf/Espuma-Bajio-Rose-2019.pdf"><div class="btn_tienda_movil" style=" text-align:center">FICHA TECNICA 2019</div></a>

		<img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

		<div class="cont_nombre_tipo bajios_ubic">
				<p style="background-position: 83.4133% 152.643%;">Bajío</p>
		</div>

	</div>

	<div class="cont_int_datos_vino section2">

		<div class="cont_datos_movil_vino">

			<div class="grupo_movil">
				<p class="bold_movil">Familia:</p>
				<p class="normal_movil">Bajio.</p>
			</div>

			<div class="grupo_movil">
				<p class="bold_movil">Origen:</p>
				<p class="normal_movil">Valle de Colón, QRO.</p>
			</div>

			<div class="grupo_movil">
				<p class="bold_movil">Varietal:</p>
				<p class="normal_movil">Syrah y Chardonnay.</p>
			</div>

			<div class="grupo_movil">
				<p class="bold_movil">Fermentación:</p>
				<p class="normal_movil">Método tradicional.</p>
			</div>

			<div class="grupo_movil">
				<p class="bold_movil">Añejamiento:</p>
				<p class="normal_movil">18 meses en contacto con lías.</p>
			</div>

			<div class="grupo_movil">
				<p class="bold_movil">Maridaje:</p>
				<p class="normal_movil">Barbacoa de borrego, tacos de carnitas y moles.</p>
			</div>

		</div>

		<img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

	</div>

	<div class="cont_int_datos_vino section3">

		<div class="cont_datos_movil_vino_2">

			<p class="titulo_datos_movil">Nota de cata</p>

			<div class="grupo_movil2">
				<p class="bold_movil">Vista:</p>
				<p class="normal_movil">Color rosa salmón, formando en el centro rosarios de burbujas muy finas para terminar en la superficie con una elegante corona.</p>
			</div>

			<div class="grupo_movil2">
				<p class="bold_movil">Olfato:</p>
				<p class="normal_movil">Intensidad aromática media, carácter frutal destacando notas como arándano, frutos secos como nuez y almendras.</p>
			</div>

			<div class="grupo_movil2">
				<p class="bold_movil">Gusto:</p>
				<p class="normal_movil">Vino con un ataque amplio al paladar, buena acidez fresco con una burbuja muy delicada.</p>
			</div>

		</div>

		<img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

	</div>
