<div class="cont_int_datos_vino section1">

    <div class="title_vino_movil">
        <p class="titulo_movil">GW Dulce<br> <span class="ano_movil"></span></p>
    </div>


    <img class="cirulo_movil" src="assets/img/uvas-naranjas.png" alt="Vinaltura">
    <img class="botella_vino_movil" src="assets/img/vinos/especiales/gw-2019.png" alt="Vinaltura tinto bajio">

    <a style="text-decoration: none; color:white;" href="https://tiendavinaltura.mx/" target="_blank">
        <div class="btn_tienda">COMPRAR</div>
    </a>

    <a target="_blank" href="assets/pdf/Especiales-GW-dulce.pdf"><div class="btn_tienda_movil">FICHA TECNICA</div></a>

    <img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

    <div class="cont_nombre_tipo bajios_ubic">
        <p style="background-position: 83.4133% 152.643%;">Bajío</p>
    </div>

</div>

<div class="cont_int_datos_vino section2">

    <div class="cont_datos_movil_vino">

        <div class="grupo_movil">
            <p class="bold_movil">Familia:</p>
            <p class="normal_movil">Ediciones especiales.</p>
        </div>

        <div class="grupo_movil">
            <p class="bold_movil">Origen:</p>
            <p class="normal_movil">Valle de Colón, QRO.</p>
        </div>

        <div class="grupo_movil">
            <p class="bold_movil">Varietal:</p>
            <p class="normal_movil">Gewürztraminer</p>
        </div>

        <div class="grupo_movil">
            <p class="bold_movil">Maridaje:</p>
            <p class="normal_movil">Postres frescos como helados, ate con queso, strudel de manzana y quesos fuertes.</p>
        </div>

        <div class="grupo_movil">
            <p class="bold_movil">Elaboración:</p>
            <p class="normal_movil">Selección de uva manual, cosechado a 25°Brix y reposado en camas de pasificación. Prensado suave, fermentación en tanque de acero inoxidable, estabilización y filtración parcial.</p>
        </div>

    </div>

    <img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

</div>

<div class="cont_int_datos_vino section3">

    <div class="cont_datos_movil_vino_2">

        <p class="titulo_datos_movil">Nota de cata</p>

        <div class="grupo_movil2">
            <p class="bold_movil">Vista:</p>
            <p class="normal_movil">Vino amarillo paja con destellos dorados, limpio, brillante, densidad aparente elevada.</p>
        </div>

        <div class="grupo_movil2">
            <p class="bold_movil">Olfato:</p>
            <p class="normal_movil">Intensidad aromática elevada, notas florales, herbales y frutales, predominando las rosas, jazmín, gardenia y un con ligeras notas a litchi, miel y guanábana.</p>
        </div>

        <div class="grupo_movil2">
            <p class="bold_movil">Gusto:</p>
            <p class="normal_movil">Amable al paladar con buena untuosidad y un retrogusto que reafirma las notas florales. </p>
        </div>

    </div>

    <img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

</div>