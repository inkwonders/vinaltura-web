<div class="cont_int_datos_vino section1">

	<div class="title_vino_movil">
		<p class="titulo_movil">Terruño Arroyo <br> <span class="ano_movil"></span></p>
	</div>

	<img class="cirulo_movil" src="assets/img/uvas-moradas.jpg" alt="Vinaltura">
	<!-- <img class="botella_vino_movil" src="assets/img/vinos/terruños/arroyo-2016-terruno.png" alt="Vinaltura arroyo terruño"> -->
	<img class="botella_vino_movil" src="assets/img/vinos/terruños/Terruno Arroyo 2017.png" alt="Vinaltura arroyo terruño">

	<a style="text-decoration: none; color:white;" href="https://tiendavinaltura.mx/" target="_blank">
		<div class="btn_tienda">COMPRAR</div>
	</a>

	<div style="display: flex; flex-direction: column; justify-content: space-around; align-items: center; position: relative; width: 100%; height: 100px; top: 62%;">
		<a target="_blank" class="btn_tienda_movil_doble" href="assets/pdf/Terruño-Arroyo-2016.pdf">FICHA TECNICA 2016</a>
		<a target="_blank" class="btn_tienda_movil_doble" href="assets/pdf/Terruños Arroyo.pdf">FICHA TECNICA 2017</a>
	</div>

	<img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

	<div class="cont_nombre_tipo bajios_ubic otros">
		<p style="background-position: 83.4133% 152.643%;">Terruños</p>
	</div>

</div>

<div class="cont_int_datos_vino section2">

	<div class="cont_datos_movil_vino">

		<div class="grupo_movil">
			<p class="bold_movil">Familia:</p>
			<p class="normal_movil">Terruños.</p>
		</div>

		<div class="grupo_movil">
			<p class="bold_movil">Origen:</p>
			<p class="normal_movil">Valle de Guadalupe, B.C.</p>
		</div>

		<div class="grupo_movil">
			<p class="bold_movil">Varietal:</p>
			<p class="normal_movil">Merlot, Cabernet Sauvignon.</p>
		</div>

		<div class="grupo_movil">
			<p class="bold_movil">Fermentación:</p>
			<p class="normal_movil">Tanques de acero inoxidable.</p>
		</div>

		<div class="grupo_movil">
			<p class="bold_movil">Añejamiento:</p>
			<p class="normal_movil">12 meses de barrica de roble francés y americano, mínimo 12 meses de botella.</p>
		</div>

		<div class="grupo_movil">
			<p class="bold_movil">Maridaje:</p>
			<p class="normal_movil">Carnitas y enfrijoladas.</p>
		</div>

	</div>

	<img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

</div>

<div class="cont_int_datos_vino section3">

	<div class="cont_datos_movil_vino_2">

		<p class="titulo_datos_movil">Nota de cata</p>

		<div class="grupo_movil2">
			<p class="bold_movil">Vista:</p>
			<p class="normal_movil">Color granate con un ribete color cereza, vino limpio y brillante con una densidad aparente alta.</p>
		</div>

		<div class="grupo_movil2">
			<p class="bold_movil">Olfato:</p>
			<p class="normal_movil">Intensidad aromática elevada destacando notas de frutos como zarzamora y tostados como la canela y nuez.</p>
		</div>

		<div class="grupo_movil2">
			<p class="bold_movil">Gusto:</p>
			<p class="normal_movil">Vino redondo, con un ataque amplio al paladar con un tanino sedoso, buena acidez y excelente permanencia en boca.</p>
		</div>

	</div>

	<img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

</div>