<div class="cont_int_datos_vino section1">

    <div class="title_vino_movil">
        <p class="titulo_movil">GW Ancestral <br> <span class="ano_movil"></span></p>
    </div>


    <img class="cirulo_movil" src="assets/img/uvas-naranjas.png" alt="Vinaltura">
    <img class="botella_vino_movil" src="assets/img/vinos/especiales/gw-ancestral.png" alt="Vinaltura tinto bajio">

    <a style="text-decoration: none; color:white;" href="https://tiendavinaltura.mx/" target="_blank">
        <div class="btn_tienda">COMPRAR</div>
    </a>

    <!-- <a target="_blank" href="assets/pdf/Especiales GW Ancestral.pdf"><div class="btn_tienda_movil">FICHA TECNICA</div></a> -->

    <div style="display: flex; flex-direction: column; justify-content: space-around; align-items: center; position: relative; width: 100%; height: 100px; top: 62%;">

        <a target="_blank" class="btn_tienda_movil_doble" href="assets/pdf/Especiales GW Ancestral.pdf">FICHA TECNICA 2020</a>
        
        <a target="_blank" class="btn_tienda_movil_doble" href="assets/pdf/Ficha_Tecnica_GW_Ancestral_2021.pdf">FICHA TECNICA 2021
            
        </a>

    </div>

    <img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

    <div class="cont_nombre_tipo bajios_ubic">
        <p style="background-position: 83.4133% 152.643%;">Bajío</p>
    </div>

</div>

<div class="cont_int_datos_vino section2">

    <div class="cont_datos_movil_vino">

        <div class="grupo_movil">
            <p class="bold_movil">Familia:</p>
            <p class="normal_movil">Ediciones especiales.</p>
        </div>

        <div class="grupo_movil">
            <p class="bold_movil">Origen:</p>
            <p class="normal_movil">Valle de Colón, QRO.</p>
        </div>

        <div class="grupo_movil">
            <p class="bold_movil">Varietal:</p>
            <p class="normal_movil">Gewürztraminer</p>
        </div>

        <div class="grupo_movil">
            <p class="bold_movil">Fermentación:</p>
            <p class="normal_movil">Directa en botella con una duración de 2 meses y medio en contacto con las lías.</p>
        </div>

        <div class="grupo_movil">
            <p class="bold_movil">Maridaje:</p>
            <p class="normal_movil">Mole de xoconostle, costillas de cerdo, ensalada de frutos, pastel tres leches.</p>
        </div>

    </div>

    <img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

</div>

<div class="cont_int_datos_vino section3">

    <div class="cont_datos_movil_vino_2">

        <p class="titulo_datos_movil">Nota de cata</p>

        <div class="grupo_movil2">
            <p class="bold_movil">Vista:</p>
            <p class="normal_movil">Amarillo paja con destellos verdosos, con una turbidez media debido a su proceso de elaboración, burbuja pequeña que va por el centro de la copa reafirmando su naturaleza con una corona de media persistencia.</p>
        </div>

        <div class="grupo_movil2">
            <p class="bold_movil">Olfato:</p>
            <p class="normal_movil">Intensidad aromática elevada donde resaltan la frutalidad y la floralidad del varietal, notas como litchi, guanábana, rosas, flores blancas.</p>
        </div>

        <div class="grupo_movil2">
            <p class="bold_movil">Gusto:</p>
            <p class="normal_movil">Vino freso con una burbuja amplia al paladar, seco con una buena acidez y una burbuja noble.</p>
        </div>

    </div>

    <img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

</div>