<div class="cont_int_datos_vino section1">

	<div class="title_vino_movil">
		<p class="titulo_movil">Vinaltura Malbec<br> <span class="ano_movil"></span></p>
	</div>

	<img class="cirulo_movil" src="assets/img/uvas-moradas.jpg" alt="Vinaltura">
	<img class="botella_vino_movil" src="assets/img/vinos/varietales/malbec.png" alt="Vinaltura malbec varietales">

	<a style="text-decoration: none; color:white;" href="https://tiendavinaltura.mx/" target="_blank">
		<div class="btn_tienda">COMPRAR</div>
	</a>

	<a target="_blank" href="assets/pdf/Malbec-2019.pdf"><div class="btn_tienda_movil">FICHA TECNICA</div></a>

	<img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

	<div class="cont_nombre_tipo bajios_ubic otros">
		<p style="background-position: 83.4133% 152.643%;">Varietales</p>
	</div>

</div>

<div class="cont_int_datos_vino section2">

	<div class="cont_datos_movil_vino">

		<div class="grupo_movil">
			<p class="bold_movil">Familia:</p>
			<p class="normal_movil">Varietales.</p>
		</div>

		<div class="grupo_movil">
			<p class="bold_movil">Origen:</p>
			<p class="normal_movil">Valle de Colón, QRO.</p>
		</div>

		<div class="grupo_movil">
			<p class="bold_movil">Varietal:</p>
			<p class="normal_movil">Chenin Blanc.</p>
		</div>

		<div class="grupo_movil">
			<p class="bold_movil">Fermentación:</p>
			<p class="normal_movil">Tanques de acero inoxidable.</p>
		</div>

		<div class="grupo_movil">
			<p class="bold_movil">Añejamiento:</p>
			<p class="normal_movil">12 meses de barrica de roble francés y americano, mínimo 6 meses de botella.</p>
		</div>

		<div class="grupo_movil">
			<p class="bold_movil">Maridaje:</p>
			<p class="normal_movil">Quesos suaves, ensalada de betabel con queso de cabra, pay de queso con frutos rojos.</p>
		</div>

	</div>

	<img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

</div>

<div class="cont_int_datos_vino section3">

	<div class="cont_datos_movil_vino_2">

		<p class="titulo_datos_movil">Nota de cata</p>

		<div class="grupo_movil2">
			<p class="bold_movil">Vista:</p>
			<p class="normal_movil">Capa de color alta, color violáceo intenso, con un ribete color granate, vino limpio y brillante con una densidad aparente media alta.</p>
		</div>

		<div class="grupo_movil2">
			<p class="bold_movil">Olfato:</p>
			<p class="normal_movil">Intensidad aromática elevada, notas frutales, dando como resultado notas compotadas de zarzamora y fresa.</p>
		</div>

		<div class="grupo_movil2">
			<p class="bold_movil">Gusto:</p>
			<p class="normal_movil">Ataque amplio al paladar con un tanino elegante, buena acidez y muy fresco.</p>
		</div>

	</div>

	<img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

</div>