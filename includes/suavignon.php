<div class="cont_int_datos_vino section1">

	<div class="title_vino_movil">
		<p class="titulo_movil">Vinaltura Sauvignon Blanc<br> <span class="ano_movil"></span></p>
	</div>

	<img class="cirulo_movil" src="assets/img/uvas-verdes.png" alt="Vinaltura">
	<img class="botella_vino_movil" src="assets/img/vinos/varietales/suavignon-blanc-2019.png" alt="Vinaltura suavignon blanc varietales">

	<a style="text-decoration: none; color:white;" href="https://tiendavinaltura.mx/" target="_blank">
		<div class="btn_tienda">COMPRAR</div>
	</a>

	<a target="_blank" href="assets/pdf/Varietales SB.pdf"><div class="btn_tienda_movil">FICHA TECNICA</div></a>

	<img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

	<div class="cont_nombre_tipo bajios_ubic otros">
		<p style="background-position: 83.4133% 152.643%;">Varietales</p>
	</div>

</div>

<div class="cont_int_datos_vino section2">

	<div class="cont_datos_movil_vino">

		<div class="grupo_movil">
			<p class="bold_movil">Familia:</p>
			<p class="normal_movil">Varietales.</p>
		</div>

		<div class="grupo_movil">
			<p class="bold_movil">Origen:</p>
			<p class="normal_movil">Valle de Colón, QRO.</p>
		</div>

		<div class="grupo_movil">
			<p class="bold_movil">Varietal:</p>
			<p class="normal_movil">Sauvignon Blanc.</p>
		</div>

		<div class="grupo_movil">
			<p class="bold_movil">Fermentación:</p>
			<p class="normal_movil">Tanques de acero inoxidable.</p>
		</div>

		<div class="grupo_movil">
			<p class="bold_movil">Añejamiento:</p>
			<p class="normal_movil">Sin Barrica, mínimo 6 meses de botella.</p>
		</div>

		<div class="grupo_movil">
			<p class="bold_movil">Maridaje:</p>
			<p class="normal_movil">Mariscos y aves con salsas blancas.</p>
		</div>

	</div>

	<img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

</div>

<div class="cont_int_datos_vino section3">

	<div class="cont_datos_movil_vino_2">

		<p class="titulo_datos_movil">Nota de cata</p>

		<div class="grupo_movil2">
			<p class="bold_movil">Vista:</p>
			<p class="normal_movil">Limpio y brillante, color amarillo pajizo con un ribete color dorado, con una densidad aparente media.</p>
		</div>

		<div class="grupo_movil2">
			<p class="bold_movil">Olfato:</p>
			<p class="normal_movil">Intensidad aromática alta de carácter floral y frutal sobresaliendo notas de lima, manzanilla y miel. GUSTO: Buena estructura y acidez, bastante fresco y equilibrado.</p>
		</div>

		<div class="grupo_movil2">
			<p class="bold_movil">Gusto:</p>
			<p class="normal_movil">Buena estructura y acidez, bastante fresco y equilibrado.</p>
		</div>

	</div>

	<img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

</div>