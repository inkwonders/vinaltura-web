<div class="cont_int_datos_vino section1">

	<div class="title_vino_movil">
		<p class="titulo_movil">Tinto <br> <span class="ano_movil"></span></p>
	</div>


	<img class="cirulo_movil" src="assets/img/uvas-moradas.jpg" alt="Vinaltura">
	<img class="botella_vino_movil" src="assets/img/vinos/bajio/tinto-bajio.png" alt="Vinaltura tinto bajio">

	<a style="text-decoration: none; color:white;" href="https://tiendavinaltura.mx/" target="_blank">
		<div class="btn_tienda">COMPRAR</div>
	</a>

	<a target="_blank" href="assets/pdf/Bajío Tinto Bajío.pdf"><div class="btn_tienda_movil">FICHA TECNICA</div></a>

	<img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

	<div class="cont_nombre_tipo bajios_ubic">
		<p style="background-position: 83.4133% 152.643%;">Bajío</p>
	</div>

</div>

<div class="cont_int_datos_vino section2">

	<div class="cont_datos_movil_vino">

		<div class="grupo_movil">
			<p class="bold_movil">Familia:</p>
			<p class="normal_movil">Bajio.</p>
		</div>

		<div class="grupo_movil">
			<p class="bold_movil">Origen:</p>
			<p class="normal_movil">Valle de Colón, QRO.</p>
		</div>

		<div class="grupo_movil">
			<p class="bold_movil">Varietal:</p>
			<p class="normal_movil">Tempranillo, Merlot, Petit Syrah.</p>
		</div>

		<div class="grupo_movil">
			<p class="bold_movil">Fermentación:</p>
			<p class="normal_movil">Tanques de acero inoxidable.</p>
		</div>

		<div class="grupo_movil">
			<p class="bold_movil">Añejamiento:</p>
			<p class="normal_movil">12 meses en barrica de roble francés y americano, mínimo 6 meses de botella.</p>
		</div>

		<div class="grupo_movil">
			<p class="bold_movil">Maridaje:</p>
			<p class="normal_movil">Gorditas de migajas y quesadillas con guisados.</p>
		</div>

	</div>

	<img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

</div>

<div class="cont_int_datos_vino section3">

	<div class="cont_datos_movil_vino_2">

		<p class="titulo_datos_movil">Nota de cata</p>

		<div class="grupo_movil2">
			<p class="bold_movil">Vista:</p>
			<p class="normal_movil">Capa de color media, con tonalidades rojo carmesí con ribete color marrón, alta limpidez y brillantez.</p>
		</div>

		<div class="grupo_movil2">
			<p class="bold_movil">Olfato:</p>
			<p class="normal_movil">Buena intensidad aromática, destacando notas especiadas y herbales tales como pimienta, canela, eucalipto.</p>
		</div>

		<div class="grupo_movil2">
			<p class="bold_movil">Gusto:</p>
			<p class="normal_movil">Buena acidez, tanino sedoso y carnoso, con agradable permanencia en boca.</p>
		</div>

	</div>

	<img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

</div>