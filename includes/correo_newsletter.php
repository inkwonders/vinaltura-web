<?php

	$correo = $_POST["txt_unirse"];

	echo "llego correo :  ".$correo;

	if (isset($correo)) {

	// $to = "contacto@vinaltura.mx, eddy@inkwonders.com, pablo@inkwonders.com, administracion@vinaltura.mx, amonola@vinaltura.mx, notificaciones@inkwonders.com";
	$to = "contacto@vinaltura.mx, administracion@vinaltura.mx, amonola@vinaltura.mx";
	// $to = "oswaldoferral@gmail.com";
	$subject = "Sitio web de Vinaltura";

	$message = '<!DOCTYPE html>

	<html lang="es">
	
		<link rel="preconnect" href="https://fonts.googleapis.com">
		<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
		<link href="https://fonts.googleapis.com/css2?family=Raleway&display=swap" rel="stylesheet">
	
		<style>
	
			*{
	
				padding         :       0;
	
				margin          :       0;
	
				font-family: "Raleway", sans-serif;
	
				font-size: 14px;
	
			}
	
			.tabla{
	
				display: table;
	
				width: 682px;
	
				background-color: #161616;
	
				color: #fff;
	
				padding: 30px 0;
	
				margin: auto;
	
			}
	
			.celda{
	
				display: table-cell;
	
				width: 33.3%;
	
				vertical-align: middle;
	
			}
	
			.bg-fondo{
	
				margin: auto;
	
				width: 680px;
	
				height: 1100px;
	
				background-image: url("http://127.0.0.1:8000/img/suscriptor.jpg");
	
				background-size: cover;
	
				position: relative;
	
			}
	
			.btn{
	
				background-color: #161616;
	
				color: #fff;
	
				text-decoration: none;
	
				display: flex;
	
				align-items: center;
	
				height: 50px;
	
				width: 250px;
	
				text-align: center;
	
				justify-content: center;
	
				border-radius: 10px;
	
				position: absolute;
	
				top: 580px;
	
				left: 28%;
	
				transform: translate(-50%);
	
			}
	
			.texto-volador {
	
				color: #503629;
	
				position: absolute;
	
				top: 523px;
	
				left: 10%;
	
				font-size: 24px;
	
			}
	
			#mensaje{
	
				width: 680px;
	
			}
	
			.redes-sociales{
	
				width: 50px;
	
				height: 50px;
	
				background-color: #fff;
	
				display: flex;
	
				align-items: center;
	
				text-align: center;
	
				justify-content: center;
	
				margin: 0 5px;
	
				border-radius: 36px;
	
			}
	
			.redes-sociales .circulo{
	
				border: solid #503629 2px;
	
				border-radius: 36px;
	
				width: 35px;
	
				height: 35px;
	
				display: flex;
	
				align-items: center;
	
				text-align: center;
	
				justify-content: center;
	
			}
	
			.redes-sociales img{
	
				width: 17px;
	
				height: 17px;
	
			}
	
			.contenedor-redes{
	
				display: flex;
	
				justify-items: center;
	
				justify-content: center;
	
			}
	
			a{
	
				text-decoration: none;
	
				color: #fff;
	
			}
	
		</style>
	
	<head>
	
		<meta charset="UTF-8">
	
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
		<title>WINE CLUB</title>
	
	</head>
	
	<body>
	
		<section id="mensaje">
	
			<div class="bg-fondo">
	
				<a href="https://vinaltura.mx" target="_blank" class="btn">
	
					ADMINISTRAR
	
				</a>
	
				<span class="texto-volador">
	
					<b>
	
						<i style="font-size: 23px;">
	
							' .$correo.'
	
						</i>
	
					</b>
	
					se ha unido al Newsletter
	
				</span>
	
			</div>
	
			<div class="tabla">
	
				<div class="celda">
	
					<div class="contenedor-redes">
	
						<div class="redes-sociales">
	
							<a target="_blank" href="https://www.facebook.com/vinaltura/">
	
								<div class="circulo">
	
									<img src="http://127.0.0.1:8000/img/facebook.svg" alt="">
	
								</div>
	
							</a>
	
	
						</div>
	
						<div class="redes-sociales">
	
							<a href="https://www.instagram.com/vinaltura/" target="_blank">
	
								<div class="circulo">
	
									<img src="http://127.0.0.1:8000/img/instagram.svg" alt="">
	
								</div>
	
							</a>
	
						</div>
	
					</div>
	
				</div>
	
				<div class="celda">
	
					<a target="_blank" href="https://g.page/VINALTURA?share">
	
						Ignacio Zaragoza S/N, Santa <br/>
	
						Rosa de Lima Colón, <br/>
	
						Querétaro de Arteaga <br/>
	
						México. <br/>
	
					</a>
	
				</div>
	
				<div class="celda" style="text-align: center;">
	
					© VINALTURA 2022
	
				</div>
	
			</div>
	
	
		</section>
	
	
	</body>
	
	</html>
	';

	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	$headers .= 'From: Vinaltura<contacto@vinaltura.mx>' . "\r\n";

// mail de respuesta para el cliente que desea info wine club
	$to2 = $_POST["correo_wine_club"];
	// $to2 = "mario@inkwonders.com,guillo@inkwonders.com";
	$subject2 = "Sitio web de Vinaltura";

	// $message2 = "<html>
	// 			<head>
	// 				<title>Contacto</title>
	// 			</head>
	// 			<body style='padding:0; margin:0; color:#000;'>
	// 				<table cellpadding='0' cellspacing='0'>
	// 					<tr>
	// 							<td align='left' style='color:#000'>
	// 							<b>Muchas gracias por ponerte en contacto con nosotros, tu correo ha sido registrado exitosamente.</b><br /><br />
	// 							<b>Un asesor se pondrá en contacto contigo muy pronto.</b>
	// 							<br /><br />
	// 							<b>Nombre:</b> " . $_POST["nombre_wine_club"] . "
	// 							<br /><br />
	// 							<b>Correo:</b> <a href='mailto:" . $_POST["correo_wine_club"] . "' style='color:#90806A;'>" . $_POST["correo_wine_club"] . "</a>
	// 						</td>
	// 					</tr>
	// 				</table>
	// 			</body>
	// 		</html>";

	$message2 = '
	<!DOCTYPE html>

	<html lang="es">
	
		<link rel="preconnect" href="https://fonts.googleapis.com">
		<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
		<link href="https://fonts.googleapis.com/css2?family=Raleway&display=swap" rel="stylesheet">
	
		<style>
	
			*{
	
				padding         :       0;
	
				margin          :       0;
	
				font-family: "Raleway", sans-serif;
	
				font-size: 14px;
	
			}
	
			.tabla{
	
				display: table;
	
				width: 682px;
	
				background-color: #161616;
	
				color: #fff;
	
				padding: 30px 0;
	
				margin: auto;
			}
	
			.celda{
	
				display: table-cell;
	
				width: 33.3%;
	
				vertical-align: middle;
	
			}
	
			.bg-fondo{
	
				margin: auto;
	
				width: 680px;
	
				height: 1100px;
	
				background-image: url("http://127.0.0.1:8000/img/newsletter.jpg");
	
				background-size: cover;
	
				position: relative;
	
			}
	
			.btn{
	
				background-color: #fd0311;
	
				color: #fff;
	
				text-decoration: none;
	
				display: flex;
	
				align-items: center;
	
				height: 50px;
	
				width: 250px;
	
				text-align: center;
	
				justify-content: center;
	
				border-radius: 10px;
	
				position: absolute;
	
				bottom: 3%;
	
				left: 50%;
	
				transform: translate(-50%);
	
			}
	
			#mensaje{
	
				width: 680px;
	
			}
	
			.redes-sociales{
	
				width: 50px;
	
				height: 50px;
	
				background-color: #fff;
	
				display: flex;
	
				align-items: center;
	
				text-align: center;
	
				justify-content: center;
	
				margin: 0 5px;
	
				border-radius: 36px;
	
			}
	
			.redes-sociales .circulo{
	
				border: solid #503629 2px;
	
				border-radius: 36px;
	
				width: 35px;
	
				height: 35px;
	
				display: flex;
	
				align-items: center;
	
				text-align: center;
	
				justify-content: center;
	
			}
	
			.redes-sociales img{
	
				width: 17px;
	
				height: 17px;
	
			}
	
			.contenedor-redes{
	
				display: flex;
	
				justify-items: center;
	
				justify-content: center;
	
			}
	
			a{
	
				text-decoration: none;
	
				color: #fff;
	
			}
	
		</style>
	
	<head>
	
		<meta charset="UTF-8">
	
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
		<title>WINE CLUB</title>
	
	</head>
	
	<body>
	
		<section id="mensaje">
	
			<div class="bg-fondo">
	
				<!-- <a href="https://vinaltura.mx" target="_blank" class="btn">
	
					VISITA LA PÁGINA
	
				</a> -->
	
			</div>
	
			<div class="tabla">
	
				<div class="celda">
	
					<div class="contenedor-redes">
	
						<div class="redes-sociales">
	
							<a target="_blank" href="https://www.facebook.com/vinaltura/">
	
								<div class="circulo">
	
									<img src="http://127.0.0.1:8000/img/facebook.svg" alt="">
	
								</div>
	
							</a>
	
	
						</div>
	
						<div class="redes-sociales">
	
							<a href="https://www.instagram.com/vinaltura/" target="_blank">
	
								<div class="circulo">
	
									<img src="http://127.0.0.1:8000/img/instagram.svg" alt="">
	
								</div>
	
							</a>
	
						</div>
	
					</div>
	
				</div>
	
				<div class="celda">
	
					<a target="_blank" href="https://g.page/VINALTURA?share">
	
						Ignacio Zaragoza S/N, Santa <br/>
	
						Rosa de Lima Colón, <br/>
	
						Querétaro de Arteaga <br/>
	
						México. <br/>
	
					</a>
	
				</div>
	
				<div class="celda" style="text-align: center;">
	
					© VINALTURA 2022
	
				</div>
	
			</div>
	
	
		</section>
	
	
	</body>
	
	</html>
	
	';

	$headers2 = "MIME-Version: 1.0" . "\r\n";
	$headers2 .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	$headers2 .= 'From: Vinaltura<contacto@vinaltura.mx>' . "\r\n";

	if(mail($to,$subject,$message,$headers)){
		echo "correcto";
		// if(mail($to2,$subject2,$message2,$headers2)){
		// echo "correcto";
		// }
		// else{

		// 	echo "incorrecto";
	
		// }
	}else{

		echo "incorrecto";

	}

	}else{
		echo "va vacio";
	}
