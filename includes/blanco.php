<div class="cont_int_datos_vino section1">

	<div class="title_vino_movil">
		<p class="titulo_movil">Blanco Vinaltura <br> <span class="ano_movil"></span></p>
	</div>

	<img class="cirulo_movil" src="assets/img/uvas-verdes.png" alt="Vinaltura">
	<img class="botella_vino_movil" src="assets/img/vinos/bajio/blanco-sin-año.png" alt="Vinaltura blanco bajio">

	<a style="text-decoration: none; color:white;" href="https://tiendavinaltura.mx/" target="_blank">
		<div class="btn_tienda">COMPRAR</div>
	</a>

	<a target="_blank" href="assets/pdf/Bajío Blanco Bajío.pdf"><div class="btn_tienda_movil">FICHA TECNICA</div></a>

	<img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

	<div class="cont_nombre_tipo bajios_ubic">
		<p style="background-position: 83.4133% 152.643%;">Bajío</p>
	</div>

</div>

<div class="cont_int_datos_vino section2">

	<div class="cont_datos_movil_vino">

		<div class="grupo_movil">
			<p class="bold_movil">Familia:</p>
			<p class="normal_movil">Bajio.</p>
		</div>

		<div class="grupo_movil">
			<p class="bold_movil">Origen:</p>
			<p class="normal_movil">Valle de Colón, QRO.</p>
		</div>

		<div class="grupo_movil">
			<p class="bold_movil">Varietal:</p>
			<p class="normal_movil">Chenin Blanc.</p>
		</div>

		<div class="grupo_movil">
			<p class="bold_movil">Fermentación:</p>
			<p class="normal_movil">Tanques de acero inoxidable.</p>
		</div>

		<div class="grupo_movil">
			<p class="bold_movil">Añejamiento:</p>
			<p class="normal_movil">Sin Barrica, mínimo 6 meses de botella.</p>
		</div>

		<div class="grupo_movil">
			<p class="bold_movil">Maridaje:</p>
			<p class="normal_movil">Antojitos mexicanos, tacos de bistec, pizza pera y gorgonzola.</p>
		</div>

	</div>

	<img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

</div>

<div class="cont_int_datos_vino section3">

	<div class="cont_datos_movil_vino_2">

		<p class="titulo_datos_movil">Nota de cata</p>

		<div class="grupo_movil2">
			<p class="bold_movil">Vista:</p>
			<p class="normal_movil">Vino limpio y brillante, color amarillo paja con destellos dorados.</p>
		</div>

		<div class="grupo_movil2">
			<p class="bold_movil">Olfato:</p>
			<p class="normal_movil">Intensidad aromática alta de carácter frutal, resaltando notas como chabacano, carambola y ciruela amarilla.</p>
		</div>

		<div class="grupo_movil2">
			<p class="bold_movil">Gusto:</p>
			<p class="normal_movil">Ataque amplio al paladar, vino fresco, untuoso y buena acidez.</p>
		</div>

	</div>

	<img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

</div>