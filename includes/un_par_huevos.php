<div class="cont_int_datos_vino section1">

    <div class="title_vino_movil">
        <p class="titulo_movil">Un par de ... (Blanco)</p>
    </div>


    <img class="cirulo_movil" src="assets/img/uvas-verdes.png" alt="Vinaltura">
    <img class="botella_vino_movil" src="assets/img/vinos/especiales/un-par-de.png" alt="Vinaltura un par ...">

    <div class="btn_tienda"><a style="text-decoration: none; color:white;" href="https://tiendavinaltura.mx/" target="_blank">COMPRAR</a></div>

    <a target="_blank" href="assets/pdf/Especiales_Un_par_de.pdf">
        <div class="btn_tienda_movil">FICHA TECNICA</div>
    </a>

    <img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

    <div class="cont_nombre_tipo bajios_ubic no_bajios_ubic">
        <p style="background-position: 83.4133% 152.643%;"> ESPECIALES</p>
    </div>

</div>

<div class="cont_int_datos_vino section2">

    <div class="cont_datos_movil_vino">

        <div class="grupo_movil">
            <p class="bold_movil">Familia:</p>
            <p class="normal_movil">Ediciones especiales.</p>
        </div>

        <!-- <div class="grupo_movil">
            <p class="bold_movil">Origen:</p>
            <p class="normal_movil">Vinaltura y Freixenet</p>
        </div> -->

        <div class="grupo_movil">
            <p class="bold_movil">Variedad:</p>
            <p class="normal_movil">(I) Albariño <br> (D) Verdejo</p>
        </div>

        <!-- <div class="grupo_movil">
            <p class="bold_movil">Casa:</p>
            <p class="normal_movil">Vinaltura y Freixenet.</p>
        </div> -->

        <div class="grupo_movil">
            <p class="bold_movil">Winemaker:</p>
            <p class="normal_movil">(I) Hans Duer <br> (D) Lluis Raventós.</p>
        </div>

        <!-- <div class="grupo_movil">
            <p class="bold_movil">Añejamiento:</p>
            <p class="normal_movil">Lorem Ipsum.</p>
        </div> -->

        <div class="grupo_movil">
            <p class="bold_movil">Maridaje:</p>
            <p class="normal_movil">Ceviche de pescado, tacos estilo ensenada, quesadillas de flor de calabaza y pay de limón.</p>
        </div>

    </div>

    <img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

</div>

<div class="cont_int_datos_vino section3">

    <div class="cont_datos_movil_vino_2">

        <p class="titulo_datos_movil">Nota de cata</p>

        <div class="grupo_movil2">
            <p class="bold_movil">Vista:</p>
            <p class="normal_movil">(I) Amarillo paja con destellos dorados, limpio, brillante con una densidad alta. (D) Amarillo paja con destellos verdosos y plateados, limpio, brillante con una densidad media alta. </p>
        </div>

        <div class="grupo_movil2">
            <p class="bold_movil">Olfato:</p>
            <p class="normal_movil">(I) Intensidad aromática media donde resaltan notas como flores y frutos tales como azucenas, melón, piña, pera y maracuyá. (D) Intensidad aromática media donde resaltan notas frutales tropicales como mango, piña y manzana verde. </p>
        </div>

        <div class="grupo_movil2">
            <p class="bold_movil">Gusto:</p>
            <p class="normal_movil">(I) Vino suave con una acidez equilibrada, fresco, seco y un retrogusto donde recordamos las frutas como piña y melón. (D) Vino suave y fresco, con un equilibrio entre la acidez y el alcohol.</p>
        </div>

    </div>

    <img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

</div>