<div class="cont_int_datos_vino section1">

    <div class="title_vino_movil">
        <p class="titulo_movil">Terruno bajio<br> <span class="ano_movil"></span></p>
    </div>

    <img class="cirulo_movil" src="assets/img/uvas-moradas.jpg" alt="Vinaltura">
    <img class="botella_vino_movil" src="assets/img/vinos/terruños/terruno-bajio.png" alt="Vinaltura blanco bajio">

    <a style="text-decoration: none; color:white;" href="https://tiendavinaltura.mx/" target="_blank">
        <div class="btn_tienda">COMPRAR</div>
    </a>

    <a target="_blank" href="assets/pdf/Terruños Bajío.pdf"><div class="btn_tienda_movil">FICHA TECNICA</div></a>

    <img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

    <div class="cont_nombre_tipo bajios_ubic">
        <p style="background-position: 83.4133% 152.643%;">Terruños</p>
    </div>

</div>

<div class="cont_int_datos_vino section2">

    <div class="cont_datos_movil_vino">

        <div class="grupo_movil">
            <p class="bold_movil">Familia:</p>
            <p class="normal_movil">Terruños.</p>
        </div>

        <div class="grupo_movil">
            <p class="bold_movil">Origen:</p>
            <p class="normal_movil">Valle de Colón, QRO.</p>
        </div>

        <div class="grupo_movil">
            <p class="bold_movil">Varietal:</p>
            <p class="normal_movil">Merlot, Malbec y Tempranillo.</p>
        </div>

        <div class="grupo_movil">
            <p class="bold_movil">Fermentación:</p>
            <p class="normal_movil">En tanque de acero inoxidable, con una crianza de 12 meses en barrica de roble francés y americano.</p>
        </div>

        <div class="grupo_movil">
            <p class="bold_movil">Maridaje:</p>
            <p class="normal_movil">Pasta con salsas rojas, enchiladas queretanas, pierna mechada, gorditas queretanas.</p>
        </div>

    </div>

    <img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

</div>

<div class="cont_int_datos_vino section3">

    <div class="cont_datos_movil_vino_2">

        <p class="titulo_datos_movil">Nota de cata</p>

        <div class="grupo_movil2">
            <p class="bold_movil">Vista:</p>
            <p class="normal_movil">Capa media alta y alta densidad aparente, color granate con un ribete púrpura.</p>
        </div>

        <div class="grupo_movil2">
            <p class="bold_movil">Olfato:</p>
            <p class="normal_movil">Intensidad aromática media donde resaltan notas frutales y de barrica, tales como frutos rojos deshidratados, compota de frutos, notas tostadas y caramelo.</p>
        </div>

        <div class="grupo_movil2">
            <p class="bold_movil">Gusto:</p>
            <p class="normal_movil">Ataque medio con tanino consistente y aterciopelado, una acidez y retrogusto medio.</p>
        </div>

    </div>

    <img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

</div>