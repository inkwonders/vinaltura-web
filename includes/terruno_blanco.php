<div class="cont_int_datos_vino section1">

    <div class="title_vino_movil">
        <p class="titulo_movil">Terruno blanco<br> <span class="ano_movil"></span></p>
    </div>

    <img class="cirulo_movil" src="assets/img/uvas-verdes.png" alt="Vinaltura">
    <img class="botella_vino_movil" src="assets/img/vinos/terruños/terruno-blanco.png" alt="Vinaltura blanco bajio">

    <a style="text-decoration: none; color:white;" href="https://tiendavinaltura.mx/" target="_blank">
        <div class="btn_tienda">COMPRAR</div>
    </a>

    <a target="_blank" href="assets/pdf/Terruños Blanco.pdf"><div class="btn_tienda_movil">FICHA TECNICA</div></a>

    <img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

    <div class="cont_nombre_tipo bajios_ubic">
        <p style="background-position: 83.4133% 152.643%;">Terruños</p>
    </div>

</div>

<div class="cont_int_datos_vino section2">

    <div class="cont_datos_movil_vino">

        <div class="grupo_movil">
            <p class="bold_movil">Familia:</p>
            <p class="normal_movil">Terruños.</p>
        </div>

        <div class="grupo_movil">
            <p class="bold_movil">Origen:</p>
            <p class="normal_movil">Valle de Colón, QRO.</p>
        </div>

        <div class="grupo_movil">
            <p class="bold_movil">Varietal:</p>
            <p class="normal_movil">Riesling, Chardonnay, Chenin Blanc, Sauvignon Blanc y Gewürztraminer.</p>
        </div>

        <div class="grupo_movil">
            <p class="bold_movil">Fermentación:</p>
            <p class="normal_movil">Controlada en tanque de acero inoxidable, estabilización y filtración parcial.</p>
        </div>

        <div class="grupo_movil">
            <p class="bold_movil">Maridaje:</p>
            <p class="normal_movil">Crema de almejas, tacos de camarón y toast de frutas con miel.</p>
        </div>

    </div>

    <img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

</div>

<div class="cont_int_datos_vino section3">

    <div class="cont_datos_movil_vino_2">

        <p class="titulo_datos_movil">Nota de cata</p>

        <div class="grupo_movil2">
            <p class="bold_movil">Vista:</p>
            <p class="normal_movil">Vino cristalino, amarillo paja con destellos plateados.</p>
        </div>

        <div class="grupo_movil2">
            <p class="bold_movil">Olfato:</p>
            <p class="normal_movil">Buena intensidad aromática donde resaltan frutos tropicales como piña, durazno, mango y notas de flores blancas.</p>
        </div>

        <div class="grupo_movil2">
            <p class="bold_movil">Gusto:</p>
            <p class="normal_movil">Ataque fresco al paladar, reafirmando la frutalidad, equilibrado y buen de acompañante de las tardes calurosas.</p>
        </div>

    </div>

    <img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

</div>