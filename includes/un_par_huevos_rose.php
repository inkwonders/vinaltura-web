<div class="cont_int_datos_vino section1">

    <div class="title_vino_movil">
        <p class="titulo_movil">Un par de ... (Rosé)</p>
    </div>


    <img class="cirulo_movil" src="assets/img/uvas-moradas.png" alt="Vinaltura">
    <img class="botella_vino_movil" src="assets/img/vinos/especiales/Un-par-de-Rose_inclinada.png" alt="Vinaltura un par ...">

    <div class="btn_tienda"><a style="text-decoration: none; color:white;" href="https://tiendavinaltura.mx/" target="_blank">COMPRAR</a></div>

    <a target="_blank" href="assets/pdf/Un-par-de_Rose_2023_1.pdf">
        <div class="btn_tienda_movil">FICHA TECNICA</div>
    </a>

    <img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

    <div class="cont_nombre_tipo bajios_ubic no_bajios_ubic">
        <p style="background-position: 83.4133% 152.643%;"> ESPECIALES</p>
    </div>

</div>

<div class="cont_int_datos_vino section2">

    <div class="cont_datos_movil_vino">

        <div class="grupo_movil">
            <p class="bold_movil">Familia:</p>
            <p class="normal_movil">Ediciones especiales.</p>
        </div>

        <!-- <div class="grupo_movil">
            <p class="bold_movil">Origen:</p>
            <p class="normal_movil">Vinaltura y Freixenet</p>
        </div> -->

        <div class="grupo_movil">
            <p class="bold_movil">Varietal:</p>
            <p class="normal_movil">(I) 100% Grenache <br>  (D) 100% Zinfandel</p>
        </div>

        <div class="grupo_movil">
            <p class="bold_movil">Fermentación:</p>
            <p class="normal_movil">Tanques de acero inoxidable en forma ovoidal (huevo)</p>
        </div>

        <div class="grupo_movil">
            <p class="bold_movil">Añejamiento:</p>
            <p class="normal_movil">Sin barrica, 6 meses en botella.</p>
        </div>

        <!-- <div class="grupo_movil">
            <p class="bold_movil">Añejamiento:</p>
            <p class="normal_movil">Lorem Ipsum.</p>
        </div> -->

        <div class="grupo_movil">
            <p class="bold_movil">Vista:</p>
            <p class="normal_movil">(I) Color rosa medio con destellos cereza, con una densidad media, limpio y brillante.  (D) Elegante color a palo de rosa, aspecto  limpio y brillante con una densidad media.</p>
        </div>

    </div>

    <img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

</div>

<div class="cont_int_datos_vino section3">

    <div class="cont_datos_movil_vino_2">

        <!-- <p class="titulo_datos_movil">Nota de cata</p> -->

        <div class="grupo_movil2">
            <p class="bold_movil">Olfato:</p>
            <p class="normal_movil">(I) Intensidad aromática media, donde resaltan frutos rojos y amarillos como frambuesa, cereza y manzana, con notas herbales similares a menta y eucalipto.  (D) Intensidad aromática media baja, donde resaltan notas frutales y florales tales como rosas, yogurt de fresa y toronja </p>
        </div>

        <div class="grupo_movil2">
            <p class="bold_movil">Gusto:</p>
            <p class="normal_movil">(I) Vino fresco con una acidez media y buen ataque en boca.  (D) Frescura y elegancia que reafirma notas percibidas en nariz. </p>
        </div>

        <div class="grupo_movil2">
            <p class="bold_movil">Persistencia:</p>
            <p class="normal_movil">Media.</p>
        </div>
        <div class="grupo_movil2">
            <p class="bold_movil">Maridaje:</p>
            <p class="normal_movil">(I) Pizzas, quesos suaves, pay de queso con frutos rojos. (D) Quesadillas con diferentes guisos, pay de limón.</p>
        </div>

    </div>

    <img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

</div>