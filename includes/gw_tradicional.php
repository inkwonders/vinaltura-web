<div class="cont_int_datos_vino section1">

    <div class="title_vino_movil">
        <p class="titulo_movil">GW Tradicional <br> <span class="ano_movil"></span></p>
    </div>


    <img class="cirulo_movil" src="assets/img/uvas-naranjas.png" alt="Vinaltura">
    <img class="botella_vino_movil" src="assets/img/vinos/especiales/gw_tradicional.png" alt="Vinaltura tinto bajio">

    <a style="text-decoration: none; color:white;" href="https://tiendavinaltura.mx/" target="_blank">
        <div class="btn_tienda">COMPRAR</div>
    </a>

    <a target="_blank" href="assets/pdf/gw_tradicional.pdf"><div class="btn_tienda_movil">FICHA TECNICA</div></a>

    <img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

    <div class="cont_nombre_tipo bajios_ubic">
        <p style="background-position: 83.4133% 152.643%;">Especiales</p>
    </div>

</div>

<div class="cont_int_datos_vino section2">

    <div class="cont_datos_movil_vino">

        <div class="grupo_movil">
            <p class="bold_movil">Familia:</p>
            <p class="normal_movil">Ediciones especiales.</p>
        </div>

        <div class="grupo_movil">
            <p class="bold_movil">Origen:</p>
            <p class="normal_movil">Valle de Colón, QRO.</p>
        </div>

        <div class="grupo_movil">
            <p class="bold_movil">Varietal:</p>
            <p class="normal_movil">100% Gewürztraminer</p>
        </div>

        <div class="grupo_movil">
            <p class="bold_movil">Fermentación:</p>
            <p class="normal_movil">Espumoso método TRADICIONAL, doble fermentación, primera en tanque de acero inoxidable y segunda en botella.</p>
        </div>

        <div class="grupo_movil">
            <p class="bold_movil">Maridaje:</p>
            <p class="normal_movil">Huazontles, pozole verde, mole verde.</p>
        </div>

    </div>

    <img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

</div>

<div class="cont_int_datos_vino section3">

    <div class="cont_datos_movil_vino_2">

        <p class="titulo_datos_movil">Nota de cata</p>

        <div class="grupo_movil2">
            <p class="bold_movil">Vista:</p>
            <p class="normal_movil">Amarillo verdoso, con destellos plateados, burbuja fina y elegante que forma un rosario constante y una corona muy fina.</p>
        </div>

        <div class="grupo_movil2">
            <p class="bold_movil">Olfato:</p>
            <p class="normal_movil">Intensidad aroma alta media, donde destacan notas cítricas y Dulces principalmente como el limón, lima, y miel.</p>
        </div>

        <div class="grupo_movil2">
            <p class="bold_movil">Gusto:</p>
            <p class="normal_movil">Vino muy fresco, acidez media, con un final ligeramente dulce y una burbuja muy sutil.</p>
        </div>

    </div>

    <img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

</div>