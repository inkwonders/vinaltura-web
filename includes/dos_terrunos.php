<div class="cont_int_datos_vino section1">

	<div class="title_vino_movil">
		<p class="titulo_movil">Dos Terruños Baja Bajío <br> <span class="ano_movil"></span></p>
	</div>

	<img class="cirulo_movil" src="assets/img/uvas-moradas.jpg" alt="Vinaltura">
	<img class="botella_vino_movil" src="assets/img/vinos/terruños/dos-terrunos-baja-bajio-2017-terrunos.png" alt="Vinaltura dos terruños bajio">

	<a style="text-decoration: none; color:white;" href="https://tiendavinaltura.mx/" target="_blank">
		<div class="btn_tienda">COMPRAR</div>
	</a>
<!-- 

	<div style="display: flex; flex-direction: column; justify-content: space-around; align-items: center; position: relative; width: 100%; height: 100px; top: 62%;">

		<a target="_blank" href="assets/pdf/Terruños Dos Terruños.pdf"><div class="btn_tienda_movil_doble">FICHA TECNICA</div></a>
	
		<a target="_blank" href="assets/pdf/Terruños Dos Terruños.pdf"><div class="btn_tienda_movil_doble">FICHA TECNICA</div></a>
		
	</div> -->

	<div style="display: flex; flex-direction: column; justify-content: space-around; align-items: center; position: relative; width: 100%; height: 100px; top: 62%;">

		<a target="_blank" class="btn_tienda_movil_doble" href="assets/pdf/ficha_tecnica_dos_terrunos_2018.pdf">FICHA TECNICA 2018</a>
	
		<a target="_blank" class="btn_tienda_movil_doble" href="assets/pdf/Terruños Dos Terruños.pdf">FICHA TECNICA 2017</a>
		
	</div>

	<img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

	<div class="cont_nombre_tipo bajios_ubic otros">
		<p style="background-position: 83.4133% 152.643%;">Terruños</p>
	</div>

</div>

<div class="cont_int_datos_vino section2">

	<div class="cont_datos_movil_vino">

		<div class="grupo_movil">
			<p class="bold_movil">Familia:</p>
			<p class="normal_movil">Terruños.</p>
		</div>

		<div class="grupo_movil">
			<p class="bold_movil">Origen:</p>
			<p class="normal_movil">Valle de Guadalupe, B.C.</p>
		</div>

		<div class="grupo_movil">
			<p class="bold_movil">Varietal:</p>
			<p class="normal_movil">Cabernet Sauvignon, Merlot, Cabernet Franc, Malbec.</p>
		</div>

		<div class="grupo_movil">
			<p class="bold_movil">Fermentación:</p>
			<p class="normal_movil">Tanques de acero inoxidable.</p>
		</div>

		<div class="grupo_movil">
			<p class="bold_movil">Añejamiento:</p>
			<p class="normal_movil">12 meses de barrica de roble francés y americano, mínimo 12 meses de botella.</p>
		</div>

		<div class="grupo_movil">
			<p class="bold_movil">Maridaje:</p>
			<p class="normal_movil">Tacos estilo Ensenada y Pay de piña.</p>
		</div>

	</div>

	<img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

</div>

<div class="cont_int_datos_vino section3">

	<div class="cont_datos_movil_vino_2">

		<p class="titulo_datos_movil">Nota de cata</p>

		<div class="grupo_movil2">
			<p class="bold_movil">Vista:</p>
			<p class="normal_movil">Chiles en nogada, caramelos, confit.</p>
		</div>

		<div class="grupo_movil2">
			<p class="bold_movil">Olfato:</p>
			<p class="normal_movil">Nariz compleja y buena intensidad, con notas a frutos negros, pimienta negra y tostados.</p>
		</div>

		<div class="grupo_movil2">
			<p class="bold_movil">Gusto:</p>
			<p class="normal_movil">Ataque agradable y refinado, con buena persistencia. Se confirman notas complejas de frutos negros.</p>
		</div>

	</div>

	<img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

</div>