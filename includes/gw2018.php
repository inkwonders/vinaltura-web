<div class="cont_int_datos_vino section1">

	<div class="title_vino_movil">
		<p class="titulo_movil">GW <br> <span class="ano_movil"></span></p>
	</div>


	<img class="cirulo_movil" src="assets/img/uvas-naranjas.png" alt="Vinaltura">
	<img class="botella_vino_movil" src="assets/img/vinos/especiales/gw-2018.png" alt="Vinaltura tinto bajio">

	<div class="btn_tienda"><a style="text-decoration: none; color:white;" href="https://tiendavinaltura.mx/" target="_blank">COMPRAR</a></div>

	<a target="_blank" href="assets/pdf/Varietales GW.pdf"><div class="btn_tienda_movil">FICHA TECNICA</div></a>

	<img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

	<div class="cont_nombre_tipo bajios_ubic no_bajios_ubic">
		<p style="background-position: 83.4133% 152.643%;"> ESPECIALES</p>
	</div>

</div>

<div class="cont_int_datos_vino section2">

	<div class="cont_datos_movil_vino">

		<div class="grupo_movil">
			<p class="bold_movil">Familia:</p>
			<p class="normal_movil">Varietales (vino de postre).</p>
		</div>

		<div class="grupo_movil">
			<p class="bold_movil">Origen:</p>
			<p class="normal_movil">Valle de Colón, QRO.</p>
		</div>

		<div class="grupo_movil">
			<p class="bold_movil">Varietal:</p>
			<p class="normal_movil">Gewürztraminer</p>
		</div>

		<div class="grupo_movil">
			<p class="bold_movil">Fermentación:</p>
			<p class="normal_movil">Tanques de acero inoxidable.</p>
		</div>

		<div class="grupo_movil">
			<p class="bold_movil">Añejamiento:</p>
			<p class="normal_movil">Sin Barrica, mínimo 12 meses de botella.</p>
		</div>

		<div class="grupo_movil">
			<p class="bold_movil">Maridaje:</p>
			<p class="normal_movil">Quesos fuertes con miel, chocolate amargo.</p>
		</div>

	</div>

	<img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

</div>

<div class="cont_int_datos_vino section3">

	<div class="cont_datos_movil_vino_2">

		<p class="titulo_datos_movil">Nota de cata</p>

		<div class="grupo_movil2">
			<p class="bold_movil">Vista:</p>
			<p class="normal_movil">Limpio y brillante, color amarillo paja con destellos dorados, con una densidad aparente media alta.</p>
		</div>

		<div class="grupo_movil2">
			<p class="bold_movil">Olfato:</p>
			<p class="normal_movil">Intensidad aromática alta de carácter floral destacando notas como pétalos de rosa, lavanda y azucena. Ligeras notas a frutos secos.</p>
		</div>

		<div class="grupo_movil2">
			<p class="bold_movil">Gusto:</p>
			<p class="normal_movil">Ataque amplio al paladar, untuoso y sedoso, reafirmando la floralidad de nariz.</p>
		</div>

	</div>

	<img class="icon_slide" src="assets/img/svg/slide.svg" alt="Vinaltura">

</div>