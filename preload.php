

<div class="preload">

	<div id="mayoria" style="position: relative; width: 100%; height: 100vh; display: flex; justify-content: center; align-items: center; background-color: white; z-index: 9999; overflow: hidden; background-image: url(assets/img/slider/1.png); background-size: cover;">
	
		<img src="assets/img/svg/logo_cafe_completo.svg" alt="Vinaltura" style="position: absolute; top: 3%; height: 10vh;">
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 606.29 474.33" style="position: relative; width: 40%;"><defs><style>.cls-1{fill:#1f1f1f;}.cls-2{fill:none;stroke:#1f1f1f;stroke-miterlimit:10;stroke-width:4px;}.cls-3{font-size:47.34px;font-family:Gotham-Light, Gotham;font-weight:300;letter-spacing:-0.04em;}.cls-12,.cls-3,.cls-9{fill:#fff;}.cls-4{letter-spacing:-0.15em;}.cls-5{letter-spacing:-0.1em;}.cls-6{letter-spacing:-0.09em;}.cls-7{letter-spacing:-0.06em;}.cls-8{letter-spacing:-0.04em;}.cls-10,.cls-12{font-size:40px;font-family:Gotham-Bold, Gotham;font-weight:700;letter-spacing:-0.06em;}.cls-10,.cls-11{fill:#503629;}</style></defs><g id="Capa_2" data-name="Capa 2"><g id="Página" class="pagina"><path class="cls-1" d="M518,237.17C518,358.59,419.56,457,298.15,457S78.29,358.59,78.29,237.17,176.72,17.31,298.15,17.31,518,115.74,518,237.17"/><path class="cls-2" d="M533.31,237.17c0,129.88-105.29,235.16-235.16,235.16S63,367.05,63,237.17,168.26,2,298.15,2,533.31,107.29,533.31,237.17"/><text class="cls-3" transform="translate(225.28 189.11)">¿ERES <tspan x="-12.78" y="45.26">M</tspan><tspan class="cls-4" x="26.41" y="45.26">A</tspan><tspan class="cls-5" x="56.71" y="45.26">Y</tspan><tspan x="85.87" y="45.26">OR </tspan><tspan x="39.41" y="90.51">DE </tspan><tspan x="0.09" y="135.77">E</tspan><tspan class="cls-6" x="29.92" y="135.77">D</tspan><tspan x="62.91" y="135.77">A</tspan><tspan class="cls-7" x="98.41" y="135.77">D</tspan><tspan class="cls-8" x="132.59" y="135.77">?</tspan></text></g></g></svg>

		<div class="cont_ciruclo">
			<div class="ciruclo_btn btn_si" onclick="cli_si();">Si</div>
			<div class="ciruclo_btn btn_no" onclick="cli_no();">No</div>
		</div>
		

	</div>

	<img src="assets/img/svg/logo_movil_cafe.svg" alt="Vinaltura" id="img-pre" class="animated fadeIn oculto">
	<div id="preloader" class="preloader oculto"></div>
</div>
