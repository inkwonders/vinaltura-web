$(document).ready(function () {

        /* slider */

        $('.slider_principal').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: false,
                autoplay: true,
                autoplaySpeed: 5000,
                slide: 'div',
                dots: true,
                mobileFirst: true,
                pauseOnHover: false,
                pauseOnDotsHover: false
        });

        /* end slider */

        /* slider links movil */

        $('.slider_movil_links').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: false,
                autoplay: false,
                autoplaySpeed: 5000,
                slide: 'div',
                dots: false,
        });

        /* end slider links movil */

        /* slider's nosotros */

        $('.slider_nosotros_vinedo').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                fade: false,
                autoplay: true,
                autoplaySpeed: 3000,
                slide: 'div',
                dots: true,
                prevArrow: '<img id="btn_slider_nosostros_derecha" src="assets/img/right.png">',
                nextArrow: '<img id="btn_slider_nosostros_izquierda" src="assets/img/left.png">',
        });

        $('.slider_nosotros_bodega').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                fade: false,
                autoplay: true,
                autoplaySpeed: 3000,
                slide: 'div',
                dots: true,
                prevArrow: '<img id="btn_slider_nosostros_derecha" src="assets/img/right.png">',
                nextArrow: '<img id="btn_slider_nosostros_izquierda" src="assets/img/left.png">',
        });

        $('.slider_nosotros_region').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                fade: false,
                autoplay: true,
                autoplaySpeed: 3000,
                slide: 'div',
                dots: true,
                prevArrow: '<img id="btn_slider_nosostros_derecha" src="assets/img/right.png">',
                nextArrow: '<img id="btn_slider_nosostros_izquierda" src="assets/img/left.png">',
        });

        /* end slider's nosotros */

        /* slider nosotros movil */

        $('.slider_nosotros_vinedo_movil').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: false,
                autoplay: true,
                autoplaySpeed: 3000,
                slide: 'div',
                dots: true,
        });

        $('.slider_nosotros_bodega_movil').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: false,
                autoplay: true,
                autoplaySpeed: 3000,
                slide: 'div',
                dots: true,
        });

        $('.slider_nosotros_region_movil').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: false,
                autoplay: true,
                autoplaySpeed: 3000,
                slide: 'div',
                dots: true,
        });

        /** end slider nosotros movil */

        /** slide terraza **/

        $('.slider_terraza_movil').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: false,
                autoplay: true,
                autoplaySpeed: 3000,
                slide: 'div',
                dots: true,
        });

        /** end slide terraza **/

        /* slider vinos movil */

        //$('.contenido_vinos_movil').slick({
        $('.interno_tipo_vino_movil').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: false,
                autoplay: false,
                autoplaySpeed: 5000,
                slide: 'div',
                dots: false,
        });

        /* end slider vinos movil */

        /* menu */

        var flag = false;

        var scroll = $(window).scrollTop();

        if (scroll > 200) {

                $(".logo_blanco").attr("src", "assets/img/svg/logo_cafe.svg");
                $(".logo_blanco").css({
                        "height": "5vh"
                });
                $("header").css({
                        "background-color": "white",
                        "height": "17%"
                });
                $(".menu").css({
                        "color": "#503629"
                });
                $(".cls-1").css({
                        "fill": "#503629"
                });
                $(".men").removeClass("menu_opc_animation");
                $(".men").addClass("menu_opc_animation_cafe");
        }


        $(window).scroll(function () {

                scroll = $(window).scrollTop();

                if (scroll > 200) {
                        if (!flag) {

                                $(".logo_blanco").attr("src", "assets/img/svg/logo_cafe.svg");
                                $(".logo_blanco").css({
                                        "height": "5vh"
                                });
                                $("header").css({
                                        "background-color": "white",
                                        "height": "17%"
                                });
                                $(".menu").css({
                                        "color": "#503629"
                                });
                                $(".login").css({
                                        "color": "#503629"
                                });
                                $(".cls-1").css({
                                        "fill": "#503629"
                                });
                                $(".men").removeClass("menu_opc_animation");
                                $(".men").addClass("menu_opc_animation_cafe");
                                flag = true;

                                /** header movil **/

                                // $(".menu-bar").css("background","var(--c_cafe)");
                                $(".menu-bar").css("height", "50px");
                                $(".reservacion_movil").css("top", "48px");


                                $(".logo_mov_header").css("display", "none");

                                $(".container").css("top", "15px");

                                // $(".bar1, .bar2, .bar3").css("background-color","#fff");

                                /** end header movil **/

                        }
                } else {
                        if (flag) {

                                $(".logo_blanco").attr("src", "assets/img/svg/logo_cafe_completo.svg");
                                $(".logo_blanco").css({
                                        "height": "10vh"
                                });
                                $("header").css({
                                        "background-color": "transparent",
                                        "height": "17%"
                                });
                                $(".menu").css({
                                        "color": "#503629"
                                });
                                $(".login").css({
                                        "color": "#503629"
                                });
                                $(".cls-1").css({
                                        "fill": "#503629"
                                });
                                $(".menu_opc_animation::before").css("background", "#503629");
                                $(".men").removeClass("menu_opc_animation_cafe");
                                $(".men").addClass("menu_opc_animation");
                                flag = false;

                                /** header movil **/

                                // $(".menu-bar").css("background","#503629");
                                $(".menu-bar").css("height", "auto");
                                $(".reservacion_movil").css("top", "90px");


                                $(".logo_mov_header").css("display", "inherit");

                                $(".container").css("top", "27px");

                                // $(".bar1, .bar2, .bar3").css("background-color","var(--c_cafe)");

                                /** end header movil **/

                        }
                }


        });

        /* end menu */

        /* movimiento de los vinos */

        document.addEventListener('mousemove', parallax); /** botella **/

        function parallax(e) {
                this.querySelectorAll('.layer').forEach(layer => {
                        let x = (window.innerWidth - e.pageX * 3) / 100 + 100;
                        let y = (window.innerHeight - e.pageY * 3) / 100 + 100;
                        layer.style.transform = `translate(${x}px, ${y}px)`;
                })
        }

        document.addEventListener('mousemove', parallax_circulo); /** circulo **/

        function parallax_circulo(e) {
                this.querySelectorAll('.layer2').forEach(layer => {
                        let x = (window.innerWidth - e.pageX * 3) / 100 + 10;
                        let y = (window.innerHeight - e.pageY * 3) / 100 + 10;
                        layer.style.transform = `translate(-${x}px, ${y}px)`;
                })
        }

        /* end movimiento de los vinos */

        $(".boton_unir").click(function () {
                animacion("contacto");
        });

        /* animacion del menu */

        function animacion(elemento) {

                $('html, body').animate({
                        scrollTop: ($("." + elemento).offset().top - 100)
                }, 1500);

        }



        $(document).on("click", ".menu_opc_animation, .menu_opc_animation_cafe, .reservar_texto", function () {

                let scroll = $(window).scrollTop();

                if (scroll > 200) {
                        $(".menu_opc_animation_cafe").removeClass("menu_active");
                        $(this).addClass("menu_active");
                } else {
                        $(".menu_opc_animation").removeClass("menu_active");
                        $(this).addClass("menu_active");
                }

                var opcion = parseInt($(this).attr("op"));

                switch (opcion) {
                        case 0:
                                animacion("slider_principal");

                                break;
                        case 1:
                                animacion("nosotros_nuevo");

                                break;
                        case 2:
                                animacion("vinos");

                                break;
                        case 3:
                                //animacion("tienda");

                                let ruta = window.location.href;
                                console.log(ruta);
                                location.href = "/tienda";

                                break;
                        case 4:
                                animacion("wine_club");

                                break;
                        case 5:
                                animacion("terraza");

                                break;
                        case 6:
                                animacion("contacto");

                                break;
                        case 7:
                                animacion("terraza");

                                break;
                        case 8:
                                animacion("terraza");

                                break;
                        default:
                                break;
                }

        });

        /* end animacion del menu */

        /* familia tipos de vinos*/

        $(document).on("click", ".lista_vinos_opc", function () {

                $(".lista_vinos_opc").removeClass("lista_vinos_active");
                $(this).addClass("lista_vinos_active");

                let opcion = parseInt($(this).attr("op"));

                switch (opcion) {
                        case 1:
                                $(".cont_vino_caracteristicas").addClass("oculto");
                                $(".zona_bajio").removeClass("oculto");

                                $(".lista_vinos_sub").addClass("oculto");
                                $(".lista_tipos_vino_bajio").removeClass("oculto");

                                $(".datos").addClass("oculto");
                                $(".vino3").removeClass("oculto");
                                $(".lista_tipos_vino_opc").removeClass("lista_tipos_vino_active");
                                $(".sub3").addClass("lista_tipos_vino_active");
                                $(".vino_img").attr("src", "assets/img/vinos/bajio/blanco-sin-año.png");
                                $(".uvas_bajio").css("background-image", "url(assets/img/uvas-verdes.png)");

                                break;
                        case 2:
                                $(".cont_vino_caracteristicas").addClass("oculto");
                                $(".zona_varietales").removeClass("oculto");

                                $(".lista_vinos_sub").addClass("oculto");
                                $(".lista_tipos_vino_varietales").removeClass("oculto");

                                $(".datos").addClass("oculto");
                                $(".vino3").removeClass("oculto");
                                $(".lista_tipos_vino_opc").removeClass("lista_tipos_vino_active");
                                $(".sub8").addClass("lista_tipos_vino_active");
                                $(".vino_img").attr("src", "assets/img/vinos/varietales/chenin-blanc-2019-varietales.png");
                                $(".uvas_varietales").css("background-image", "url(assets/img/uvas-verdes.png)");
                                break;
                        case 3:
                                $(".cont_vino_caracteristicas").addClass("oculto");
                                $(".zona_terruños").removeClass("oculto");

                                $(".lista_vinos_sub").addClass("oculto");
                                $(".lista_tipos_vino_terruños").removeClass("oculto");

                                $(".datos").addClass("oculto");
                                $(".vino4").removeClass("oculto");
                                $(".lista_tipos_vino_opc").removeClass("lista_tipos_vino_active");
                                $(".sub9").addClass("lista_tipos_vino_active");
                                $(".vino_img").attr("src", "assets/img/vinos/terruños/terruno-blanco.png");
                                $(".uvas_terrunos").css("background-image", "url(assets/img/uvas-verdes.png)");
                                break;
                        case 4:
                                $(".cont_vino_caracteristicas").addClass("oculto");
                                $(".zona_ensayos").removeClass("oculto");

                                $(".lista_vinos_sub").addClass("oculto");
                                $(".lista_tipos_vino_ensayos").removeClass("oculto");

                                $(".datos").addClass("oculto");
                                $(".vino2").removeClass("oculto");
                                $(".lista_tipos_vino_opc").removeClass("lista_tipos_vino_active");
                                $(".sub14").addClass("lista_tipos_vino_active");
                                $(".vino_img").attr("src", "assets/img/vinos/especiales/gw-ancestral.png");
                                $(".uvas_especiales").css("background-image", "url(assets/img/uvas-naranjas.png)");

                                break;
                        default:
                                break;
                }

        });

        /* end familia tipos de vinos */

        /* nostros viñedo-bodega-region */

        $(document).on("click", ".opc_menu_nosotros", function () {

                $(".opc_menu_nosotros").removeClass("menu_nosotros_active");
                $(this).addClass("menu_nosotros_active");

                let opcion = parseInt($(this).attr("op"));

                switch (opcion) {
                        case 1:
                                $(".opciones_nostros_cont").addClass("oculto");
                                $(".vinedo").removeClass("oculto");

                                $('.slider_nosotros_vinedo').slick('refresh');

                                break;
                        case 2:
                                $(".opciones_nostros_cont").addClass("oculto");
                                $(".bodega").removeClass("oculto");

                                $('.slider_nosotros_bodega').slick('refresh');


                                break;
                        case 3:
                                $(".opciones_nostros_cont").addClass("oculto");
                                $(".region").removeClass("oculto");

                                $('.slider_nosotros_region').slick('refresh');

                                break;
                        default:
                                break;
                }

        });

        $(document).on("click", ".opc_menu_nosotros_movil", function () {

                let opcion = parseInt($(this).attr("op"));

                switch (opcion) {
                        case 1:
                                $(".opciones_nosotros_cont_movil").addClass("oculto");
                                $(".vinedo_movil").removeClass("oculto");

                                $('.slider_nosotros_vinedo_movil').slick('refresh');

                                break;
                        case 2:
                                $(".opciones_nosotros_cont_movil").addClass("oculto");
                                $(".bodega_movil").removeClass("oculto");

                                $('.slider_nosotros_bodega_movil').slick('refresh');


                                break;
                        case 3:
                                $(".opciones_nosotros_cont_movil").addClass("oculto");
                                $(".region_movil").removeClass("oculto");

                                $('.slider_nosotros_region_movil').slick('refresh');

                                break;
                        default:
                                break;
                }

        });

        /* end nostros viñedo-bodega-region */

        /*  tipos de vinos*/

        $(document).on("click", ".lista_tipos_vino_opc", function () {


                $(".lista_tipos_vino_opc").removeClass("lista_tipos_vino_active");
                $(this).addClass("lista_tipos_vino_active");

                let opcion = parseInt($(this).attr("op"));

                $(".datos").addClass("oculto");
                $(".vino_img_2").addClass("oculto");

                console.log(opcion)


                switch (opcion) {
                        case 1:
                                $(".vino1").removeClass("oculto");
                                $(".vino_img").attr("src", "assets/img/vinos/bajio/tinto-bajio.png");
                                $(".uvas_bajio").css("background-image", "url(assets/img/uvas-moradas.jpg)");
                                break;
                        case 2:
                                $(".vino2").removeClass("oculto");
                                $(".vino_img").attr("src", "assets/img/vinos/bajio/espuma-rose-bajio.png");
                                $(".uvas_bajio").css("background-image", "url(assets/img/uvas-verdes-moradas.png)");
                                break;
                        case 3:
                                $(".vino3").removeClass("oculto");
                                $(".vino_img").attr("src", "assets/img/vinos/bajio/blanco-sin-año.png");
                                $(".uvas_bajio").css("background-image", "url(assets/img/uvas-verdes.png)");
                                break;
                        case 4:
                                $(".vino4").removeClass("oculto");
                                $(".vino_img").attr("src", "assets/img/vinos/bajio/rose-bajio.png");
                                $(".uvas_bajio").css("background-image", "url(assets/img/uvas-moradas.jpg)");
                                break;
                        case 5:
                                $(".vino1").removeClass("oculto");
                                $(".vino_img").attr("src", "assets/img/vinos/varietales/suavignon-blanc-2019.png");
                                $(".uvas_varietales").css("background-image", "url(assets/img/uvas-verdes.png)");
                                break;
                        case 6:
                                $(".vino2").removeClass("oculto");
                                $(".vino_img").attr("src", "assets/img/vinos/varietales/riesling-2020.png");
                                $(".uvas_varietales").css("background-image", "url(assets/img/uvas-verdes.png)");
                                break;
                        case 7:
                                $(".vino3").removeClass("oculto");
                                $(".vino_img").attr("src", "assets/img/vinos/varietales/chenin-blanc-2019-varietales.png");
                                $(".uvas_varietales").css("background-image", "url(assets/img/uvas-verdes.png)");
                                break;
                        case 8:
                                $(".vino4").removeClass("oculto");
                                $(".vino_img").attr("src", "assets/img/vinos/varietales/malbec.png");
                                $(".uvas_varietales").css("background-image", "url(assets/img/uvas-moradas.jpg)");
                                break;
                        case 9:
                                $(".vino1").removeClass("oculto");
                                // $(".vino_img").attr("src","assets/img/vinos/terruños/arroyo-2016-terruno.png");
                                $(".vino_img").attr("src", "assets/img/vinos/terruños/Terruno Arroyo 2017.png");
                                $(".uvas_terrunos").css("background-image", "url(assets/img/uvas-moradas.jpg)");

                                break;
                        case 10:
                                $(".vino2").removeClass("oculto");
                                // $(".vino_img").attr("src","assets/img/vinos/terruños/ladera-2016-teruno.png");
                                $(".vino_img").attr("src", "assets/img/vinos/terruños/Terruno Ladera 2017.png");
                                $(".uvas_terrunos").css("background-image", "url(assets/img/uvas-moradas.jpg)");
                                break;
                        case 11:
                                $(".vino3").removeClass("oculto");
                                $(".vino_img").attr("src", "assets/img/vinos/terruños/dos-terrunos-baja-bajio-2017-terrunos.png");
                                $(".uvas_terrunos").css("background-image", "url(assets/img/uvas-moradas.jpg)");
                                break;
                        case 12:
                                $(".vino5").removeClass("oculto");
                                $(".vino_img").attr("src", "assets/img/vinos/especiales/gw-2018.png");
                                $(".uvas_varietales").css("background-image", "url(assets/img/uvas-naranjas.png)");

                                break;
                        case 13:
                                $(".vino1").removeClass("oculto");
                                $(".vino_img").attr("src", "assets/img/vinos/especiales/un-par-de.png");
                                $(".uvas_especiales").css("background-image", "url(assets/img/uvas-verdes.png)");
                                break;
                        case 14:
                                $(".vino3").removeClass("oculto");
                                $(".vino_img").attr("src", "assets/img/vinos/especiales/gw_tradicional.png");
                                $(".uvas_especiales").css("background-image", "url(assets/img/uvas-verdes.jpg");
                                break;
                        case 15:
                                $(".vino4").removeClass("oculto");
                                $(".vino_img").attr("src", "assets/img/vinos/terruños/terruno-blanco.png");
                                $(".uvas_terrunos").css("background-image", "url(assets/img/uvas-verdes.png)");

                                break;
                        case 16:
                                $(".vino5").removeClass("oculto");
                                $(".vino_img").attr("src", "assets/img/vinos/terruños/terruno-bajio.png");
                                $(".uvas_terrunos").css("background-image", "url(assets/img/uvas-moradas.jpg)");
                                break;
                        case 17:
                                $(".vino2").removeClass("oculto");
                                $(".vino_img").attr("src", "assets/img/vinos/especiales/gw-ancestral.png");
                                $(".uvas_especiales").css("background-image", "url(assets/img/uvas-naranjas.png)");
                                break;
                        case 18:
                                $(".vino5").removeClass("oculto");
                                $(".vino_img").attr("src", "assets/img/vinos/especiales/gw-2019.png");
                                $(".uvas_especiales").css("background-image", "url(assets/img/uvas-naranjas.png)");
                                break;
                        case 19:
                                $(".vino6").removeClass("oculto");
                                $(".vino_img").attr("src", "assets/img/vinos/especiales/Merlot.png");
                                $(".uvas_varietales").css("background-image", "url(assets/img/uvas-moradas.jpg)");

                                break;
                        case 20:
                                $(".vino5").removeClass("oculto");
                                $(".vino_img").attr("src", "assets/img/vinos/bajio/blanc_blancs.png");
                                $(".uvas_bajio").css("background-image", "url(assets/img/uvas-verdes.png)");
        
                                break;

                        case 21:
                                $(".vino7").removeClass("oculto");
                                $(".vino_img").attr("src", "assets/img/vinos/varietales/CH_Inclinada-8.png");
                                $(".uvas_varietales").css("background-image", "url(assets/img/uvas-verdes.png)");
                                break;

                                case 22:
                                $(".vino22").removeClass("oculto");
                                $(".vino_img").attr("src", "assets/img/vinos/especiales/Un-par-de-Rose_inclinada.png");
                                $(".uvas_especiales").css("background-image", "url(assets/img/uvas-moradas.jpg");
                                break;

                                        
                        default:
                                break;

                }

        });

        /* end  tipos de vinos */

        if (screen.width > 980) {
                $(".img_botella").attr("src", "assets/img/svg/imgizq_quienes_somos.svg");
        } else {
                $(".img_botella").attr("src", "assets/img/svg/imgizq_quienes_somos.svg");
        }

        /* scroll posicion de menu */

        var nosotros_pos = $(".nosotros_nuevo").offset().top;
        var vinos_pos = $(".vinos").offset().top;
        var wine_club_pos = $(".wine_club").offset().top;
        var terraza_pos = $(".terraza").offset().top;
        var contacto_pos = $(".contacto").offset().top;

        window.onscroll = function () {

                if (screen.width > 980) {

                        var scroll_pos = $(window).scrollTop();

                        if (scroll_pos < nosotros_pos) {
                                $('.men').removeClass("menu_active");
                        }

                        if (scroll_pos >= (nosotros_pos - 250)) {
                                $('.men').removeClass("menu_active");
                                $('.men[op="1"]').addClass("menu_active");
                        }

                        if (scroll_pos >= (vinos_pos - 250)) {
                                $('.men').removeClass("menu_active");
                                $('.men[op="2"]').addClass("menu_active");
                        }

                        if (scroll_pos >= (wine_club_pos - 250)) {
                                $('.men').removeClass("menu_active");
                                $('.men[op="4"]').addClass("menu_active");
                        }

                        if (scroll_pos >= (terraza_pos - 250)) {
                                $('.men').removeClass("menu_active");
                                $('.men[op="7"]').addClass("menu_active");
                        }

                        if (scroll_pos >= (contacto_pos - 250)) {
                                $('.men').removeClass("menu_active");
                                $('.men[op="6"]').addClass("menu_active");
                        }

                } else {

                        var nosotros_pos_mov = $(".nosotros_nuevo").offset().top;
                        var vinos_pos_mov = $(".vinos_movil").offset().top;
                        var wine_club_pos_mov = $(".wine_club").offset().top;
                        var terraza_pos_mov = $(".terraza").offset().top;
                        var contacto_pos_mov = $(".contacto").offset().top;

                        var scroll_pos_mov = $(window).scrollTop();

                        if (scroll_pos_mov < nosotros_pos) {
                                $(".nom_pos_menu_movil").empty();
                        }

                        if (scroll_pos_mov >= (nosotros_pos_mov - 250)) {
                                $(".nom_pos_menu_movil").empty();
                                $(".nom_pos_menu_movil").html("<p class='titulo_menu_movil'>NOSOTROS</p>");
                        }

                        if (scroll_pos_mov >= (vinos_pos_mov - 250)) {
                                $(".nom_pos_menu_movil").empty();
                                $(".nom_pos_menu_movil").html("<p class='titulo_menu_movil'>VINOS</p>");
                        }

                        if (scroll_pos_mov >= (wine_club_pos_mov - 250)) {
                                $(".nom_pos_menu_movil").empty();
                                $(".nom_pos_menu_movil").html("<p class='titulo_menu_movil'>WINE CLUB</p>");
                        }

                        if (scroll_pos_mov >= (terraza_pos_mov - 250)) {
                                $(".nom_pos_menu_movil").empty();
                                $(".nom_pos_menu_movil").html("<p class='titulo_menu_movil'>VISITAS</p>");
                        }

                        if (scroll_pos_mov >= (contacto_pos - 250)) {
                                $(".nom_pos_menu_movil").empty();
                                $(".nom_pos_menu_movil").html("<p class='titulo_menu_movil'>BRINDEMOS</p>");
                        }

                }

        }

        /* end scroll posicion de menu */

        /* scroll menu movil */

        function animacion_movil(elemento) {
                console.log(elemento);
                $('html, body').animate({
                        scrollTop: ($("." + elemento).offset().top - 50)
                }, 1500);
        }

        $(document).on("click", ".menu-el", function () {

                $(".container").removeClass("change");

                let opcion = $(this).attr("op");

                switch (opcion) {
                        case "home":
                                animacion_movil("slider_principal");
                                //$(".nom_pos_menu_movil").empty();
                                break;
                        case "nosotros":
                                animacion_movil("nosotros_nuevo");
                                //$(".nom_pos_menu_movil").html("<p class='titulo_menu_movil'>NOSOTROS</p>");
                                break;
                        case "vinos":
                                animacion_movil("vinos_movil");
                                //$(".nom_pos_menu_movil").html("<p class='titulo_menu_movil'>VINOS</p>");
                                break;
                        case "wineclub":
                                animacion_movil("wine_club");
                                //$(".nom_pos_menu_movil").html("<p class='titulo_menu_movil'>WINECLUB</p>");
                                break;
                        case "visitas":
                                animacion_movil("terraza");
                                //$(".nom_pos_menu_movil").html("<p class='titulo_menu_movil'>TERRAZA</p>");
                                break;
                        case "contacto":
                                animacion_movil("contacto");
                                //$(".nom_pos_menu_movil").html("<p class='titulo_menu_movil'>CONTACTO</p>");
                                break;
                        default:
                                break;
                }

        });

        /* end scroll menu movil */

});

/* menu movil */

function myFunction(x) {

        x.classList.toggle("change");
        $(".cont_logo_barra").toggle("change");

        if (document.getElementById("contenidoMenu").style.display == "none") {

                document.getElementById("contenidoMenu").style.display = "";

                $("#contenidoMenu").animate({
                        height: '100vh'
                }, "slow");

                $(".bar3").addClass("no_visible");
                $(".logo_mov_header").addClass("no_visible");

                $(".nom_pos_menu_movil").css("opacity", "0");

        } else {

                $("#contenidoMenu").animate({
                        height: '0'
                }, "slow", function () {
                        document.getElementById("contenidoMenu").style.display = "none";
                });

                $(".bar3").removeClass("no_visible");
                $(".logo_mov_header").removeClass("no_visible");

                $(".nom_pos_menu_movil").css("opacity", "1");

        }
}

/* end menu movil */

/* vinos movil */

$(document).on("click", ".opc_vino_movil", function () {

        $(".opc_vino_movil").removeClass("active_movil_zona");
        $(this).addClass("active_movil_zona");
        $(".tipo_movil").removeClass("tipo_movil_active");
        $(".interno_tipo_vino_movil").addClass("oculto");

        let opcion = parseInt($(this).attr("op"));

        $(".contenido_vinos_movil").addClass("oculto");

        switch (opcion) {
                case 1:

                        $(".tipos_vino_movil").addClass("oculto");
                        $(".tip_bajio").removeClass("oculto");
                        $(".bajio_movil").removeClass("oculto");
                        $(".blanco_movil").removeClass("oculto");
                        $(".tipo_movil[op=blanco]").addClass("tipo_movil_active");
                        break;
                case 2:
                        $(".tipos_vino_movil").addClass("oculto");
                        $(".tip_varietales").removeClass("oculto");
                        $(".varietales_movil").removeClass("oculto");
                        $(".chenin_movil").removeClass("oculto");
                        $(".tipo_movil[op=chenin]").addClass("tipo_movil_active");
                        break;
                case 3:
                        $(".tipos_vino_movil").addClass("oculto");
                        $(".tip_terrunos").removeClass("oculto");
                        $(".terrunos_movil").removeClass("oculto");
                        $(".terruno_blanco_movil").removeClass("oculto");
                        $(".tipo_movil[op=terruno_blanco]").addClass("tipo_movil_active");
                        break;
                case 4:
                        $(".tipos_vino_movil").addClass("oculto");
                        $(".tip_ensayos").removeClass("oculto");
                        $(".ensayos_movil").removeClass("oculto");
                        $(".gw_ancestral_movil").removeClass("oculto");
                        $(".tipo_movil[op=gw_ancestral]").addClass("tipo_movil_active");
                        break;

                default:
                        break;
        }

        $('.interno_tipo_vino_movil').slick('refresh');

});

$(document).on("click", ".tipo_movil", function () {

        let opcion = $(this).attr("op");
        console.log(opcion);

        $(".tipo_movil").removeClass("tipo_movil_active");
        $(this).addClass("tipo_movil_active");

        $(".interno_tipo_vino_movil").addClass("oculto");

        switch (opcion) {
                case "tinto_bajio":
                        $(".tinto_bajio_movil").removeClass("oculto");
                        $('.interno_tipo_vino_movil').slick('refresh');
                        break;
                case "espuma_rose":
                        $(".espuma_rose_movil").removeClass("oculto");
                        $('.interno_tipo_vino_movil').slick('refresh');
                        break;
                case "blanco":
                        $(".blanco_movil").removeClass("oculto");
                        $('.interno_tipo_vino_movil').slick('refresh');
                        break;
                case "rose_bajio":
                        $(".rose_bajio_movil").removeClass("oculto");
                        $('.interno_tipo_vino_movil').slick('refresh');
                        break;

                case "suavignon":
                        $(".suavignon_movil").removeClass("oculto");
                        $('.interno_tipo_vino_movil').slick('refresh');
                        break;
                case "riesling":
                        $(".riesling_movil").removeClass("oculto");
                        $('.interno_tipo_vino_movil').slick('refresh');
                        break;
                case "chenin":
                        $(".chenin_movil").removeClass("oculto");
                        $('.interno_tipo_vino_movil').slick('refresh');
                        break;
                case "gw":
                        $(".gw_movil").removeClass("oculto");
                        $('.interno_tipo_vino_movil').slick('refresh');
                case "malbec":
                        $(".malbec_movil").removeClass("oculto");
                        $('.interno_tipo_vino_movil').slick('refresh');
                        break;
                
                case "ch":
                        $(".ch_movil").removeClass("oculto");
                        $('.interno_tipo_vino_movil').slick('refresh');
                        break;


                case "arroyo":
                        $(".arroyo_movil").removeClass("oculto");
                        $('.interno_tipo_vino_movil').slick('refresh');
                        break;
                case "ladera":
                        $(".ladera_movil").removeClass("oculto");
                        $('.interno_tipo_vino_movil').slick('refresh');
                        break;
                case "dos_terrunos":
                        $(".dos_terrunos_movil").removeClass("oculto");
                        $('.interno_tipo_vino_movil').slick('refresh');
                        break;


                case "par_de_huevos":
                        $(".par_huevos_movil").removeClass("oculto");
                        $('.interno_tipo_vino_movil').slick('refresh');
                        break;
                case "par_de_huevos_rose":
                                $(".par_huevos__rose_movil").removeClass("oculto");
                                $('.interno_tipo_vino_movil').slick('refresh');
                                break;

                case "terruno_blanco":
                        $(".terruno_blanco_movil").removeClass("oculto");
                        $('.interno_tipo_vino_movil').slick('refresh');
                        break;

                case "terruno_bajio":
                        $(".terruno_bajio_movil").removeClass("oculto");
                        $('.interno_tipo_vino_movil').slick('refresh');
                        break;

                case "gw_ancestral":
                        $(".gw_ancestral_movil").removeClass("oculto");
                        $('.interno_tipo_vino_movil').slick('refresh');
                        break;
                case "gw_2019":
                        $(".gw_2019_movil").removeClass("oculto");
                        $('.interno_tipo_vino_movil').slick('refresh');
                        break;
                case "merlot":
                        $(".merlot_movil").removeClass("oculto");
                        $('.interno_tipo_vino_movil').slick('refresh');
                        break;

                case "blanc_blancs":
                        $(".blanc_blancs_movil").removeClass("oculto");
                        $('.interno_tipo_vino_movil').slick('refresh');
                        break;

                case "gw_tradicional":
                        $(".gw_tradicional_movil").removeClass("oculto");
                        $('.interno_tipo_vino_movil').slick('refresh');
                        break;


                default:
                        break;
        }

});

/* end vinos movil */

/** validar formulario **/

const formulario = document.getElementById('formulario'); /* formulario contacto */
const inputs = document.querySelectorAll('#formulario input');

const formulario_wineclub = document.getElementById('formulario_wine_club'); /* formulario wineclub */

const form_newsletters = document.getElementById('form-newsletters'); /* formulario wineclub */

const expresiones = {
        nombre: /^[a-zA-ZÀ-ÿ\s]{1,40}$/, // Letras y espacios, pueden llevar acentos.
        correo: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
        telefono: /^\d{10}$/, // 10 numeros.,
        mensaje: /^[a-zA-ZÀ-ÿ\s]{1,400}$/, // Letras y espacios, pueden llevar acentos.
}

const campos = {
        nombre: false,
        correo: false,
        telefono: false,
        mensaje: false
}

const validarFormulario = (e) => {
        switch (e.target.name) {
                case "nombre":
                        validarCampo(expresiones.nombre, e.target, 'nombre');
                        break;
                case "correo":
                        validarCampo(expresiones.correo, e.target, 'correo');
                        break;
                case "telefono":
                        validarCampo(expresiones.telefono, e.target, 'telefono');
                        break;
                        case "mensaje":
                                validarCampo(expresiones.mensaje, e.target, 'mensaje');
                                break;
        }
}

const validarCampo = (expresion, input, campo) => {
        if (expresion.test(input.value)) {

                document.getElementById(`grupo__${campo}`).classList.remove('formulario__grupo-incorrecto');
                document.getElementById(`grupo__${campo}`).classList.add('formulario__grupo-correcto');
                document.querySelector(`#grupo__${campo} i`).classList.add('fa-check-circle');
                document.querySelector(`#grupo__${campo} i`).classList.remove('fa-times-circle');

                campos[campo] = true;

                console.log(campo + " correcto");

        } else {

                document.getElementById(`grupo__${campo}`).classList.add('formulario__grupo-incorrecto');
                document.getElementById(`grupo__${campo}`).classList.remove('formulario__grupo-correcto');
                document.querySelector(`#grupo__${campo} i`).classList.add('fa-times-circle');
                document.querySelector(`#grupo__${campo} i`).classList.remove('fa-check-circle');
                //document.querySelector(`#grupo__${campo} .formulario__input-error`).classList.add('formulario__input-error-activo');

                campos[campo] = false;

                console.log(campo + " incorrecto");

        }
}

inputs.forEach((input) => {
        input.addEventListener('keyup', validarFormulario);
        input.addEventListener('blur', validarFormulario);
});

formulario.addEventListener('submit', (e) => {
        e.preventDefault();

        if (campos.nombre && campos.correo && campos.telefono && campos.mensaje) {

                console.log("fomulario validado correctamente");

                var datos = $("form").serialize();

                console.log(datos);
                console.log(campos);

                $.ajax({
                        url: 'includes/correo.php',
                        data: datos,
                        type: 'POST',
                        success: function (respuesta) {
                                console.log(respuesta);
                                formulario.reset();
                                campos.nombre = false;
                                campos.correo = false;
                                campos.telefono = false;
                                campos.mensaje = false;

                                Swal.fire({
                                        position: 'center',
                                        icon: 'success',
                                        title: 'Recibimos tus datos! nos pondremos en contacto contigo',
                                        showConfirmButton: false,
                                        timer: 3000
                                });

                                $(".grupo__nombre").removeClass("formulario__grupo-correcto");
                                $(".grupo__correo").removeClass("formulario__grupo-correcto");
                                $(".grupo__telefono").removeClass("formulario__grupo-correcto");
                                $(".grupo__mensaje").removeClass("formulario__grupo-correcto");
                        },
                        error: function () {
                                console.log("No se envio el correo");
                        }
                });

        } else {

                console.log("fomulario incorrecto");

        }
});

formulario_wineclub.addEventListener('submit', (e) => {
        e.preventDefault();

        var datos = $("#formulario_wine_club").serialize();

        $.ajax({
                url: 'includes/correo_wineclub.php',
                data: datos,
                type: 'POST',
                success: function (respuesta) {
                        console.log(respuesta);
                        formulario_wineclub.reset();

                        Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: 'Recibimos tus datos! nos pondremos en contacto contigo',
                                showConfirmButton: false,
                                timer: 3000
                        });

                },
                error: function () {
                        console.log("No se envio el correo");
                }
        });

});

form_newsletters.addEventListener('submit', (e) => {
        e.preventDefault();

        var datos = $("#form-newsletters").serialize();

        $.ajax({
                url: 'includes/correo_newsletter.php',
                data: datos,
                type: 'POST',
                success: function (respuesta) {
                        console.log(respuesta);
                        form_newsletters.reset();

                        Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: 'Recibimos tus datos! nos pondremos en contacto contigo',
                                showConfirmButton: false,
                                timer: 3000
                        });

                },
                error: function () {
                        console.log("No se envio el correo");
                }
        });

});

/** end validar fomrulario **/


/** seccion nosotros **/

$(document).on("click", ".txt_lista_nosotros", function () {
        console.log("click");
        $(".centrado").removeClass("padding_30");
        $(".cont_datos_nosotros").addClass("oculto");
        $(".linea_seleccion_nosotros").addClass("no-visible");
        let op = $(this).attr("op");

        $(".botellas").addClass("oculto");
        $(".top_img_bg").addClass("oculto");

        switch (op) {
                case "1":
                        $(".cont_quines_somos").removeClass("oculto");
                        $(".linea1").removeClass("no-visible");
                        /*$(".top_img_bg").css("background-image","url(assets/img/svg/banner_quienes_somos.svg)");
                        $(".img_top_circulo").css("background-image","url(assets/img/svg/circulo_quienes_somos.svg)");
                        $(".img_botella").attr("src","assets/img/svg/imgizq_quienes_somos.svg");*/
                        $(".img_botella_uno").removeClass("oculto");
                        $(".top_img_bg_uno").removeClass("oculto");

                        $(this).parent().addClass("padding_30");
                        break;
                case "2":
                        $(".cont_manifiesto").removeClass("oculto");
                        $(".linea2").removeClass("no-visible");
                        /*$(".top_img_bg").css("background-image","url(assets/img/svg/banner_manifiesto.svg)");
                        $(".img_top_circulo").css("background-image","url(assets/img/svg/circulo_manifiesto.svg)");
                        $(".img_botella").attr("src","assets/img/svg/imgizq_manifiesto.svg");*/

                        $(".img_botella_dos").removeClass("oculto");
                        $(".top_img_bg_dos").removeClass("oculto");

                        $(this).parent().addClass("padding_30");
                        break;

                case "4":
                        $(".cont_queretaro_region_de_vino").removeClass("oculto");
                        $(".linea4").removeClass("no-visible");
                        /*$(".top_img_bg").css("background-image","url(assets/img/svg/banner_queretaro.svg)");
                        $(".img_top_circulo").css("background-image","url(assets/img/svg/circulo_queretaro.svg)");
                        $(".img_botella").attr("src","assets/img/svg/imgizq_queretaro.svg");*/

                        $(".img_botella_tres").removeClass("oculto");
                        $(".top_img_bg_tres").removeClass("oculto");

                        $(this).parent().addClass("padding_30");
                        break;
                case "5":
                        $(".cont_viticultura_extrema").removeClass("oculto");
                        $(".linea5").removeClass("no-visible");
                        /*$(".top_img_bg").css("background-image","url(assets/img/svg/banner_viticultura.svg)");
                        $(".img_top_circulo").css("background-image","url(assets/img/svg/circulo_viticultura.svg)");
                        $(".img_botella").attr("src","assets/img/svg/imgizq_viticultura.svg");*/

                        $(".img_botella_cuatro").removeClass("oculto");
                        $(".top_img_bg_cuatro").removeClass("oculto");

                        $(this).parent().addClass("padding_30");
                        break;
                case "6":
                        $(".cont_viñedo").removeClass("oculto");
                        $(".linea6").removeClass("no-visible");
                        /*$(".top_img_bg").css("background-image","url(assets/img/svg/banner_vinedo.svg)");
                        $(".img_top_circulo").css("background-image","url(assets/img/svg/circulo_vinedo.svg)");
                        $(".img_botella").attr("src","assets/img/svg/imgizq_vinedo.svg");*/

                        $(".img_botella_cinco").removeClass("oculto");
                        $(".top_img_bg_cinco").removeClass("oculto");

                        $(this).parent().addClass("padding_30");
                        break;
                case "7":
                        $(".cont_viñedo_vinicola").removeClass("oculto");
                        $(".linea7").removeClass("no-visible");
                        /*$(".top_img_bg").css("background-image","url(assets/img/svg/banner_vinicola.svg)");
                        $(".img_top_circulo").css("background-image","url(assets/img/svg/circulo_vinicola.svg)");
                        $(".img_botella").attr("src","assets/img/svg/imgizq_vinicola.svg");*/

                        $(".img_botella_seis").removeClass("oculto");
                        $(".top_img_bg_seis").removeClass("oculto");

                        $(this).parent().addClass("padding_30");
                        break;
                default:
        }
});

$(document).on("click", ".txt_lista_nosotros_mov", function () {
        $(".txt_lista_nosotros_mov").css("text-decoration", "none");
        $(this).css("text-decoration", "underline");
        $(".cont_datos_nosotros_mov").addClass("oculto");
        let op = $(this).attr("op");
        switch (op) {
                case "1":
                        $(".cont_quines_somos_mov").removeClass("oculto");

                        break;
                case "2":
                        $(".cont_manifiesto_mov").removeClass("oculto");

                        break;

                case "4":
                        $(".cont_queretaro_region_de_vino_mov").removeClass("oculto");

                        break;
                case "5":
                        $(".cont_viticultura_extrema_mov").removeClass("oculto");

                        break;
                case "6":
                        $(".cont_viñedo_mov").removeClass("oculto");

                        break;

                case "7":
                        $(".cont_viñedo_vinicola_mov").removeClass("oculto");

                        break;
                default:
        }
});

/** end seccion nosotros **/